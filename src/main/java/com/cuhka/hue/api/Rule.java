package com.cuhka.hue.api;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Rule implements Serializable {
	private static final long serialVersionUID = 7698790248326793660L;
	private final String id;
	private final String name;
	private final String owner;
	private final Instant created;
	private final Optional<Instant> lastTriggered;
	private final int timesTriggered;
	private final Status status;
	private final List<Condition> conditions;
	private final List<Action> actions;

	public enum Status {
		ENABLED, DISABLED
	}

	@SuppressWarnings("unchecked")
	public static class Builder {
		private String id;
		private String name;
		private String owner;
		private Instant created;
		private Optional<Instant> lastTriggered;
		private int timesTriggered;
		private Collection<Condition> conditions;
		private Collection<Action> actions;
		private Status status;

		public String getId() {
			return id;
		}

		public Builder id(String id) {
			this.id = id;
			return this;
		}

		public String getName() {
			return name;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public String getOwner() {
			return owner;
		}

		public Builder owner(String owner) {
			this.owner = owner;
			return this;
		}

		public Instant getCreated() {
			return created;
		}

		public Builder created(Instant created) {
			this.created = created;
			return this;
		}

		public Optional<Instant> getLastTriggered() {
			return lastTriggered;
		}

		public Builder lastTriggered(Instant lastTriggered) {
			return lastTriggered(Optional.ofNullable(lastTriggered));
		}

		public Builder lastTriggered(Optional<Instant> lastTriggered) {
			this.lastTriggered = lastTriggered;
			return this;
		}

		public int getTimesTriggered() {
			return timesTriggered;
		}

		public Builder timesTriggered(int timesTriggered) {
			this.timesTriggered = timesTriggered;
			return this;
		}

		public Status getStatus() {
			return status;
		}

		public Builder status(Status status) {
			this.status= status;
			return this;
		}

		public Collection<Condition> getConditions() {
			return conditions;
		}

		public Builder conditions(Collection<? extends Condition> conditions) {
			this.conditions = (Collection<Condition>) conditions;
			return this;
		}

		public Collection<Action> getActions() {
			return actions;
		}

		public Builder actions(Collection<? extends Action> actions) {
			this.actions = (Collection<Action>) actions;
			return this;
		}

		public Rule build() {
			return new Rule(this);
		}
	}

	private Rule(Builder source) {
		this.id = source.id;
		this.name = source.name;
		this.owner = source.owner;
		this.created = source.created;
		this.lastTriggered = source.lastTriggered;
		this.timesTriggered = source.timesTriggered;
		this.status = source.status;
		this.conditions = source.conditions != null ? Collections.unmodifiableList(new ArrayList<>(source.conditions)) : Collections.emptyList();
		this.actions = source.actions != null ? Collections.unmodifiableList(new ArrayList<>(source.actions)) : Collections.emptyList();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getOwner() {
		return owner;
	}

	public Instant getCreated() {
		return created;
	}

	public Optional<Instant> getLastTriggered() {
		return lastTriggered;
	}

	public int getTimesTriggered() {
		return timesTriggered;
	}

	public Status getStatus() {
		return status;
	}

	public List<Condition> getConditions() {
		return conditions;
	}

	public List<Action> getActions() {
		return actions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rule other = (Rule) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Rule [id=" + id + ", name=" + name + ", owner=" + owner + ", created=" + created + ", lastTriggered="
				+ lastTriggered + ", timesTriggered=" + timesTriggered + ", status=" + status + ", conditions="
				+ conditions + ", actions=" + actions + "]";
	}

}
