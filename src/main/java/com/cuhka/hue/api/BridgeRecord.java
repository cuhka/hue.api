package com.cuhka.hue.api;

import java.io.Serializable;

public class BridgeRecord implements BridgeLocator, Serializable {
	private static final long serialVersionUID = -755221616732055206L;
	private final String id;
	private final String internalIPAddress;

	public BridgeRecord(String id, String internalIPAddress) {
		this.id = id;
		this.internalIPAddress = internalIPAddress;
	}

	public String getId() {
		return id;
	}

	@Override
	public String getAddress() {
		return internalIPAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BridgeRecord other = (BridgeRecord) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BridgeRecord [id=");
		builder.append(id);
		builder.append(", internalIPAddress=");
		builder.append(internalIPAddress);
		builder.append("]");
		return builder.toString();
	}
}
