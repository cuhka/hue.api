package com.cuhka.hue.api;


public class InternalErrorInBridgeException extends HueException {
	private static final long serialVersionUID = -1107025446810816294L;

	public InternalErrorInBridgeException(HueError error) {
		super(error);
	}
}
