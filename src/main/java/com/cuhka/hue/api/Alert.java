package com.cuhka.hue.api;

/**
 * The alert effect, which is a temporary change to the bulb’s state.
 */
public enum Alert {
	NONE, SELECT, LSELECT
}
