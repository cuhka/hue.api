package com.cuhka.hue.api;

import java.io.Serializable;
import java.util.Iterator;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Update<T> implements Iterable<Modification>, Serializable {
	private static final long serialVersionUID = -341925673778325497L;
	private final Modifications modifications;
	private final Supplier<? extends T> supplier;
	
	/**
	 * @param modifications
	 * @param updateSupplier
	 */
	public Update(Modifications modifications, Supplier<? extends T> updateSupplier) {
		this.modifications = modifications;
		this.supplier = updateSupplier;
	}
	
	/**
	 * @return modifications returned by the Hue bridge
	 */
	public Modifications getModifications() {
		return modifications;
	}
	
	
	/**
	 * @return if the modifications don't contain an error
	 */
	public boolean isOk() {
		return modifications.isOk();
	}
	
	/**
	 * @return update of original with modifications returned by Hue bridge
	 * @throws HueException if no update supplier was given
	 */
	public T createUpdated() {
		if (supplier != null) {
			return supplier.get();
		}
		
		throw new HueException("No suplier for updated value was provided");
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Update [modifications=");
		builder.append(modifications);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public Iterator<Modification> iterator() {
		return modifications.iterator();
	}
	
	public Stream<Modification> stream() {
		return modifications.stream();
	}
}
