package com.cuhka.hue.api;

import java.util.Optional;

public class HueException extends RuntimeException {
	private static final long serialVersionUID = -2414208877170454117L;
	private Optional<HueError> error = Optional.empty();

	public HueException() {
		super();
	}

	public HueException(String message) {
		super(message);
	}

	public HueException(Throwable cause) {
		super(cause);
	}

	public HueException(String message, Throwable cause) {
		super(message, cause);
	}

	public HueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public HueException(HueError error) {
		super(String.format("(%d) %s: %s", error.getType(), error.getAddress(), error.getDescription()));
		this.error = Optional.of(error);
	}

	public Optional<HueError> getError() {
		return error;
	}
}
