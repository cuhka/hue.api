package com.cuhka.hue.api;

import java.io.IOException;

import com.google.gson.stream.JsonWriter;

class LightAttributesUpdateSerializer extends DataSerializer<LightAttributesUpdate> {
	@Override
	public void write(LightAttributesUpdate update, JsonWriter writer) throws IOException {
		writer.beginObject();
		writeString(writer, "name", update.getName());
		writer.endObject();
	}
}
