package com.cuhka.hue.api;


public class DeviceGroupTableFullException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public DeviceGroupTableFullException(HueError error) {
		super(error);
	}
}
