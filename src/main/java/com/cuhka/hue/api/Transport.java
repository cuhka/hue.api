package com.cuhka.hue.api;

public interface Transport {
	Modifications delete(CharSequence path);
	<T> T get(DataParser<? extends T> parser, CharSequence path);
	<T> Modifications post(T data, DataSerializer<? super T> serializer, CharSequence path);
	<T> Modifications put(T data, DataSerializer<? super T> serializer, CharSequence path);
}
