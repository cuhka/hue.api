package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.getStringAttribute;
import static com.cuhka.hue.api.BuilderUtils.optional;
import static com.cuhka.hue.api.BuilderUtils.required;
import static java.util.Collections.unmodifiableSet;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.cuhka.hue.api.GamutTriangle.Point;

public class Light implements Serializable {
	public final static Comparator<Light> NAME_COMPARATOR = (l1, l2) -> l1.name.compareToIgnoreCase(l2.name);	
	
	private static final long serialVersionUID = -3092303619014055758L;
	public final static Set<String> HUE_MODELIDS;
	public final static Set<String> LIVING_COLORS_MODELIDS;
	public final static Set<String> LUX_MODELIDS;
	
	private static final Map<String, GamutTriangle> GAMMUTS = new HashMap<>();
	private static final GamutTriangle DEFAULT_GAMUT = new GamutTriangle(new Point(1.0,0), new Point(0.0,1.0), new Point(1.0,1.0)); 

	private static void registerGamutPoints(Collection<String> ids, GamutTriangle triangle) {
		ids.forEach(id -> GAMMUTS.put(id, triangle));
	}
	
	private static Set<String> toSet(String... strings) {
		return unmodifiableSet(new LinkedHashSet<>(Arrays.asList(strings)));
	}
		 	
	static {
		HUE_MODELIDS = toSet("LCT001","LCT002", "LCT003");
		LIVING_COLORS_MODELIDS = toSet("LLC005","LLC006","LLC007","LLC011","LLC012","LLC013","LST001");
		LUX_MODELIDS = toSet("LWB004");
		registerGamutPoints(HUE_MODELIDS, new GamutTriangle(new Point(0.674, 0.322), new Point(0.408, 0.517), new Point(0.168, 0.041)));
		registerGamutPoints(LIVING_COLORS_MODELIDS, new GamutTriangle(new Point(0.704, 0.296), new Point(0.2151, 0.7106), new Point(0.138, 0.08)));
	}
	
	public static class Builder implements Source {
		public String id;
		public LightState lightState;
		public String modelId;
		public String name;
		public String manufacturerName;
		public String uniqueId;
		public String softwareVersion;
		public String type;

		@Override
		public String getId() {
			return id;
		}

		@Override
		public LightState getLightstate() {
			return lightState;
		}

		@Override
		public String getModelId() {
			return modelId;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getSoftwareVersion() {
			return softwareVersion;
		}

		@Override
		public String getType() {
			return type;
		}

		@Override
		public String toString() {
			return String.format("Builder [id=%s, lightState=%s, modelId=%s, name=%s, pointsymbols=%s, softwareVersion=%s, type=%s]", id, lightState, modelId,
					name, softwareVersion, type);
		}

		@Override
		public String getManufacturerName() {
			return manufacturerName;
		}

		@Override
		public String getUniqueId() {
			return uniqueId;
		}
		
		public Light build() {
			return new Light(this);
		}
	}

	public interface Source {
		String getId();

		LightState getLightstate();

		String getModelId();

		String getName();

		/** @since Hue API 1.7 */
		String getManufacturerName();
		
		/** @since Hue API 1.4 */
		String getUniqueId();

		String getSoftwareVersion();

		String getType();
	}

	static class Adapter implements Source {
		protected final Light original;
		
		Adapter(Light original) {
			this.original = original;
		}

		@Override
		public String getId() {
			return original.getId();
		}

		@Override
		public LightState getLightstate() {
			return original.getLightState();
		}

		@Override
		public String getModelId() {
			return original.getModelId();
		}

		@Override
		public String getName() {
			return original.getName();
		}

		@Override
		public String getSoftwareVersion() {
			return original.getSoftwareVersion();
		}

		@Override
		public String getType() {
			return original.getType();
		}

		@Override
		public String getManufacturerName() {
			return original.getManufacturerName();
		}

		@Override
		public String getUniqueId() {
			return original.getUniqueId();
		}
	}

	static class MergeAttributes extends Adapter {
		final Modifications modifications;
		
		public MergeAttributes(Light original, Modifications modifications) {
			super(original);
			this.modifications = modifications;
		}

		@Override
		public String getModelId() {
			return getStringAttribute(modifications,"modelid",() -> super.getModelId());
		}

		@Override
		public String getName() {
			return getStringAttribute(modifications,"name",() -> super.getName());
		}

		@Override
		public String getSoftwareVersion() {
			return getStringAttribute(modifications,"swversion",() -> super.getSoftwareVersion());
		}

		@Override
		public String getType() {
			return getStringAttribute(modifications, "type", () -> super.getType());
		}
		
		
	}
	
	static class MergePointSymbols extends Adapter {
		final Modifications modifications;

		MergePointSymbols(Light original, Modifications modifications) {
			super(original);
			this.modifications = modifications;
		}
	}		
	
	static class MergeLightState extends Adapter {
		private final Modifications modifications;
		
		MergeLightState(Light original, Modifications modifications) {
			super(original);
			this.modifications = modifications;
		}

		@Override
		public LightState getLightstate() {
			return new LightState(new LightState.UpdateMerger(super.getLightstate(), modifications));
		}
	}
	
	

	private final String id;
	private final String name;
	private final String modelId;
	private final String manufacturerName;
	private final String uniqueId;
	private final String softwareVersion;
	private final LightState state;
	private final String type;

	public Light(Source source) {
		this.id = required(source.getId(), "id");
		this.name = optional(source.getName());
		this.state = source.getLightstate();
		this.type = optional(source.getType());
		this.manufacturerName = optional(source.getManufacturerName());
		this.uniqueId = optional(source.getUniqueId());
		this.modelId = optional(source.getModelId());
		this.softwareVersion = optional(source.getSoftwareVersion());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Light other = (Light) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getId() {
		return id;
	}

	public String getModelId() {
		return modelId;
	}

	public String getName() {
		return name;
	}

	/** @since Hue API 1.4 */
	public String getUniqueId() {
		return uniqueId;
	}

	/** @since Hue API 1.7 */
	public String getManufacturerName() {
		return manufacturerName;
	}	


	public String getSoftwareVersion() {
		return softwareVersion;
	}

	public LightState getLightState() {
		return state;
	}

	public String getType() {
		return type;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		return type + " [id=" + id + ", name=" + name + ", modelId=" + modelId + "]";
	}



	/**
	 * @see <a href="https://github.com/PhilipsHue/PhilipsHueSDK-iOS-OSX/blob/master/ApplicationDesignNotes/RGB%20to%20xy%20Color%20conversion.md">Color conversion formulas RGB to xy and back</a>
	 */
	public GamutTriangle getGamutTriangle() {
		return getGamutTriangle(modelId);
	}
	
	public static GamutTriangle getGamutTriangle(String modelId) {
		return GAMMUTS.getOrDefault(modelId, DEFAULT_GAMUT);
	}
}
 