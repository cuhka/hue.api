package com.cuhka.hue.api;

public class UnauthorizedUserException extends HueException {
	private static final long serialVersionUID = 1714127674547416255L;

	public UnauthorizedUserException(HueError error) {
		super(error);
	}
}
