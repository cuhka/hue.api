package com.cuhka.hue.api;

public interface BridgeLocator {
	String getAddress();
}
