package com.cuhka.hue.api;

import java.time.Duration;
import java.util.Optional;

public class RecurringTimer extends Timer {
	private static final long serialVersionUID = -5591155566251794163L;
	private final Optional<Integer> occurences;

	public RecurringTimer(Duration duration) {
		this(duration, Optional.empty());
	}

	public RecurringTimer(Duration duration, Duration randomize) {
		this(duration, randomize, Optional.empty());
	}

	public RecurringTimer(Duration duration, int occurences) {
		this(duration, Optional.of(Integer.valueOf(occurences)));
	}

	public RecurringTimer(Duration duration, Optional<Integer> occurences) {
		this(duration, Duration.ZERO, occurences);
	}

	public RecurringTimer(Duration duration, Duration randomize, int occurences) {
		this(duration, randomize, Optional.of(Integer.valueOf(occurences)));
	}

	public RecurringTimer(Duration duration, Duration randomize, Optional<Integer> occurences) {
		super(duration, randomize);
		this.occurences = occurences;
	}

	public Optional<Integer> getOccurences() {
		return occurences;
	}

	@Override
	protected void apiValue(StringBuilder builder) {
		builder.append("R");

		if (occurences.isPresent()) {
			builder.append(String.format("%02d", occurences.get()));
		}
		builder.append("/");
		super.apiValue(builder);
	}
}
