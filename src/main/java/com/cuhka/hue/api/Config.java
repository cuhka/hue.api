package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.SUPPLY_ZERO;
import static com.cuhka.hue.api.BuilderUtils.optional;
import static com.cuhka.hue.api.BuilderUtils.optionalSet;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public class Config implements Serializable {
	private static final long serialVersionUID = -419701577263218223L;

	public static class Builder implements Source {
		public String name;
		public Instant utc;
		public LocalDateTime localtime;
		public ZoneId timezone;
		public String mac;
		public Boolean dhcp;
		public String ipaddress;
		public String netmask;
		public String gateway;
		public String proxyaddress;
		public Integer proxyport;
		public Set<ApiRegistration> whitelist;
		public String swversion;
		public SoftwareUpdate swudpdate;
		public String apiversion;
		public Boolean linkbutton;
		public Boolean portalservices;
		public Boolean portalconnection;
		public PortalState portalstate;
		public Integer zigbeechannel;

		@Override
		public String getApiVersion() {
			return apiversion;
		}

		@Override
		public LocalDateTime getLocalTime() {
			return localtime;
		}

		@Override
		public ZoneId getTimeZone() {
			return timezone;
		}

		@Override
		public Boolean getDhcp() {
			return dhcp;
		}

		@Override
		public String getGateway() {
			return gateway;
		}

		@Override
		public String getIpAddress() {
			return ipaddress;
		}

		@Override
		public String getMacAddress() {
			return mac;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getNetmask() {
			return netmask;
		}

		@Override
		public Boolean getPortalConnection() {
			return portalconnection;
		}

		@Override
		public Boolean getPortalServices() {
			return portalservices;
		}

		@Override
		public String getProxyAddress() {
			return proxyaddress;
		}

		@Override
		public Integer getProxyPort() {
			return proxyport;
		}

		@Override
		public SoftwareUpdate getSoftwareUpdate() {
			return swudpdate;
		}

		@Override
		public PortalState getPortalState() {
			return portalstate;
		}

		@Override
		public Collection<? extends ApiRegistration> getWhitelist() {
			return whitelist;
		}

		@Override
		public Instant getUTC() {
			return utc;
		}

		@Override
		public String getSoftwareVersion() {
			return swversion;
		}

		@Override
		public Integer getZigbeeChannel() {
			return zigbeechannel;
		}
		
		public Config build() {
			return new Config(this);
		}

	}

	public interface Source {
		String getApiVersion();

		Integer getZigbeeChannel();
		
		LocalDateTime getLocalTime();

		ZoneId getTimeZone();

		Boolean getDhcp();

		String getGateway();

		String getIpAddress();

		String getMacAddress();

		String getName();

		String getNetmask();

		Boolean getPortalConnection();

		Boolean getPortalServices();

		String getProxyAddress();

		Integer getProxyPort();

		SoftwareUpdate getSoftwareUpdate();

		PortalState getPortalState();

		Collection<? extends ApiRegistration> getWhitelist();

		Instant getUTC();

		String getSoftwareVersion();

	}

	public static class Proxy {
		private final String ipaddress;
		private final int port;

		Proxy(String ipaddress, int port) {
			this.ipaddress = ipaddress;
			this.port = port;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Proxy other = (Proxy) obj;
			if (ipaddress == null) {
				if (other.ipaddress != null)
					return false;
			} else if (!ipaddress.equals(other.ipaddress))
				return false;
			if (port != other.port)
				return false;
			return true;
		}

		public final String getIpaddress() {
			return ipaddress;
		}

		public final int getPort() {
			return port;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = port;
			result = prime * result + ipaddress.hashCode();
			result = prime * result + port;

			return result;
		}

		@Override
		public String toString() {
			return "Proxy [ipaddress=" + ipaddress + ", port=" + port + "]";
		}
	}

	public static class PortalState {
		public enum Communication {
			DISCONNECTED, CONNECTING, CONNECTED
		}

		public static class Builder implements Source {
			public Boolean signedOn;
			public Boolean incoming;
			public Boolean outgoing;
			public Communication communication;

			@Override
			public Boolean isSignedOn() {
				return signedOn;
			}

			@Override
			public Boolean hasIncomingConnection() {
				return incoming;
			}

			@Override
			public Boolean hasOutgoingConnection() {
				return outgoing;
			}

			@Override
			public Communication getCommunication() {
				return communication;
			}
		}

		public interface Source {
			Boolean isSignedOn();

			Boolean hasIncomingConnection();

			Boolean hasOutgoingConnection();

			Communication getCommunication();

		}

		private boolean signedOn;
		private boolean incoming;
		private boolean outgoing;
		private Communication communication;

		public PortalState(Source source) {
			signedOn = optional(source.isSignedOn(), () -> Boolean.TRUE);
			incoming = optional(source.hasIncomingConnection(), () -> Boolean.TRUE);
			outgoing = optional(source.hasOutgoingConnection(), () -> Boolean.TRUE);
			communication = optional(source.getCommunication(), () -> Communication.CONNECTED);

		}

		public boolean isSignedOn() {
			return signedOn;
		}

		public boolean isIncoming() {
			return incoming;
		}

		public boolean isOutgoing() {
			return outgoing;
		}

		public Communication getCommunication() {
			return communication;
		}
	}

	private final int zigbeeChannel;
	private final String apiversion;
	private final LocalDateTime localtime;
	private final ZoneId timezone;
	private final boolean dhcp;
	private final String gateway;
	private final String ipaddress;
	private final boolean linkbutton;
	private final String mac;
	private final String name;
	private final String netmask;
	private final Optional<Proxy> proxy;
	private final Optional<SoftwareUpdate> softwareUpdate;

	private final String swversion;
	private final Instant utc;
	private final Set<ApiRegistration> whitelist;
	private final boolean portalservices;
	private final boolean portalconnection;
	private final PortalState portalState;

	public Config(Source source) {
		this.zigbeeChannel = optional(source.getZigbeeChannel(), SUPPLY_ZERO);
		this.apiversion = optional(source.getApiVersion());
		this.localtime = source.getLocalTime();
		this.timezone = optional(source.getTimeZone(), () -> ZoneOffset.UTC);
		this.dhcp = optional(source.getDhcp(), () -> Boolean.TRUE);
		this.gateway = optional(source.getGateway());
		this.ipaddress = optional(source.getIpAddress());
		this.linkbutton = optional(source.getDhcp(), () -> Boolean.FALSE);
		this.mac = optional(source.getMacAddress());
		this.name = optional(source.getName());
		this.netmask = optional(source.getNetmask());
		this.portalconnection = optional(source.getPortalConnection(), () -> Boolean.TRUE);
		this.portalservices = optional(source.getPortalServices(), () -> Boolean.TRUE);

		String proxyAddress = source.getProxyAddress();

		Proxy proxy = null;
		if (proxyAddress != null && !proxyAddress.isEmpty()) {
			Integer port = source.getProxyPort();
			proxy = new Proxy(proxyAddress, port != null ? port : 80);
		}
		this.proxy = Optional.ofNullable(proxy);
		this.softwareUpdate = Optional.ofNullable(source.getSoftwareUpdate());
		this.portalState = source.getPortalState();
		this.whitelist = optionalSet(source.getWhitelist());
		this.utc = source.getUTC();;
		this.swversion = source.getSoftwareVersion();
	}

	public String getAPIVersion() {
		return apiversion;
	}

	public ZonedDateTime getLocalDateTime() {
		return ZonedDateTime.of(localtime, timezone);
	}

	public ZoneId getTimeZone() {
		return timezone;
	}

	public String getBridgeName() {
		return name;
	}

	public String getGateway() {
		return gateway;
	}

	public String getIPAddress() {
		return ipaddress;
	}

	public String getMacAddress() {
		return mac;
	}

	public String getNetmask() {
		return netmask;
	}

	public Optional<Proxy> getProxy() {
		return proxy;
	}

	public Optional<SoftwareUpdate> getSoftwareUpdate() {
		return softwareUpdate;
	}

	public String getSoftwareVersion() {
		return swversion;
	}

	public Instant getUTC() {
		return utc;
	}

	public Set<ApiRegistration> getWhitelist() {
		return whitelist;
	}

	public boolean isDhcp() {
		return dhcp;
	}

	public boolean isLinkButtonPressed() {
		return linkbutton;
	}

	public boolean isSynchronizingToPortal() {
		return portalservices;
	}

	public boolean isConnectedToPortal() {
		return portalconnection;
	}

	public PortalState getPortalState() {
		return portalState;
	}

	/**
	 * @since Hue-Api 1.3.0
	 * @return
	 */
	public int getZigbeeChannel() {
		return zigbeeChannel;
	}
	
	public Optional<ApiRegistration> findApiRegistrationByName(String name) {
		return whitelist.stream().filter((r) -> r.getName().equals(name)).findAny();
	}
	
	public Optional<ApiRegistration> findApiRegistrationById(String id) {
		return whitelist.stream().filter((r) -> r.getId().equals(id)).findAny();
	}
}