package com.cuhka.hue.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.stream.JsonReader;

class SceneParser extends DataParser<Scene> {
	private final String id;

	public SceneParser(String sceneId) {
		this.id = sceneId;
	}

	@Override
	protected Scene parseObject(JsonReader reader) throws IOException {
		Scene.Builder builder = new Scene.Builder();
		builder.id(id);

		reader.beginObject();

		while (reader.hasNext()) {
			String name = reader.nextName();
			switch (name) {
			case "name":
				builder.name(reader.nextString());
				break;

			case "lights":
				builder.lights(readIds(reader));
				break;

			case "owner":
				builder.owner(reader.nextString());
				break;

			case "recycle":
				builder.recycle(reader.nextBoolean());
				break;

			case "locked":
				builder.locked(reader.nextBoolean());
				break;

			case "appdata":
				builder.appData(readAppData(reader));
				break;

			case "picture":
				builder.picture(reader.nextString());
				break;

			case "version":
				builder.version(reader.nextInt());
				break;

			case "lightstates":
				builder.lightstates(readLightStates(reader));
				break;

			default:
				reader.skipValue();
				break;
			}
		}

		reader.endObject();
		return new Scene(builder);
	}

	private AppData readAppData(JsonReader reader) throws IOException {
		reader.beginObject();
		AppData.Builder builder = reader.hasNext() ? new AppData.Builder() : null;
		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "version":
				builder.version(reader.nextInt());
				break;

			case "data":
				builder.data(reader.nextString());
				break;

			default:
				reader.skipValue();
				break;
			}
		}

		reader.endObject();
		
		return builder != null ? builder.build() : null;
	}

	private Map<String, LightState> readLightStates(JsonReader reader) throws IOException {
		Map<String, LightState> lightstates = new LinkedHashMap<>();
		LightStateParser parser = new LightStateParser();

		reader.beginObject();

		while (reader.hasNext()) {
			String lightId = reader.nextName();
			LightState lightState = parser.parse(reader);
			lightstates.put(lightId, lightState);
		}

		reader.endObject();

		return lightstates;
	}

	private Collection<String> readIds(JsonReader reader) throws IOException {
		Collection<String> ids = new ArrayList<>();

		reader.beginArray();
		while (reader.hasNext()) {
			ids.add(reader.nextString());
		}
		reader.endArray();

		return ids;
	}

}
