package com.cuhka.hue.api;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Scenes extends ImmutableSet<Scene> {
	public Scenes(Collection<? extends Scene> scenes) {
		super(scenes);
	}
	
	public Scenes filter(Predicate<? super Scene> p) {
		return new Scenes(stream().filter(p).collect(Collectors.toSet()));
	}

	public Scenes filterIds(String... ids) {
		return filterIds(Arrays.asList(ids));
	}

	public Scenes filterIds(Collection<String> ids) {
		return filter((l) -> ids.contains(l.getId()));
	}

	public Optional<Scene> find(Predicate<? super Scene> p) {
		return stream().filter(p).findAny();
	}

	public Optional<Scene> findById(String id) {
		return find((l) -> l.getId().equals(id));
	}

	public Optional<Scene> findByName(String name) {
		return find((l) -> l.getName().equals(name));
	}	
}
