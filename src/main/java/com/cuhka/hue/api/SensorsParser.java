package com.cuhka.hue.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.stream.JsonReader;

public class SensorsParser extends DataParser<Sensors> {

	@Override
	protected Sensors parseObject(JsonReader reader) throws IOException {
		Collection<Sensor> sensors = new ArrayList<>();
		reader.beginObject();
		
		while (reader.hasNext()) {
			String id = reader.nextName();
			SensorParser parser = new SensorParser(id);
			
			sensors.add(parser.parse(reader));
		}
		
		reader.endObject();
		return new Sensors(sensors);
	}

}
