package com.cuhka.hue.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;

import com.google.gson.stream.JsonReader;

class CollectionParser<T,R> extends DataParser<R> {
	
	private final Function<String, DataParser<? extends T>> parseItem;
	private final Function<Collection<? super T>,R> createResult;
	
	public CollectionParser(Function<String, DataParser<? extends T>> parseItem,
			Function<Collection<? super T>, R> createResult) {
		this.parseItem = parseItem;
		this.createResult = createResult;
	}
	
	@Override
	protected R parseObject(JsonReader reader) throws IOException {
		Collection<T> items = new ArrayList<>();
		reader.beginObject();
		
		while (reader.hasNext()) {
			String id = reader.nextName();
			DataParser<? extends T> parser = parseItem.apply(id);
			T item = parser.parse(reader);
			items.add(item);
		}
		
		reader.endObject();
		
		return createResult.apply(items);
	}
}
