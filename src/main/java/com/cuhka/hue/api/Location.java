package com.cuhka.hue.api;

public enum Location {
	LIVING_ROOM("Living room"),
	KITCHEN("Kitchen"),
	DINING("Dining"),
	BEDROOM("Bedroom"),
	KIDS_BEDROOM("Kids bedroom"),
	BATHROOM("Bathroom"),
	NURSERY("Nursery"),
	RECREATION("Recreation"),
	OFFICE("Office"),
	GYM("Gym"),
	HALLWAY("Hallway"),
	FRONT_DOOR("Front door"),
	GARAGE("Garage"),
	TERRACE("Terrace"),
	GARDEN("Garden"),
	DRIVEWAY("Driveway"),
	CARPORT("Carport"),
	OTHER("Other");

	public static Location fromClassString(String value) {
		for (Location type : values()) {
			if (type.roomclass.equals(value)) {
				return type;
			}
		}
		return OTHER;
	}
	
	private Location(String roomclass) {
		this.roomclass = roomclass;
	}
	
	public String getRoomClass() {
		return roomclass;
	}

	private final String roomclass;
	
	@Override
	public String toString() {
		return roomclass;
	}
}
