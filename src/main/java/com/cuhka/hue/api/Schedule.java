package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.getBooleanAttribute;
import static com.cuhka.hue.api.BuilderUtils.getStringAttribute;
import static com.cuhka.hue.api.BuilderUtils.optional;
import static com.cuhka.hue.api.BuilderUtils.required;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

public class Schedule implements Serializable {
	private static final long serialVersionUID = -7064835982820927838L;
	static class MergeAttributes implements Source {
		private final Schedule original;
		private final Modifications modification;

		MergeAttributes(Schedule original, Modifications modification) {
			this.original = original;
			this.modification = modification;
		}

		@Override
		public Action getCommand() {
			return original.getCommand();
		}

		@Override
		public LocalDateTime getCreated() {
			return original.getCreated();
		}

		@Override
		public String getDescription() {
			return getStringAttribute(modification, "description", () -> original.getDescription());
		}

		@Override
		public String getId() {
			return original.getId();
		}

		@Override
		public String getName() {
			return getStringAttribute(modification, "name", () -> original.getName());
		}

		@Override
		public Status getStatus() {
			Optional<Modification> attribute = modification.findAttribute("status");

			if (attribute.isPresent()) {
				return Status.valueOf(attribute.get().getStringValue().toUpperCase());
			}

			return original.getStatus();
		}
		
		@Override
		public TimeTrigger getTimeTrigger() {
			return original.getTimeTrigger();
		}

		@Override
		public Boolean getAutodelete() {
			return getBooleanAttribute(modification, "autodelete", () -> original.isAutodelete());
		}
	}

	public static class Builder implements Source {
		public Action command;
		public LocalDateTime created;
		public String description;
		public String id;
		public String name;
		public Status status;
		public TimeTrigger recurringTimes;
		public Boolean autodelete;
		
		public Builder() {
		}

		public Builder(String id) {
			this.id = id;
		}

		@Override
		public Action getCommand() {
			return command;
		}

		@Override
		public LocalDateTime getCreated() {
			return created;
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public Status getStatus() {
			return status;
		}

		@Override
		public TimeTrigger getTimeTrigger() {
			return recurringTimes;
		}
		
		@Override
		public Boolean getAutodelete() {
			return autodelete;
		}
		
		public Schedule build() {
			return new Schedule(this);
		}
	}

	public interface Source {
		String getId();
		Action getCommand();
		LocalDateTime getCreated();
		String getDescription();
		String getName();
		Status getStatus();
		TimeTrigger getTimeTrigger();

		Boolean getAutodelete();
	}

	public static enum Status {
		DISABLED, ENABLED
	}

	private final Action command;
	private final LocalDateTime created;
	private final String description;
	private final String id;
	private final String name;
	private final Status status;
	private final TimeTrigger timeTrigger;
	private final boolean autodelete;

	public Schedule(Source source) {
		this.id = required(source.getId(), "id");
		this.command = source.getCommand();
		this.created = optional(source.getCreated(),LocalDateTime::now);
		this.description = optional(source.getDescription());
		this.name = optional(source.getName());
		this.status = optional(source.getStatus(), () -> Status.ENABLED);
		this.timeTrigger = source.getTimeTrigger();
		this.autodelete = optional(source.getAutodelete(), () -> Boolean.TRUE);
	}

	public TimeTrigger getTimeTrigger() {
		return timeTrigger;
	}

	public Action getCommand() {
		return command;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public String getDescription() {
		return description;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Status getStatus() {
		return status;
	}

	public boolean isAutodelete() {
		return autodelete;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Schedule [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);

		builder.append("]");
		return builder.toString();
	}

}
