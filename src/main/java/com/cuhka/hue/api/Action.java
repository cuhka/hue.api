package com.cuhka.hue.api;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class Action implements Serializable {
	private static final long serialVersionUID = 204320339013826335L;
	public enum Method {
		POST, PUT, DELETE
	}
	
	private final String address;
	private final Method method;
	private final Map<String, ?> body;
	
	public static class Builder {
		private String address;
		private Method method;
		private Map<String, ?> body;
		
		public Builder address(String address) {
			this.address = address;
			return this;
		}
		
		public String getAddress() {
			return address;
		}
		
		public Builder method(Method method) {
			this.method = method;
			return this;
		}
		
		public Builder body(String key, String value) {
			return body(Collections.singletonMap(key, value));
		}
		
		public Builder body(Map<String, ?> body) {
			this.body = body;
			return this;
		}
		
		public <T> Map<String, T> getBody() {
			@SuppressWarnings("unchecked")
			Map<String, T> map = (Map<String, T>) this.body;
			return map;
		}
		
		public Action build() {
			return new Action(this);
		}
	}
	
	private Action(Builder builder) {
		this.address = Objects.requireNonNull(builder.address, "address");
		this.method = Objects.requireNonNull(builder.method, "method");
		this.body = BuilderUtils.optionalMap(builder.body);
	}

	public String getAddress() {
		return address;
	}
	
	public Method getMethod() {
		return method;
	}
	
	public Map<String, ?> getBody() {
		return body;
	}
}
