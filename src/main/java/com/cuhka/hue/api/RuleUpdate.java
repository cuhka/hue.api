package com.cuhka.hue.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class RuleUpdate {
	private final Optional<String> name;
	private final Optional<Rule.Status> status;
	private final Optional<List<Condition>> conditions;
	private final Optional<List<Action>> actions;
	
	public static class Builder {
		private String name;
		private Rule.Status status;
		private Collection<? extends Condition> conditions;
		private Collection<? extends Action> actions;

		public Builder as(Rule rule) {
			this.name = rule.getName();
			this.conditions = rule.getConditions();
			this.actions = rule.getActions();
			this.status = rule.getStatus();
			
			return this;
		}
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public String getName() {
			return name;
		}
		
		public Builder status(Rule.Status status) {
			this.status = status;
			return this;
		}
		
		public Rule.Status getStatus() {
			return status;
		}
		
		public Builder condition(Condition c) {
			return conditions(Collections.singletonList(c));
		}
		
		public Builder conditions(Collection<? extends Condition> conditions) {
			this.conditions = conditions;
			return this;
		}
		
		@SuppressWarnings("unchecked")
		public <C extends Condition> Collection<C> getConditions() {
			return (Collection<C>) this.conditions;
		}
		
		public Builder action(Action action) {
			return this.actions(Collections.singletonList(action));
		}
		
		public Builder actions(Collection<? extends Action> actions) {
			this.actions = actions;
			return this;
		}
		
		@SuppressWarnings("unchecked")
		public <A extends Action> Collection<A> getActions() {
			return (Collection<A>) actions;
		}
		
		public RuleUpdate build() {
			return new RuleUpdate(this);
		}
	}
	
	private RuleUpdate(Builder builder) {
		this.name = Optional.ofNullable(builder.name);
		this.status = Optional.ofNullable(builder.status);
		this.conditions = Optional.ofNullable(builder.conditions).map(c -> Collections.unmodifiableList(new ArrayList<>(c)));
		this.actions = Optional.ofNullable(builder.actions).map(c -> Collections.unmodifiableList(new ArrayList<>(c)));
	}

	/**
	 * @return the name
	 */
	public final Optional<String> getName() {
		return name;
	}

	/**
	 * @return the status
	 */
	public final Optional<Rule.Status> getStatus() {
		return status;
	}

	/**
	 * @return the conditions
	 */
	public final Optional<List<Condition>> getConditions() {
		return conditions;
	}

	/**
	 * @return the actions
	 */
	public final Optional<List<Action>> getActions() {
		return actions;
	}
}
