package com.cuhka.hue.api;

import java.io.Serializable;
import java.util.Optional;

public class LightAttributesUpdate implements Serializable {
	private static final long serialVersionUID = -2898444374344960857L;
	private final String name;
	
	public static class Builder {
		private String name;
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Optional<String> getName() {
			return Optional.ofNullable(name);
		}
		
		public LightAttributesUpdate build() {
			return new LightAttributesUpdate(this);
		}
	}
	
	private LightAttributesUpdate(Builder builder) {
		this.name = builder.name;
	}

	public Optional<String> getName() {
		return Optional.ofNullable(name);
	}
}
