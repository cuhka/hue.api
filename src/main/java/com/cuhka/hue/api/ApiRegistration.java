package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.optional;
import static com.cuhka.hue.api.BuilderUtils.required;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ApiRegistration implements Serializable {
	private static final long serialVersionUID = 7591776907392677778L;

	public static class Builder implements Source {
		public LocalDateTime created;
		public String id;
		public LocalDateTime lastUsed;
		public String name;

		@Override
		public LocalDateTime getCreated() {
			return created;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public LocalDateTime getLastUsed() {
			return lastUsed;
		}

		@Override
		public String getName() {
			return name;
		}
		
		public ApiRegistration build() {
			return new ApiRegistration(this);
		}
	}

	public interface Source {
		String getId();
		String getName();
		LocalDateTime getCreated();
		LocalDateTime getLastUsed();
	}

	private final LocalDateTime created;
	private final String id;
	private final LocalDateTime lastUsed;
	private final String name;

	public ApiRegistration(Source source) {
		this.id = required(source.getId(), "id");
		this.name = required(source.getName(), "name");
		this.lastUsed = optional(source.getLastUsed(),() -> LocalDateTime.MIN);
		this.created = optional(source.getCreated(), () -> LocalDateTime.MIN);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApiRegistration other = (ApiRegistration) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public String getId() {
		return id;
	}

	public LocalDateTime getLastUsed() {
		return lastUsed;
	}

	public final String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
