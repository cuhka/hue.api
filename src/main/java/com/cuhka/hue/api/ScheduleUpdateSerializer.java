package com.cuhka.hue.api;

import java.io.IOException;
import java.util.Optional;

import com.google.gson.stream.JsonWriter;

public class ScheduleUpdateSerializer extends DataSerializer<ScheduleUpdate> {
	@Override
	public void write(ScheduleUpdate data, JsonWriter writer) throws IOException {
		writer.beginObject();
		writeString(writer, "name", data.getName());
		writeString(writer, "description", data.getDescription());
		
		if (data.getCommand().isPresent()) {
			writer.name("command");
			writeAction(writer, data.getCommand().get());
		}
		
		writeLocalTime(writer, "localtime", data.getLocaltime());
		
		writer.endObject();
	}

	private void writeLocalTime(JsonWriter writer, String string, Optional<TimeTrigger> localtime) throws IOException {
		if (localtime.isPresent()) {
			writer.name("localtime").value(localtime.get().formatApi());
		}
	}
}
