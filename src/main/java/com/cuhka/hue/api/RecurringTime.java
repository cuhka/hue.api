package com.cuhka.hue.api;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

public class RecurringTime extends TimeTrigger {
		private final Set<DayOfWeek> days;
		private final LocalTime time;
		private final Duration randomize;
		
		public RecurringTime(Collection<DayOfWeek> days, LocalTime time) {
			this(days, time, Duration.ZERO);
		}
		
		public RecurringTime(Collection<DayOfWeek> days, LocalTime time, Duration randomize) {
			EnumSet<DayOfWeek> set = EnumSet.noneOf(DayOfWeek.class);
			set.addAll(Objects.requireNonNull(days, "days"));
			this.days = Collections.unmodifiableSet(set);
			this.time = Objects.requireNonNull(time, "time");
			this.randomize = Objects.requireNonNull(randomize, "randomize");
		}
		
		public boolean isRandomized() {
			return !randomize.isZero();
		}

		public Set<DayOfWeek> getDays() {
			return days;
		}

		public LocalTime getTime() {
			return time;
		}

		public Duration getRandomize() {
			return randomize;
		}

		@Override
		protected void apiValue(StringBuilder builder) {
			int mask = 0;
			
			for (DayOfWeek day : DayOfWeek.values()) {
				mask <<= 1;
				if (days.contains(day)) {
					mask |= 1;
				}
			}
			
			builder.append(String.format("W%1$03d/T%2$tH:%2$tM:%2$tS", mask, time));
			appendRandom(builder, randomize);
			
		}
	}
