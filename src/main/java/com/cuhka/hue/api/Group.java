package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.getStringAttribute;
import static com.cuhka.hue.api.BuilderUtils.optional;
import static com.cuhka.hue.api.BuilderUtils.optionalSet;
import static com.cuhka.hue.api.BuilderUtils.required;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;

/**
 * @see <a href="http://www.developers.meethue.com/documentation/groups-api"/>Hue API: Groups</a>
 */
public class Group implements  Serializable {
	private static final long serialVersionUID = 7587762979806074461L;
	public final static Comparator<Group> NAME_COMPARATOR = (g1, g2) -> g1.name.compareToIgnoreCase(g2.name);	

	/** @since Hue API 1.4 */
	public static final String LUMINAIRE = "Luminaire";
	
	/** @since Hue API 1.4 */
	public static final String LIGHTSOURCE = "LightSource";
	
	/** @since Hue API 1.4 */
	public static final String LIGHTGROUP = "LightGroup";
	
	public static class Builder implements Serializable, Source {
		private static final long serialVersionUID = 812285246882431988L;
		public String id;
		public String name;
		public Collection<String> lightIds;
		public LightState lightState;
		public String modelId;
		public GroupType type;
		public Location locationClass;
		
		public Builder() {
		}

		public Builder(String id) {
			this.id = id;
		}

		public Builder id(String id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder lightIds(Collection<String> lightIds) {
			this.lightIds = lightIds;
			return this;
		}

		public Builder lightState(LightState lightState) {
			this.lightState = lightState;
			return this;
		}

		public Builder type(GroupType type) {
			this.type = type;
			return this;
		}

		public Builder modelId(String modelId) {
			this.modelId = modelId;
			return this;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public String getName() {
			return name;
		}

		/** @since Hue API 1.4 */
		@Override
		public GroupType getType() {
			return type;
		}
		
		/** @since Hue API 1.4 */
		@Override
		public Collection<String> getLightIds() {
			return lightIds;
		}

		/** @since Hue API 1.4 */
		@Override
		public String getModelId() {
			return modelId;
		}
		
		/** @since Hue API 1.4 */
		@Override
		public LightState getLightState() {
			return lightState;
		}
		

		public Builder locationClass(Location locationClass) {
			this.locationClass = locationClass;
			return this;
		}
		
		@Override 
		public Location getLocationClass() {
			return locationClass;
		}
		
		public Group build() {
			return new Group(this);
		}

		@Override
		public String toString() {
			return "Group.Builder [id=" + id + ", name=" + name + ", lightIds=" + lightIds + ", lightState=" + lightState
					+ ", modelId=" + modelId + ", type=" + type + ", locationClass=" + locationClass + "]";
		}
	}

	static class AttributesMerge implements Source {
		private final Group original;
		private final Modifications modifications;
		
		AttributesMerge(Group original, Modifications modificiations) {
			this.original = original;
			this.modifications = modificiations;
		}

		@Override
		public String getId() {
			return original.id;
		}

		@Override
		public GroupType getType() {
			return modifications.findAttribute("type")
					.map(m -> m.getStringValue())
					.map(s -> GroupType.fromGroupType(s))
					.orElse(original.getType());
		}
		
		@Override
		public String getName() {
			return getStringAttribute(modifications, "name",() -> original.getName());
		}

		@Override
		public Collection<String> getLightIds() {
			Optional<Modification> mod = modifications.findAttribute("lights");
			return mod.isPresent() ? mod.get().getStringListValue() : original.getLightIds();
		}

		@Override
		public LightState getLightState() {
			return original.getLightState();
		}

		@Override
		public String getModelId() {
			return original.getModelId().orElse(null);
		}
		
		@Override
		public Location getLocationClass() {
			return modifications.findAttribute("class")
					.map(m -> m.getStringValue())
					.map(s -> Location.fromClassString(s))
					.orElse(original.getLocation());
		}
	}
	
	public interface Source {
		String getModelId();
		String getId();
		String getName();
		GroupType getType(); 
		LightState getLightState();
		Collection<String> getLightIds();
		Location getLocationClass();
	}

	private final String id;
	private final String name;
	private final Set<String> lights;
	private final LightState lightstate;
	private final GroupType type;
	private final Location locationClass;
	
	private Optional<String> modelId = Optional.empty();

	public Group(Source source) {
		this.id = required(source.getId(), "id");
		this.name = optional(source.getName());
		this.lightstate = optional(source.getLightState(),() -> null);
		this.lights = optionalSet(source.getLightIds());
		this.type = optional(source.getType(),() -> GroupType.LIGHTGROUP);
		this.modelId = Optional.ofNullable(source.getModelId());
		this.locationClass = optional(source.getLocationClass(), () -> Location.OTHER);
	}

	public Location getLocation() {
		return locationClass;
	}

	/** 
	 * Returns the model id for {@link #LUMINAIRE} groups.
	 * @since Hue API 1.4 
	 */
	public Optional<String> getModelId() {
		return modelId;
	}
	

	public String getName() {
		return name;
	}

	/** @since Hue API 1.4 */
	public GroupType getType() {
		return type;
	}
	
	/** @since Hue API 1.4 */
	public Set<String> getLightIds() {
		return lights;
	}

	public String getId() {
		return id;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Group [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", light-ids=");
		builder.append(lights);
		builder.append("]");
		return builder.toString();
	}

	/** @since Hue API 1.4 */
	public LightState getLightState() {
		return lightstate;
	}
}
