package com.cuhka.hue.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.stream.JsonReader;

public class GroupsParser extends DataParser<Groups> {
	@Override
	protected Groups parseObject(JsonReader reader)
			throws IOException {
		Collection<Group> index = new ArrayList<>();
		reader.beginObject();
		
		while (reader.hasNext()) {
			String id = reader.nextName();
			GroupParser parser = new GroupParser(id);
			index.add(parser.parse(reader));
		}
		
		reader.endObject();
		return new Groups(index);
	}

}
