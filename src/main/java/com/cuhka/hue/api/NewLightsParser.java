package com.cuhka.hue.api;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;

import com.google.gson.stream.JsonReader;

class NewLightsParser extends DataParser<NewLights> {
	@Override
	protected NewLights parseObject(JsonReader reader) throws IOException {
		reader.beginObject();
		LocalDateTime lastscanned = null;
		Collection<LightSummary> found = new LinkedList<>();
		boolean scanning = false;
		String lastscanText = "";
		while (reader.hasNext()) {
			String name = reader.nextName();
			
			if (name.equals("lastscan")) {
				
				lastscanText = reader.nextString();
				switch (lastscanText) {
				case "active":
					lastscanned = null;
					scanning = true;
					break;

				case "none":
					lastscanned = null;
					scanning = false;
					break;
					
				default:
					lastscanned = LocalDateTime.parse(lastscanText);
					scanning = false;
					break;
				}
			} else {
				found.add(new LightSummary(name, readName(reader)));
			}
		}
		
		reader.endObject();
		
		return new NewLights(found,Optional.ofNullable(lastscanned), lastscanText, scanning);
	}

	private String readName(JsonReader reader) throws IOException {
		String name = "";
		reader.beginObject();
		
		while (reader.hasNext()) {
			if (reader.nextName().equals("name")) {
				name = reader.nextString();
			}
		}
		
		reader.endObject();
		
		return name;
	}
}
