package com.cuhka.hue.api;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * @see <a href="http://developers.meethue.com/">Philips Hue Api</a>
 */
public class Bridge {
	private static final String SENSORS = "/sensors/";
	private static final String SCENES = "/scenes/";
	private static final String ACTION = "/action";
	private static final String GROUPS = "/groups/";
	private static final String LIGHTS = "/lights/";
	private static final String STATE = "/state";
	private static final String RULES = "/rules/";
	private static final String SCHEDULES = "/schedules/";
	
	private final Transport transport;
	
	public Bridge(String host, int port, String username) {
		this(new HttpConnectionTransport(host, port, username));
	}

	public Bridge(String host, String username) {
		this(host, 80, username);
	}

	public Bridge(Transport transport) {
		this.transport = requireNonNull(transport);
	}

	/**
	 * Create a new group.
	 * 
	 * @param definition
	 *            the name and lights of the group
	 * @return id of the created group
	 */
	public Modifications createGroup(GroupAttributesUpdate definition) {
		return transport.post(definition, new GroupAttributesUpdate.Serializer(), "/groups");
	}

	public Modifications createGroup(String name, Collection<Light> lights) {
		GroupAttributesUpdate.Builder definition = new GroupAttributesUpdate.Builder();
		definition.name(name);
		definition.lights(lights);
		return createGroup(definition.build());
	}

	public Modifications createGroup(String name, Light... lights) {
		return createGroup(name, asList(lights));
	}

	public Modifications createScene(SceneAttributesUpdate attributes) {
		return transport.post(attributes, new SceneAttributesUpdateSerializer(), SCENES);
	}
	
	public Modifications deleteGroup(Group group) {
		return deleteGroup(group.getId());
	}
	
	public Modifications deleteGroup(String groupId) {
		StringBuilder builder = new StringBuilder(GROUPS.length() + groupId.length());
		builder.append(GROUPS);
		builder.append(groupId);
		return transport.delete(builder);
	}

	public Modifications deleteLight(Light light) {
		return deleteLight(light.getId());
	}

	public Modifications deleteLight(String lightId) {
		StringBuilder builder = new StringBuilder(LIGHTS.length() + lightId.length());
		builder.append(LIGHTS);
		builder.append(lightId);
		return transport.delete(builder);
	}

	public Modifications deleteRegistration(ApiRegistration registration) {
		return deleteRegistration(registration.getId());
	}
	
	public Modifications deleteRegistration(String id) {
		return transport.delete("/config/whitelist/" + id);
	}
	
	public Modifications deleteScene(String id) {
		StringBuilder builder = new StringBuilder(SCENES.length() + id.length());
		builder.append(SCENES);
		builder.append(id);
		return transport.delete(builder);
	}
	
	public Group fetchGroup(String id) {
		return transport.get(new GroupParser(id), GROUPS + id);
	}
	
	public Groups fetchGroups() {
		return transport.get(new GroupsParser(), "/groups");
	}
	
	public Light fetchLight(String id) {
		StringBuilder path = new StringBuilder(8 + id.length());
		path.append("/lights/");
		path.append(id);
		
		return transport.get(new LightParser(id), path);
	}
	
	public Lights fetchLights() {
		return transport.get(new LightsParser(), "/lights");
	}
	
	public NewLights fetchNewLights() {
		return transport.get(new NewLightsParser() , "/lights/new");
	}
	
	public Scene fetchScene(String id) {
		return transport.get(new SceneParser(id), "/scenes/" + id);
	}
	
	public Scenes fetchScenes() {
		return transport.get(new ScenesParser(), SCENES);
	}

	public Schedules fetchSchedules() {
		return transport.get(new SchedulesParser(), SCHEDULES);
	}
	
	public Schedule fetchSchedule(String scheduleId) {
		return transport.get(new ScheduleParser(scheduleId), path(SCHEDULES, scheduleId));
	}
	
	public Modifications recallScene(Scene scene) {
		return recallScene(scene.getId());
	}
	
	public Modifications recallScene(String sceneId) {
		return recallScene("0", sceneId);
	}
	
	public Modifications recallScene(String groupId, String sceneId) {
		StringBuilder path = new StringBuilder(15 + groupId.length());
		path.append(GROUPS);
		path.append(groupId);
		path.append(ACTION);
		
		return transport.put(sceneId, new RecallSceneSerializer(), path);
	}

	public Modifications searchNewLights(Collection<String> ids) {
		Set<String> search = null;
		
		if (ids != null && !ids.isEmpty()) {
			search = new LinkedHashSet<>(ids);
		}
		
		return transport.post(search, new NewLightsSerializer(), "/lights");
	}
	
	public Modifications searchNewLights(String... ids) {
		return searchNewLights(asList(ids));
	}

	public Update<Group> updateGroupAttributes(Group group, GroupAttributesUpdate update) {
		Modifications m = updateGroupAttributes(group.getId(), update);
		return new Update<>(m,() -> new Group(new Group.AttributesMerge(group, m)));
	}
	
	public Modifications updateGroupAttributes(String id, GroupAttributesUpdate update) {
		StringBuilder builder = new StringBuilder(GROUPS.length() + id.length());
		builder.append(GROUPS);
		builder.append(id);

		return transport.put(update, new GroupAttributesUpdate.Serializer(), builder);
	}
	
	public Update<Group> updateGroupLightState(Group group, LightStateUpdate update) {
		Modifications m = updateGroupLightState(group.getId(), update);
		
		return new Update<>(m, () -> new Group(new Group.AttributesMerge(group, m)));
	}
	
	public Modifications updateGroupLightState(String groupId, LightStateUpdate update) {
		StringBuilder builder = new StringBuilder(GROUPS.length() + groupId.length() + ACTION.length());
		builder.append(GROUPS);
		builder.append(groupId);
		builder.append(ACTION);

		return transport.put(update, new LightStateUpdateSerializer(), builder);
	}
	
	public Update<Light> updateLightAttributes(Light light, LightAttributesUpdate update) {
		Modifications m = updateLightAttributes(light.getId(), update);
		return new Update<Light>(m,() -> new Light(new Light.MergeAttributes(light, m)));
	}
	
	public Modifications updateLightAttributes(String lightId, LightAttributesUpdate update) {
		StringBuilder path = new StringBuilder(2 + LIGHTS.length());
		path.append(LIGHTS);
		path.append(lightId);
		
		return transport.put(update, new LightAttributesUpdateSerializer(), path);
	}
	
	public Update<Light> updateLightState(Light light, LightStateUpdate update) {
		Modifications m = updateLightState(light.getId(), update);
		return new Update<Light>(m,() -> new Light(new Light.MergeLightState(light, m)));
	}
	
	public Modifications updateLightState(String id, LightStateUpdate update) {
		StringBuilder path = new StringBuilder(2 + LIGHTS.length() + STATE.length());
		path.append(LIGHTS);
		path.append(id);
		path.append(STATE);

		return transport.put(update, new LightStateUpdateSerializer(), path);
	}
	
	public Modifications updateSceneAttributes(Scene scene, SceneAttributesUpdate update) {
		return updateSceneAttributes(scene.getId(), update);
	}
	
	public Modifications updateSceneAttributes(String sceneId, SceneAttributesUpdate update) {
		return transport.put(update, new SceneAttributesUpdateSerializer(), path(SCENES,sceneId));
	}
	
	public Modifications updateSceneLightState(Scene scene, Light light, LightStateUpdate update) {
		return updateSceneLightState(scene.getId(), light.getId(), update);
	}
	
	public Modifications updateSceneLightState(String sceneId, String lightId, LightStateUpdate update) {
		return transport.put(update, new LightStateUpdateSerializer(), path(SCENES,sceneId,"/lightstates/", lightId));
	}

	public Rules fetchRules() {
		return transport.get(new RulesParser(), RULES);
	}
	
	public Rule fetchRule(String id) {
		return transport.get(new RuleParser(id), path(RULES,id));
	}
	
	public Modifications updateRule(Rule rule, RuleUpdate update) {
		return updateRule(rule.getId(), update);
	}
	
	public Modifications updateRule(String id, RuleUpdate update) {
		return transport.put(update, new RuleUpdateSerializer(), path(RULES, id));
	}

	
	public Modifications createSchedule(ScheduleUpdate update) {
		return transport.post(update, new ScheduleUpdateSerializer(), SCHEDULES);
	}
	
	public Modifications updateSchedule(Schedule schedule, ScheduleUpdate update) {
		return updateSchedule(schedule.getId(), update);
	}
	
	public Modifications updateSchedule(String scheduleId, ScheduleUpdate update) {
		return transport.put(update, new ScheduleUpdateSerializer(), path(SCHEDULES, scheduleId));
	}
	
	public Modifications deleteSchedule(String scheduleId) {
		return transport.delete(path(SCHEDULES, scheduleId));
	}
	
	private CharSequence path(String... parts) {
		int length = 0;
		for (String part : parts) {
			length += part.length();
		}
		
		StringBuilder path = new StringBuilder(length);
		for (String part : parts) {
			path.append(part);
		}
		
		return path;
	}

	public Scenes fetchSensors() {
		return transport.get(new ScenesParser(), SENSORS);
	}

	public Sensor fetchSensor(String id) {
		return transport.get(new SensorParser(id), path(SENSORS, id));
	}
}
