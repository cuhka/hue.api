package com.cuhka.hue.api;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;

public class ScheduledDateTime extends TimeTrigger implements Serializable {
	private static final long serialVersionUID = 6098475435398258530L;
	private final LocalDateTime dt;
	private final Duration randomize;
	
	public ScheduledDateTime(LocalDateTime dt) {
		this(dt, Duration.ZERO);
	}

	public ScheduledDateTime(LocalDateTime dt, Duration randomize) {
		this.dt = Objects.requireNonNull(dt, "dt");
		this.randomize = Objects.requireNonNull(randomize, "randomize");
	}

	public LocalDateTime getDateTime() {
		return dt;
	}

	public Duration getRandomize() {
		return randomize;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dt == null) ? 0 : dt.hashCode());
		result = prime * result + ((randomize == null) ? 0 : randomize.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ScheduledDateTime)) {
			return false;
		}
		ScheduledDateTime other = (ScheduledDateTime) obj;
		if (dt == null) {
			if (other.dt != null) {
				return false;
			}
		} else if (!dt.equals(other.dt)) {
			return false;
		}
		if (randomize == null) {
			if (other.randomize != null) {
				return false;
			}
		} else if (!randomize.equals(other.randomize)) {
			return false;
		}
		return true;
	}

	@Override
	protected void apiValue(StringBuilder builder) {
		builder.append(dt.toString());
		appendRandom(builder, randomize);
	}
}
