package com.cuhka.hue.api;


public class MissingRequiredParametersException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public MissingRequiredParametersException(HueError error) {
		super(error);
	}
}
