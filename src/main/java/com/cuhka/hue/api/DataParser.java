package com.cuhka.hue.api;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.cuhka.hue.api.Action.Method;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

public abstract class DataParser<T> {
	public T parse(JsonReader reader) throws IOException {
		JsonToken token = reader.peek();

		switch (token) {
		case BEGIN_OBJECT:
			return parseObject(reader);

		case BEGIN_ARRAY:
			throw readErrorResponse(reader).toException();

		default:
			throw new IOException("Expected object or array");
		}
	}

	protected abstract T parseObject(JsonReader reader) throws IOException;

	private HueError readErrorResponse(JsonReader reader) throws IOException {
		HueError error = null;

		reader.beginArray();
		reader.beginObject();

		if (reader.nextName().equals("error")) {
			HueError.Builder builder = new HueError.Builder();

			reader.beginObject();
			while (reader.hasNext()) {
				switch (reader.nextName()) {
				case "type":
					builder.type = reader.nextInt();
					break;

				case "address":
					builder.address = reader.nextString();
					break;

				case "description":
					builder.description = reader.nextString();
					break;
				default:
					reader.skipValue();
					break;
				}
			}

			reader.endObject();

			error = new HueError(builder);
		} 

		reader.endObject();
		reader.endArray();

		if (error != null) {
			return error;
		}

		throw new IOException("Expected error structure");
	}

	protected <E extends Enum<E>> E nextEnum(Class<E> enumType, JsonReader reader) throws IOException {

		try {
			return Enum.valueOf(enumType, reader.nextString().toUpperCase());
		} catch (IllegalArgumentException e) {
			// ok
		}

		return null;
	}

	protected Map<String, Object> readMap(JsonReader reader) throws IOException {
		reader.beginObject();

		Map<String, Object> map = new LinkedHashMap<>();

		while (reader.hasNext()) {
			String key = reader.nextName();
			Object value = null;

			value = readValue(reader);

			map.put(key, value);
		}

		reader.endObject();

		return Collections.unmodifiableMap(map);
	}

	protected Object readValue(JsonReader reader) throws IOException {
		switch (reader.peek()) {
		case BEGIN_ARRAY:
			return readArray(reader);

		case BEGIN_OBJECT:
			return readMap(reader);

		case BOOLEAN:
			return reader.nextBoolean();

		case NULL:
			reader.nextNull();
			return null;

		case NUMBER:
			return reader.nextDouble();

		case STRING:
			return reader.nextString();

		default:
			reader.skipValue();
			return null;
		}
	}

	protected Instant readInstant(JsonReader reader) throws IOException {
		String value = reader.nextString();

		if (!value.equals("none")) {
			return LocalDateTime.parse(value).toInstant(ZoneOffset.UTC);
		} 

		return null;
	}

	protected Object readArray(JsonReader reader) throws IOException {
		List<Object> array = new ArrayList<>();

		reader.beginArray();

		while (reader.hasNext()) {
			array.add(readValue(reader));
		}

		reader.endArray();

		return Collections.unmodifiableList(array);
	}

	protected Action readAction(JsonReader reader) throws IOException {
		Action.Builder builder = new Action.Builder();
		
		reader.beginObject();
		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "address":
				builder.address(reader.nextString());
				break;
				
			case "method":
				builder.method(nextEnum(Method.class, reader));
				break;
				
			case "body":
				builder.body(readMap(reader));
				break;
				
			default:
				reader.skipValue();
				break;
			}
		}
		reader.endObject();
		
		return builder.build();
	}
}
