package com.cuhka.hue.api;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Schedules extends ImmutableSet<Schedule> {
	private Set<Schedule> items;

	public Schedules(Collection<? extends Schedule> schedules) {
		super(schedules);
	}

	public Optional<Schedule> findById(String id) {
		return find((s) -> s.getId().equals(id));
	}

	public Optional<Schedule> findByName(String name) {
		return find((s) -> s.getName().equals(name));
	}

	public Optional<Schedule> find(Predicate<? super Schedule> p) {
		return stream().filter(p).findAny();
	}

	public Schedules filter(Predicate<? super Schedule> p) {
		return new Schedules(items.stream().filter(p).collect(Collectors.toSet()));
	}
}
