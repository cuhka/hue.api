package com.cuhka.hue.api;

import java.io.Serializable;
import java.time.Duration;
import java.util.Objects;

public class Timer extends TimeTrigger implements Serializable {
	private static final long serialVersionUID = 1330406410400626217L;
	private final Duration duration;
	private final Duration randomize;

	public Timer(Duration duration) {
		this(duration, Duration.ZERO);
	}

	public Timer(Duration duration, Duration randomize) {
		this.duration = Objects.requireNonNull(duration, "duration");
		this.randomize = Objects.requireNonNull(randomize, "randomize");
	}

	public Duration getDuration() {
		return duration;
	}

	public Duration getRandomize() {
		return randomize;
	}

	@Override
	protected void apiValue(StringBuilder builder) {
		builder.append("PT");
		formatDuration(builder, duration);
		appendRandom(builder, randomize);
	}	
}
