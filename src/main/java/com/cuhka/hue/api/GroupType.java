package com.cuhka.hue.api;

public enum GroupType {
	GROUP0("0"),
	LUMINAIRE("Luminaire"),
	LIGHTSOURCE("Lightsource"),
	LIGHTGROUP("LightGroup"),
	ROOM("Room");
	
	private final String type;
	
	private GroupType(String type) {
		this.type = type;
	}
	
	public static GroupType fromGroupType(String type) {
		for (GroupType value : values()) {
			if (value.type.equals(type)) {
				return value;
			}
		}
		
		return LIGHTGROUP;
	}
	
	@Override
	public String toString() {
		return type;
	}
}
