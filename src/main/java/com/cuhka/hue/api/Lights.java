package com.cuhka.hue.api;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Lights extends ImmutableSet<Light> {
	public static class Types {
		public static Collection<String> COLORLIGHTS = Arrays.asList(new String[] { "Color light", "Extended color light" });
		public static Collection<String> EXTENDED_COLORLIGHTS = Collections.singleton("Extended color light");
		public static Collection<String> DIMMABLE_PLUGIN = Collections.singleton("Dimmable plug-in unit");
	}

	public static Predicate<Light> SWITCHED_ON = (l) -> l.getLightState().isOn();
	public static Predicate<Light> SWITCHED_OFF = (l) -> !l.getLightState().isOn();
	public static Predicate<Light> REACHABLE = (l) -> l.getLightState().isReachable();
	public static Predicate<Light> UNREACHABLE = (l) -> !l.getLightState().isReachable();

	public Lights(Collection<? extends Light> lights) {
		super(lights);
	}

	public Lights filter(Predicate<? super Light> p) {
		return new Lights(stream().filter(p).collect(Collectors.toSet()));
	}

	public Lights filterModelIds(Collection<String> ids) {
		return filter((l) -> ids.contains(l.getModelId()));
	}

	public Lights filterModelIds(String... ids) {
		return filterModelIds(Arrays.asList(ids));
	}

	public Lights filterTypes(Collection<String> types) {
		return filter((l) -> types.contains(l.getType()));
	}

	public Lights filterTypes(String... types) {
		return filterTypes(Arrays.asList(types));
	}

	public Lights filterIds(String... ids) {
		return filterIds(Arrays.asList(ids));
	}

	public Lights filterIds(Collection<String> ids) {
		return filter((l) -> ids.contains(l.getId()));
	}

	public Lights filterNames(String... names) {
		return filterNames(Arrays.asList(names));
	}
	
	public Lights filterNames(Collection<String> names) {
		return filter(l -> names.contains(l.getName()));
	}
	

	public Optional<Light> findById(String id) {
		return find((l) -> l.getId().equals(id));
	}

	public Optional<Light> findByName(String name) {
		return find((l) -> l.getName().equals(name));
	}
}
