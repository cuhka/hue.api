package com.cuhka.hue.api;

import java.io.IOException;

import com.google.gson.stream.JsonReader;

public class LightParser extends DataParser<Light> {
	private final String id;
	
	public LightParser(String id) {
		this.id = id;
	}

	@Override
	public Light parseObject(JsonReader reader) throws IOException {
		Light.Builder builder = new Light.Builder();

		reader.beginObject();
		
		builder.id = id;
		while (reader.hasNext()) {
			String name = reader.nextName();
			
			switch (name) {
			case "state":
				builder.lightState = new LightStateParser().parse(reader);
				break;

			case "type":
				builder.type = reader.nextString();
				break;
				
			case "name":
				builder.name = reader.nextString();
				break;
				
			case "modelid":
				builder.modelId = reader.nextString();
				break;
				
			case "manufacturername":
				builder.manufacturerName = reader.nextString();
				break;
				
			case "uniqueid":
				builder.uniqueId = reader.nextString();
				break;
				
			case "swversion":
				builder.softwareVersion = reader.nextString();
				break;
				
			default:
				reader.skipValue();
				break;
			}
		}

		reader.endObject();
		return new Light(builder);
	}
}
