package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.optionalList;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Modifications implements Iterable<Modification>, Serializable {
	private static final long serialVersionUID = -1610689138871274062L;

	public static class Builder implements Source {
		public Collection<? extends Modification> successes;
		public Collection<? extends HueError> errors;

		@Override
		public Collection<? extends Modification> getSuccesses() {
			return successes;
		}

		@Override
		public Collection<? extends HueError> getErrors() {
			return errors;
		}
	}

	public interface Source {
		Collection<? extends Modification> getSuccesses();

		Collection<? extends HueError> getErrors();
	}

	private final List<Modification> successes;
	private final List<HueError> errors;

	public Modifications(Source source) {
		successes = optionalList(source.getSuccesses());
		errors = optionalList(source.getErrors());
	}

	public List<Modification> getSuccesses() {
		return successes;
	}

	public List<HueError> getErrors() {
		return errors;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ModificationResult [successes=");
		builder.append(successes);
		builder.append(", errors=");
		builder.append(errors);
		builder.append("]");

		return builder.toString();
	}

	public boolean isOk() {
		return errors.isEmpty();
	}

	public Optional<Modification> findAttribute(String attribute) {
		return successes.stream().filter((m) -> m.getAttribute().equals(attribute)).findAny();
	}

	@Override
	public Iterator<Modification> iterator() {
		return successes.iterator();
	}
	
	public Stream<Modification> stream() {
		return successes.stream();
	}
}
