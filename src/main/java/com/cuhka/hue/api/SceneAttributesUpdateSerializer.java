package com.cuhka.hue.api;

import java.io.IOException;
import java.util.Optional;

import com.google.gson.stream.JsonWriter;

class SceneAttributesUpdateSerializer extends DataSerializer<SceneAttributesUpdate> {
	@Override
	public void write(SceneAttributesUpdate data, JsonWriter writer) throws IOException {
		writer.beginObject();
		writeString(writer, "name", data.getName());
		writeStringArray(writer, "lights", data.getLights());
		writeBoolean(writer, "recycle", data.getRecycle());
		writeInteger(writer, "transitiontime", data.getTransitionTime());
		writeAppData(writer, "appdata", data.getAppData());
		writeString(writer, "picture", data.getPicture());
		writer.endObject();
	}

	private void writeAppData(JsonWriter writer, String string, Optional<AppData> appData) throws IOException {
		AppData data = appData.orElse(null);

		if (data != null) {
			writer.name("appdata");
			
			writer.beginObject();
			writer.name("version");
			writer.value(data.getVersion());
			writer.name("data");
			writer.value(data.getData());
			writer.endObject();
		}
	}
}
