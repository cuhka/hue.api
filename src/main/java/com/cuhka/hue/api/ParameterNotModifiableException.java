package com.cuhka.hue.api;


public class ParameterNotModifiableException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public ParameterNotModifiableException(HueError error) {
		super(error);
	}
}
