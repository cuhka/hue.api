package com.cuhka.hue.api;

import java.io.IOException;
import java.util.Collection;

import com.google.gson.stream.JsonWriter;

class NewLightsSerializer extends DataSerializer<Collection<String>>{
	@Override
	public void write(Collection<String> data, JsonWriter writer) throws IOException {
		if (data != null && !data.isEmpty()) {
			writer.beginObject();
			writer.name("deviceid");
			
			writer.beginArray();

			for (String id : data) {
				writer.value(id);
			}

			writer.endArray();
			writer.endObject();
		}

	}

}
