package com.cuhka.hue.api;


public class ResourceNotAvailableException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public ResourceNotAvailableException(HueError error) {
		super(error);
	}
}
