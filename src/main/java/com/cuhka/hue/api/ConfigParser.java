package com.cuhka.hue.api;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.LinkedHashSet;
import java.util.Set;

import com.google.gson.stream.JsonReader;

class ConfigParser extends DataParser<Config> {
	@Override
	protected Config parseObject(JsonReader reader) throws IOException {
		Config.Builder builder = new Config.Builder();
		reader.beginObject();

		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "name":
				builder.name = reader.nextString();
				break;

			case "zigbeechannel":
				builder.zigbeechannel = reader.nextInt();
				break;
				
			case "UTC":
				builder.utc = LocalDateTime.parse(reader.nextString()).toInstant(ZoneOffset.UTC);
				break;

			case "localtime":
				builder.localtime = LocalDateTime.parse(reader.nextString());
				break;

			case "timezone":
				builder.timezone = ZoneId.of(reader.nextString());
				break;

			case "mac":
				builder.mac = reader.nextString();
				break;

			case "dhcp":
				builder.dhcp = reader.nextBoolean();
				break;

			case "ipaddress":
				builder.ipaddress = reader.nextString();
				break;

			case "netmask":
				builder.netmask = reader.nextString();
				break;

			case "gateway":
				builder.gateway = reader.nextString();
				break;

			case "proxyaddress":
				builder.proxyaddress = reader.nextString();
				
				if (builder.proxyaddress.equals("none")) {
					builder.proxyaddress = null;
				}
				break;

			case "proxyport":
				builder.proxyport = reader.nextInt();
				break;

			case "whitelist":
				builder.whitelist = parseRegistrations(reader);
				break;

			case "swversion":
				builder.swversion = reader.nextString();
				break;
				
			case "swupdate":
				builder.swudpdate = parseSwUpdate(reader);
				break;

			case "apiversion":
				builder.apiversion = reader.nextString();
				break;
				
				
			case "linkbutton":
				builder.linkbutton = reader.nextBoolean();
				break;
			
			case "portalservices":
				builder.portalservices = reader.nextBoolean();
				break;
				
			case "portalconnection":
				builder.portalconnection = reader.nextString().equals("connected");
				break;
				
			case "portalstate":
				builder.portalstate = parsePortalState(reader);
				break;
				
			default:
				reader.skipValue();
				break;
			}
		}
		reader.endObject();

		
		return new Config(builder);
	}

	private Config.PortalState parsePortalState(JsonReader reader) throws IOException {
		reader.beginObject();
		
		Config.PortalState.Builder builder = new Config.PortalState.Builder();
		
		while (reader.hasNext()) {
			switch(reader.nextName()) {
			
			case "signedon":
				builder.signedOn = reader.nextBoolean();
				break;
				
			case "incoming":
				builder.incoming = reader.nextBoolean();
				break;
				
			case "outgoing":
				builder.outgoing = reader.nextBoolean();
				break;
				
			case "communication":
				builder.communication = Config.PortalState.Communication.valueOf(reader.nextString().toUpperCase());
				break;
				
			default:
				reader.skipValue();
				break;
			}
		}
		
		reader.endObject();
		return new Config.PortalState(builder);
	}

	private SoftwareUpdate parseSwUpdate(JsonReader reader) throws IOException {
		int state = 0;
		String url = null;
		String text = null;
		boolean notify = false;

		reader.beginObject();

		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "updatestate":
				state = reader.nextInt();
				break;

			case "url":
				url = reader.nextString();
				break;

			case "text":
				text = reader.nextString();
				break;

			case "notify":
				notify = reader.nextBoolean();
				break;

			default:
				reader.skipValue();
				break;
			}
		}
		reader.endObject();

		return state != 0 ? new SoftwareUpdate(state, url, text, notify) : null;
	}

	private Set<ApiRegistration> parseRegistrations(JsonReader reader)
			throws IOException {
		Set<ApiRegistration> registrations = new LinkedHashSet<>();

		reader.beginObject();
		while (reader.hasNext()) {
			String id = reader.nextName();
			ApiRegistration registration = readRegistration(id, reader);
			registrations.add(registration);
		}
		reader.endObject();
		return registrations;
	}

	private ApiRegistration readRegistration(String id, JsonReader reader) throws IOException {
		ApiRegistration.Builder builder = new ApiRegistration.Builder();
		builder.id = id;
		
		reader.beginObject();

		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "last use date":
				builder.lastUsed = LocalDateTime.parse(reader.nextString());
				break;

			case "create date":
				builder.created = LocalDateTime.parse(reader.nextString());;
				break;

			case "name":
				builder.name = reader.nextString();
				break;

			default:
				reader.skipValue();
				break;
			}
		}

		reader.endObject();
		
		return new ApiRegistration(builder);
	}
}
