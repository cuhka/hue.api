package com.cuhka.hue.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.stream.JsonReader;

public class RulesParser extends DataParser<Rules> {

	@Override
	protected Rules parseObject(JsonReader reader) throws IOException {
		Collection<Rule> rules = new ArrayList<>();
		
		reader.beginObject();
		
		while (reader.hasNext()) {
			rules.add(new RuleParser(reader.nextName()).parseObject(reader));
		}
		
		reader.endObject();
		
		return new Rules(rules);
	}

}
