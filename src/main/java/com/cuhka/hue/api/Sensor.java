package com.cuhka.hue.api;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class Sensor implements Serializable {
	private static final long serialVersionUID = 6539402340969536142L;
	private final String id;
	private final String name;
	private final String type;
	private final String modelId;
	private final String manufacturer;
	private final String swVersion;
	private final String uuid;
	private final Map<String, Object> state;
	private final Map<String, Object> config;
	
	public static class Builder {
		private String id;
		private String name;
		private String type;
		private String modelId;
		private String manufacturer;
		private String swVersion;
		private String uuid;
		private Map<String, Object> state = new LinkedHashMap<>();
		private Map<String, Object> config = new LinkedHashMap<>();
		
		
		public Builder id(String id) {
			this.id = id;
			return this;
		}
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Builder type(String type) {
			this.type = type;
			return this;
		}
		
		public Builder modelId(String modelId) {
			this.modelId = modelId;
			return this;
		}
		
		public Builder manufacturer(String name) {
			this.manufacturer = name;
			return this;
		}
		
		public Builder swVersion(String version) {
			this.swVersion = version;
			return this;
		}
		
		public Builder uuid(String uuid) {
			this.uuid = uuid;
			return this;
		}

		public Builder config(Map<String, ? extends Object> config) {
			this.config.putAll(config);
			return this;
		}
		
		public Builder state(Map<String, ? extends Object> state) {
			this.state.putAll(state);
			return this;
		}
		
		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}

		public String getModelId() {
			return modelId;
		}

		public String getManufacturer() {
			return manufacturer;
		}

		public String getSwVersion() {
			return swVersion;
		}

		public String getUuid() {
			return uuid;
		}
		
		public Map<String, Object> getState() {
			return state;
		}
		
		public Sensor build() {
			return new Sensor(this);
		}
	}

	private Sensor(Builder builder) {
		this.id = BuilderUtils.required(builder.id, "id");
		this.name = BuilderUtils.optional(builder.name);
		this.modelId = BuilderUtils.optional(builder.modelId);
		this.type = BuilderUtils.optional(builder.type);
		this.manufacturer = BuilderUtils.optional(builder.manufacturer);
		this.swVersion = BuilderUtils.optional(builder.swVersion);
		this.uuid = BuilderUtils.optional(builder.uuid);
		this.state = Collections.unmodifiableMap(new LinkedHashMap<String, Object>(builder.state));
		this.config = Collections.unmodifiableMap(new LinkedHashMap<String, Object>(builder.config));
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getModelId() {
		return modelId;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getSwVersion() {
		return swVersion;
	}

	public String getUuid() {
		return uuid;
	}

	public Map<String, Object> getState() {
		return state;
	}

	public Map<String, Object> getConfig() {
		return config;
	}

	public String getId() {
		return id;
	}
}
