package com.cuhka.hue.api;


public class DeviceSwitchedOffException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public DeviceSwitchedOffException(HueError error) {
		super(error);
	}
}
