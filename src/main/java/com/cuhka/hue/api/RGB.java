package com.cuhka.hue.api;

import static java.lang.Math.log;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.pow;
import static java.lang.Math.round;

import java.io.Serializable;

import com.cuhka.hue.api.GamutTriangle.Point;

public class RGB implements Serializable {
	private static final long serialVersionUID = 4675419814562337805L;
	
	private static double[][] MATRIX =
		{
			{ 0.649926d, 0.103455d, 0.197109d },
			{ 0.234327d, 0.743075d, 0.022598d },
			{ 0.000000d, 0.053077d, 1.035763d }
		};

	private final int red;
	private final int green;
	private final int blue;

	public RGB(int color) {
		this((color & 0xFF0000) >> 16, (color & 0x00FF00 ) >> 8, color & 0x0000FF);
	}
	
	public RGB(int red, int green, int blue) {
		this.red = min(0xFF, max(0x00,red));
		this.green = min(0xFF, max(0x00,green));
		this.blue = min(0xFF, max(0x00,blue));
	}

	public XYColor toXYColor() {
		return toXYColor(GamutTriangle.UNKNOWN);
	}
	
	public XYColor toXYColor(GamutTriangle triangle) {
		final double r = gammaCorrection(normalized(red));
		final double g = gammaCorrection(normalized(green));
		final double b = gammaCorrection(normalized(blue));

		final double X = wideRGBD65(0, r,g,b);
		final double Y = wideRGBD65(1, r,g,b);
		final double Z = wideRGBD65(2, r,g,b);
		final double XYZ = X + Y + Z;
		
	    final double x = X / XYZ;
	    final double y = Y / XYZ;
	    
	    final Point p = triangle.closestPoint(x,y);
	    
	    return new XYColor(p.getX(), p.getY());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RGB other = (RGB) obj;
		if (blue != other.blue)
			return false;
		if (green != other.green)
			return false;
		if (red != other.red)
			return false;
		return true;
	}

	public int getBlue() {
		return blue;
	}

	public int getGreen() {
		return green;
	}

	public int getRed() {
		return red;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + blue;
		result = prime * result + green;
		result = prime * result + red;
		return result;
	}
	private double wideRGBD65(int index, double r, double g, double b) {
		final double[] line = MATRIX[index];
		
		return (r * line[0]) + (g * line[1]) + (b * line[2]);
	}
	
	private final double normalized(double value) {
		return value / 255d;
	}

	@Override
	public String toString() {
		return String.format("RGB [R:%02x G:%02x B:%02x]",red,green,blue);
	}

	private double gammaCorrection(double v) {
		return (v > 0.04045d) ? pow((v + 0.055d) / 1.055d, 2.4d) : (v / 12.92d);
	}
	
	public static RGB fromCT(CTColor ct) {
		return fromCT(ct.inKelvin());
	}
	
	/**
	 * @see "http://www.tannerhelland.com/4435/convert-temperature-rgb-algorithm-code/"
	 */
	public static RGB fromCT(int kelvin) {
		double temperature = ((double)max(min(kelvin, 40_000), 1_000)) / 100d;
		double red, green, blue;
		
		if (temperature <= 66d) {
			red = 255d;
		} else {
			red = 329.698727446 * pow(temperature - 60, 0.1332047592);
		}
		
		if (temperature <= 66d) {
			green = 99.4708025861d * log(temperature) - 161.1195681661d;
		} else {
			green = 288.1221695283d * pow(temperature - 60,-0.0755148492d);
		}
		
		if (temperature >= 66d) {
			blue = 255d;
		} else if (temperature < 19d) {
			blue = 0d;
		} else {
			blue = 138.5177312231d * log(temperature) - 305.0447927307d;
		}
		
		return new RGB((int)round(red),(int)round(green),(int)round(blue));
	}
	
	
}