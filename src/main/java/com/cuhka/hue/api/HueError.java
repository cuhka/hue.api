package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.optional;
import static com.cuhka.hue.api.BuilderUtils.required;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @see http://developers.meethue.com/8_errormessages.html
 *
 */
public class HueError implements Serializable {
	private static final long serialVersionUID = -3898044559129858807L;

	public static class Builder implements Source {
		public Integer type;
		public String address;
		public String description;
		
		public Integer getType() {
			return type;
		}
		public String getAddress() {
			return address;
		}
		public String getDescription() {
			return description;
		}
		
		@Override
		public String toString() {
			return String.format("HueError.Builder [type=%s, address=%s, description=%s]", type, address, description);
		}
	}
	
	public interface Source {
		Integer getType();
		String getAddress();
		String getDescription();
	}
	
	public final static int UNAUTHORIZED_USER = 1;
	public final static int INVALID_JSON = 2;
	public final static int RESOURCE_NOT_AVAILABLE = 3;
	public final static int METHOD_NOT_AVAILABLE_FOR_RESOURCE = 4;
	public final static int MISSING_REQUIRED_PARAMETERS = 5;
	public final static int PARAMETER_NOT_AVAILABLE = 6;
	public final static int INVALID_VALUE = 7;
	public final static int PARAMETER_NOT_MODIFIABLE = 8;
	public final static int INTERNAL_ERROR = 901;
	public final static int LINK_BUTTON_NOT_PRESSED = 101;
	public final static int PARAMETER_NOT_MODIFIABLE_DEVICE_OFF = 201;
	public final static int GROUP_TABLE_FULL = 301;
	public final static int DEVICE_GROUP_TABLE_FULL = 302;

	private final int[] TYPE_CODES = {
			UNAUTHORIZED_USER,
			INVALID_JSON,
			RESOURCE_NOT_AVAILABLE,
			MISSING_REQUIRED_PARAMETERS,
			PARAMETER_NOT_AVAILABLE,
			INVALID_VALUE,
			PARAMETER_NOT_MODIFIABLE,
			INTERNAL_ERROR,
			LINK_BUTTON_NOT_PRESSED,
			PARAMETER_NOT_MODIFIABLE_DEVICE_OFF,
			GROUP_TABLE_FULL,
			DEVICE_GROUP_TABLE_FULL
	};
	
	Class<?>[] CLASSES = {
			UnauthorizedUserException.class,
			InvalidJsonException.class,
			ResourceNotAvailableException.class,
			MissingRequiredParametersException.class,
			ParameterNotAvailableException.class,
			InvalidValueException.class,
			ParameterNotModifiableException.class,
			InternalErrorInBridgeException.class,
			LinkButtonNotPressedException.class,
			DeviceSwitchedOffException.class,
			GroupTableFullException.class,
			DeviceGroupTableFullException.class
	};
	
	private final int type;
	private final String address;
	private final String description;
	
	public HueError(Source source) {
		type = required(source.getType(),"type");
		address = optional(source.getAddress());
		description = optional(source.getDescription());
	}
	
	public int getType() {
		return type;
	}

	public String getAddress() {
		return address;
	}

	public String getDescription() {
		return description;
	}
	
	public String getAttribute() {
		return address.substring(address.lastIndexOf('/') + 1);
	}

	@SuppressWarnings("unchecked")
	public HueException toException() {
		Class<HueException> exceptionClass = HueException.class;
		
		try {
			for (int i = 0; i < TYPE_CODES.length; i++) {
				if (TYPE_CODES[i] == type) {
					exceptionClass = (Class<HueException>) CLASSES[i];
					break;
				}
			}
			Constructor<HueException> c = (Constructor<HueException>)exceptionClass.getConstructor(HueError.class);
			return c.newInstance(this);			
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HueError [type=");
		builder.append(type);
		builder.append(", address=");
		builder.append(address);
		builder.append(", description=");
		builder.append(description);
		builder.append(", attribute=");
		builder.append(getAttribute());
		builder.append("]");
		return builder.toString();
	}
}
