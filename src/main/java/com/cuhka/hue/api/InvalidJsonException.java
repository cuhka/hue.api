package com.cuhka.hue.api;


public class InvalidJsonException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public InvalidJsonException(HueError error) {
		super(error);
	}
}
