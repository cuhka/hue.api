package com.cuhka.hue.api;

import java.util.Objects;

public class AppData {
	private final int version;
	private final String data;
	
	public static class Builder {
		private int version = 1;
		private CharSequence data;
		
		public Builder version(int version) {
			this.version = version;
			return this;
		}
		
		public int getVersion() {
			return version;
		}
		
		public Builder data(CharSequence data) {
			this.data = data;
			return this;
		}
		
		public CharSequence getData() {
			return data;
		}

		public AppData build() {
			return new AppData(this);
		}
	}
	
	private AppData(Builder source) {
		this.version = source.version;
		this.data = Objects.requireNonNull(source.data.toString(), "data");
	}

	public int getVersion() {
		return version;
	}

	public String getData() {
		return data;
	}
}
