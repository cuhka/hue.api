package com.cuhka.hue.api;


public class InvalidValueException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public InvalidValueException(HueError error) {
		super(error);
	}
}
