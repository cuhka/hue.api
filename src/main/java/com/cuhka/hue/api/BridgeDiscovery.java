package com.cuhka.hue.api;

import java.util.List;

public interface BridgeDiscovery {
	List<BridgeLocator> discoverBridges();
}
