package com.cuhka.hue.api;

import static java.lang.Math.sqrt;

import java.io.Serializable;

public class GamutTriangle implements Serializable {	
	private static final long serialVersionUID = -3355183378109302221L;

	public static class Point implements Serializable {
		private static final long serialVersionUID = 7299281096355488500L;
		private final double x;
		private final double y;

		public Point(double x, double y) {			
			this.x = x;
			this.y = y;
		}

		public double getX() {
			return x;
		}

		public double getY() {
			return y;
		}

		public double distance(Point p) {
			double dx = this.x - p.x;
			double dy = this.y - p.y;
			
			return sqrt(dx * dx + dy * dy);
		}
		
		public double crossProduct(Point p) {
			return this.x * p.y - (this.y * p.x);
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(x);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(y);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Point other = (Point) obj;
			if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
				return false;
			if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return String.format("(%d,%d)",x,y);
		}
	}
	
	public static final GamutTriangle UNKNOWN = new GamutTriangle(new Point(1d,0d), new Point(0d,1d), new Point(0d,0d));
	
	private final Point red;
	private final Point green;
	private final Point blue;

	public GamutTriangle(Point red, Point green, Point blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public Point getRed() {
		return red;
	}

	public Point getGreen() {
		return green;
	}

	public Point getBlue() {
		return blue;
	}

	public boolean isInside(Point p) {
		Point v1 = new Point(green.x - red.x, green.y - red.y);
		Point v2 = new Point(blue.x - red.x, blue.y - red.y);
		Point q = new Point(p.x-red.x, p.y - red.y);
		double s = q.crossProduct(v2)  / v1.crossProduct(v2);
		double t = v1.crossProduct(q) / v1.crossProduct(v2);
		
		return s >= 0&& t >= 0D && (s + t <= 1D);
	}
	
	public Point closestPoint(double x, double y) {
		Point r = new Point(x,y);
		
		if (!isInside(r)) {
			Point rg = getClosestPointToPoints(red, green, r);
			Point br = getClosestPointToPoints(blue, red, r);
			Point gb = getClosestPointToPoints(green, blue, r);
			
			r = findClosest(r, rg, br,gb);
		}
		
		
		return r;
	}
	
	private Point findClosest(Point p, Point... points) {
		double lowest = Double.MAX_VALUE;
		Point closest = null;
		
		for (Point point : points) {
			double distance = p.distance(point);
			
			if (distance < lowest) {
				lowest = distance;
				closest = point;
			}
		}
		
		return closest;
	}
	
	private static Point getClosestPointToPoints(Point a, Point b, Point p) {
		Point ap = new Point(p.x - a.x, p.y - a.y);
		Point ab = new Point(b.x - a.x, b.y - a.y);
		double ab2 = ab.x * ab.x + ab.y * ab.y;
		double apAb = ap.x * ab.x + ap.y * ab.y;
		double t = apAb / ab2;
		if (t < 0.0F) {
			t = 0.0F;
		} else if (t > 1.0F) {
			t = 1.0F;
		}
		return new Point(a.x + ab.x * t, a.y + ab.y * t);
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blue == null) ? 0 : blue.hashCode());
		result = prime * result + ((green == null) ? 0 : green.hashCode());
		result = prime * result + ((red == null) ? 0 : red.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GamutTriangle other = (GamutTriangle) obj;
		if (blue == null) {
			if (other.blue != null)
				return false;
		} else if (!blue.equals(other.blue))
			return false;
		if (green == null) {
			if (other.green != null)
				return false;
		} else if (!green.equals(other.green))
			return false;
		if (red == null) {
			if (other.red != null)
				return false;
		} else if (!red.equals(other.red))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GamutTriangle [red=");
		builder.append(red);
		builder.append(", green=");
		builder.append(green);
		builder.append(", blue=");
		builder.append(blue);
		builder.append("]");
		return builder.toString();
	}
}
