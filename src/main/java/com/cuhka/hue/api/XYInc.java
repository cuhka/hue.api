package com.cuhka.hue.api;

import java.io.Serializable;

public class XYInc implements Serializable {
	private static final long serialVersionUID = 526708144645264683L;
	private final double dx;
	private final double dy;

	public XYInc(double dx, double dy) {
		this.dx = dx;
		this.dy = dy;
	}

	public double getDeltaX() {
		return dx;
	}

	public double getDeltaY() {
		return dy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(dx);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(dy);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof XYInc)) {
			return false;
		}
		XYInc other = (XYInc) obj;
		if (Double.doubleToLongBits(dx) != Double.doubleToLongBits(other.dx)) {
			return false;
		}
		if (Double.doubleToLongBits(dy) != Double.doubleToLongBits(other.dy)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "XYInc [dx=" + dx + ", dy=" + dy + "]";
	}
}
