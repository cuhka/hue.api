package com.cuhka.hue.api;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class TimeTrigger {
	private final static long HOURS = 60 * 60;
	private final static long MINUTES = 60;
	private static final String TIME = "(?<tH>\\d{2}):(?<tm>\\d{2}):(?<ts>\\d{2})";
	private static final String DATE = "(?<dY>\\d{4})-(?<dM>\\d{2})-(?<dD>\\d{2})";
	private static final String RANDOMIZED = "(?<A>A(?<rH>\\d{2}):(?<rm>\\d{2}):(?<rs>\\d{2}))?";

	private static final Pattern SCHEDULED_DATE_TIME = Pattern.compile(DATE + "T" + TIME + RANDOMIZED);
	private static final Pattern RECURRING_TIMES = Pattern.compile("W(\\d{1,3})/T" + TIME + RANDOMIZED) ;
	private static final Pattern TIMER = Pattern.compile("PT" + TIME + RANDOMIZED);
	private static final Pattern RECURRING_TIMER = Pattern.compile("R(?<times>\\d{1,2})?/PT" + TIME + RANDOMIZED);

	private static final Map<Pattern, Function<Matcher,? extends TimeTrigger>> SCHEDULE_MATCHERS = new HashMap<>();

	static {
		SCHEDULE_MATCHERS.put(SCHEDULED_DATE_TIME, TimeTrigger::parseAbsoluteTime);
		SCHEDULE_MATCHERS.put(RECURRING_TIMES, TimeTrigger::parseRecurringTimes);
		SCHEDULE_MATCHERS.put(TIMER, TimeTrigger::parseTimer);
		SCHEDULE_MATCHERS.put(RECURRING_TIMER, TimeTrigger::parseRecurringTimer);
	}

	public static TimeTrigger fromApi(String value) {
		for (Map.Entry<Pattern, Function<Matcher, ? extends TimeTrigger>> entry : SCHEDULE_MATCHERS.entrySet()) {
			Matcher matcher = entry.getKey().matcher(value);
			if (matcher.matches()) {
				return entry.getValue().apply(matcher);
			}
		}
		
		throw new IllegalArgumentException(String.format("Value '%s' has no parser", value));
	}

	private static Set<DayOfWeek> toDays(int value) {
		Set<DayOfWeek> days = EnumSet.noneOf(DayOfWeek.class);

		int bitmask = 0b1000000;

		for (DayOfWeek day : DayOfWeek.values()) {
			if ( (value & bitmask) != 0) {
				days.add(day);
			}
			bitmask >>= 1;
		}
		return days;
	}

	private static int match(Matcher matcher, String groupName) {
		return Integer.valueOf(matcher.group(groupName));
	}

	private static ScheduledDateTime parseAbsoluteTime(Matcher matcher) {
		LocalDateTime dt = LocalDateTime.of(match(matcher, "dY"), match(matcher, "dM"), match(matcher, "dD"),
				match(matcher, "tH"), match(matcher, "tm"), match(matcher, "ts"));
		Duration random = extractRandomized(matcher);
		return new ScheduledDateTime(dt, random);
		
	}

	private static RecurringTime parseRecurringTimes(Matcher matcher) {
		Set<DayOfWeek> days = toDays(Integer.valueOf(matcher.group(1)));
		int h = Integer.valueOf(matcher.group("tH"));
		int m = Integer.valueOf(matcher.group("tm"));
		int s = Integer.valueOf(matcher.group("ts"));
		
		LocalTime time = LocalTime.of(h, m, s);
		Duration random = extractRandomized(matcher);
		return new RecurringTime(days, time, random);
	}

	private static Duration extractRandomized(Matcher matcher) {
		if (matcher.group("A") != null) {
			long rh = Integer.valueOf(matcher.group("rH"));
			long rm = Integer.valueOf(matcher.group("rm"));
			long rs = Integer.valueOf(matcher.group("rs"));
			
			return Duration.ofSeconds(rh * 3600 + rm * 60 + rs);
		}
		return Duration.ZERO;
	}
	
	private static Timer parseTimer(Matcher matcher) {
		long h = match(matcher, "tH");
		long m = match(matcher, "tm");
		long s = match(matcher, "ts");
		
		Duration duration = Duration.ofSeconds(h * 3600 + m * 60  + s);
		
		return new Timer(duration, extractRandomized(matcher));
	}

	private static RecurringTimer parseRecurringTimer(Matcher matcher) {
		long h = match(matcher, "tH");
		long m = match(matcher, "tm");
		long s = match(matcher, "ts");
		
		Duration duration = Duration.ofSeconds(h * 3600 + m * 60  + s);
		
		String times = matcher.group("times");
		
		Optional<Integer> occurences = Optional.empty();
		
		if (times != null) {
			occurences = Optional.of(new Integer(times));
		}
		
		return new RecurringTimer(duration, extractRandomized(matcher), occurences);
	}
	
	String formatApi() {
		StringBuilder builder = new StringBuilder(64);
		apiValue(builder);
		return builder.toString();
	}
	
	protected void appendRandom(StringBuilder builder, Duration duration) {
		if (!duration.isZero()) {
			builder.append("A");
			formatDuration(builder, duration);
		}
	}

	protected void formatDuration(StringBuilder builder, Duration duration) {
		long seconds = duration.getSeconds();
		long hours = seconds / HOURS;
		seconds %= HOURS;
		long minutes = seconds / MINUTES;
		seconds %= MINUTES;
		builder.append(String.format(String.format("%02d:%02d:%02d", hours, minutes, seconds)));
	}
	
	protected abstract void apiValue(StringBuilder builder); 
}
