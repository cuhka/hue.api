package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.SUPPLY_ZERO;
import static com.cuhka.hue.api.BuilderUtils.getBooleanAttribute;
import static com.cuhka.hue.api.BuilderUtils.getEnumAttribute;
import static com.cuhka.hue.api.BuilderUtils.getIntegerAttribute;
import static com.cuhka.hue.api.BuilderUtils.optional;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public class LightState implements Serializable {
	private static final long serialVersionUID = -2558457942944314876L;

	public static class Builder implements Source {
		private Boolean on;
		private Integer bri;
		private HSColor hs;
		private XYColor xy;
		private CTColor ct;
		private Alert alert;
		private Effect effect;
		private ColorMode colormode;
		private Boolean reachable;

		public Builder on(Boolean on) {
			this.on = on;
			return this;
		}
		
		@Override
		public Boolean isOn() {
			return on;
		}

		public Builder brightness(Integer brightness) {
			this.bri = brightness;
			return this;
		}
		
		@Override
		public Integer getBrightness() {
			return bri;
		}

		public Builder xy(XYColor xy) {
			this.xy = xy;
			return this;
		}
		
		@Override
		public XYColor getXYColor() {
			return xy;
		}

		public Builder ct(CTColor ct) {
			this.ct = ct;
			return this;
		}
		
		@Override
		public CTColor getCTColor() {
			return ct;
		}

		public Builder alert(Alert alert) {
			this.alert = alert;
			return this;
		}
		
		@Override
		public Alert getAlert() {
			return alert;
		}

		public Builder effect(Effect effect) {
			this.effect = effect;
			return this;
		}
		
		@Override
		public Effect getEffect() {
			return effect;
		}

		public Builder reachable(Boolean reachable) {
			this.reachable = reachable;
			return this;
		}
		
		@Override
		public Boolean isReachable() {
			return reachable;
		}

		public Builder colormode(ColorMode colormode) {
			this.colormode = colormode;
			return this;
		}
		
		@Override
		public ColorMode getColorMode() {
			return colormode;
		}

		public Builder hs(HSColor hs) {
			this.hs = hs;
			return this;
		}
		
		@Override
		public HSColor getHSColor() {
			return hs;
		}
	}

	public interface Source {
		Boolean isOn();

		Integer getBrightness();

		HSColor getHSColor();
		XYColor getXYColor();
		CTColor getCTColor();

		Alert getAlert();
		Effect getEffect();
		ColorMode getColorMode();

		Boolean isReachable();
	}

	static class UpdateMerger implements Source {
		private final LightState original;
		private final Modifications modifications;
		
		public UpdateMerger(LightState original, Modifications modifications) {
			this.original = original;
			this.modifications = modifications;
		}

		@Override
		public Boolean isOn() {
			return getBooleanAttribute(modifications,"on",() -> original.isOn());
		}

		@Override
		public Integer getBrightness() {
			return getIntegerAttribute(modifications, "bri", () -> original.getBrightness());
		}

		@Override
		public HSColor getHSColor() {
			Optional<Modification> hueMod = modifications.findAttribute("hue");
			Optional<Modification> satMod = modifications.findAttribute("sat");
			HSColor originalColor = original.getHSColor();
			
			int hue = hueMod.isPresent() ? hueMod.get().getNumberValue().intValue() : originalColor.getHue();
			int sat = satMod.isPresent() ? satMod.get().getNumberValue().intValue() : originalColor.getSaturation();
			
			return new HSColor(hue,sat);
		}

		@Override
		public XYColor getXYColor() {
			Optional<Modification> mod = modifications.findAttribute("xy");
			
			if (mod.isPresent()) {
				List<Number> values = mod.get().getNumberListValue();
				double x = values.get(0).doubleValue();
				double y = values.get(1).doubleValue();
				
				return new XYColor(x,y);
			}
			
			return original.getXYColor();
		}

		@Override
		public CTColor getCTColor() {
			Optional<Modification> mod = modifications.findAttribute("ct");
			return mod.isPresent() ? CTColor.fromMired(mod.get().getNumberValue().intValue()) : original.getCTColor();
		}

		@Override
		public Alert getAlert() {
			return getEnumAttribute(Alert.class, modifications,"alert",() -> original.getAlert());
		}
					
		@Override
		public Effect getEffect() {
			return getEnumAttribute(Effect.class, modifications,"effect",() -> original.getEffect());
		}

		@Override
		public ColorMode getColorMode() {
			return getEnumAttribute(ColorMode.class, modifications,"colormode", () -> original.getColormode().orElse(null));
		}

		@Override
		public Boolean isReachable() {
			return getBooleanAttribute(modifications,"reachable",() -> original.isReachable());
		}
	}
	
	private final boolean on;
	private final int bri;
	private final HSColor hs;
	private final XYColor xy;
	private final CTColor ct;
	private final Alert alert;
	private final Effect effect;
	private final ColorMode colormode;
	private final boolean reachable;
	

	public LightState(Source source) {
		this.on = optional(source.isOn(), () -> Boolean.FALSE);
		this.bri = optional(source.getBrightness(), SUPPLY_ZERO);
		this.hs = optional(source.getHSColor(), () -> new HSColor(0,0));
		this.xy = optional(source.getXYColor(), () -> new XYColor(0, 0));
		this.ct = optional(source.getCTColor(), () -> CTColor.fromKelvin(2000));
		this.alert = optional(source.getAlert(), () -> Alert.NONE);
		this.effect = optional(source.getEffect(), () -> Effect.NONE);
		this.colormode = optional(source.getColorMode(), () -> ColorMode.CT);
		this.reachable = optional(source.isReachable(), () -> Boolean.TRUE);
	}

	public boolean isOn() {
		return on;
	}

	public int getBrightness() {
		return bri;
	}

	public HSColor getHSColor() {
		return hs;
	}
	
	public XYColor getXYColor() {
		return xy;
	}

	public CTColor getCTColor() {
		return ct;
	}

	public Alert getAlert() {
		return alert;
	}

	public Effect getEffect() {
		return effect;
	}

	public Optional<ColorMode> getColormode() {
		return Optional.ofNullable(colormode);
	}

	public boolean isReachable() {
		return reachable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alert == null) ? 0 : alert.hashCode());
		result = prime * result + bri;
		result = prime * result + ((colormode == null) ? 0 : colormode.hashCode());
		result = prime * result + ((ct == null) ? 0 : ct.hashCode());
		result = prime * result + ((effect == null) ? 0 : effect.hashCode());
		result = prime * result + ((hs == null) ? 0 : hs.hashCode());
		result = prime * result + (on ? 1231 : 1237);
		result = prime * result + (reachable ? 1231 : 1237);
		result = prime * result + ((xy == null) ? 0 : xy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LightState other = (LightState) obj;
		if (alert != other.alert)
			return false;
		if (bri != other.bri)
			return false;
		if (colormode != other.colormode)
			return false;
		if (ct == null) {
			if (other.ct != null)
				return false;
		} else if (!ct.equals(other.ct))
			return false;
		if (effect != other.effect)
			return false;
		if (hs == null) {
			if (other.hs != null)
				return false;
		} else if (!hs.equals(other.hs))
			return false;
		if (on != other.on)
			return false;
		if (reachable != other.reachable)
			return false;
		if (xy == null) {
			if (other.xy != null)
				return false;
		} else if (!xy.equals(other.xy))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LightState [on=");
		builder.append(on);
		builder.append(", reachable=");
		builder.append(reachable);
		builder.append(", bri=");
		builder.append(bri);
		builder.append(", hs=");
		builder.append(hs);
		builder.append(", xy=");
		builder.append(xy);
		builder.append(", ct=");
		builder.append(ct);
		builder.append(", colormode=");
		builder.append(colormode);		
		builder.append(", alert=");
		builder.append(alert);
		builder.append(", effect=");
		builder.append(effect);
		builder.append("]");
		return builder.toString();
	}
}
