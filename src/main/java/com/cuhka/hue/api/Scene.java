package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.optional;
import static com.cuhka.hue.api.BuilderUtils.optionalMap;
import static com.cuhka.hue.api.BuilderUtils.optionalSet;
import static com.cuhka.hue.api.BuilderUtils.required;

import java.io.Serializable;
import java.time.Instant;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class Scene implements Serializable {
	private static final long serialVersionUID = 357252273045865868L;
	public final static Comparator<Scene> NAME_COMPARATOR = new Comparator<Scene>() {
		@Override
		public int compare(Scene o1, Scene o2) {
			return o1.name.compareToIgnoreCase(o2.name);
		}
	};

	public interface Source {
		/** @since Hue 1.1 */
		String getId();
		
		/** @since Hue 1.1 */
		String getName();
		
		/** @since Hue 1.1 */
		Collection<String> getLights();

		/** @since Hue 1.11 */
		String getOwner();
		
		/** @since Hue 1.11 */
		boolean	isRecycle();
		
		/** @since Hue 1.11 */
		boolean isLocked();
		
		/** @since Hue 1.11 */
		AppData getAppData();
		
		/** @since Hue 1.11 */
		String getPicture();
		
		/** 
		 * @since Hue 1.11
		 */
		Instant getLastUpdated();
		
		/**
		 * @since Hue 1.11
		 */
		int getVersion();
		
		/**
		 * @since Hue 1.11
		 */
		Map<String, LightState> getLightstates();
	}
	
	public static class Builder implements Source {
		private String id;
		private String name;
		private Collection<String> lights;
		private String owner;
		private boolean recycle;
		private boolean locked;
		private AppData appData;
		private String picture;
		private Instant lastUpdated;
		private int version = 1;
		private Map<String, LightState> lightstates;
		
		public Builder id(String id) {
			this.id = id;
			return this;
		}
		
		@Override
		public String getId() {
			return id;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		@Override
		public String getName() {
			return name;
		}

		public Builder lights(Collection<String> lights) {
			this.lights = lights;
			return this;
		}
		
		@Override
		public Collection<String> getLights() {
			return lights;
		}

		public Builder owner(String owner) {
			this.owner = owner;
			return this;
		}
		
		@Override
		public String getOwner() {
			return owner;
		}

		public Builder recycle(boolean recycle) {
			this.recycle = recycle;
			return this;
		}
		
		@Override
		public boolean isRecycle() {
			return recycle;
		}

		public Builder locked(boolean locked) {
			this.locked = locked;
			return this;
		}
		
		@Override
		public boolean isLocked() {
			return locked;
		}

		public Builder appData(AppData appData) {
			this.appData = appData;
			return this;
		}
		
		@Override
		public AppData getAppData() {
			return appData;
		}

		public Builder picture(String picture) {
			this.picture = picture;
			return this;
		}
		
		@Override
		public String getPicture() {
			return picture;
		}

		@Override
		public Instant getLastUpdated() {
			return lastUpdated;
		}

		public Builder version(int version) {
			this.version = version;
			return this;
		}
		@Override
		public int getVersion() {
			return version;
		}
		
		public Builder lightstates(Map<String, LightState> lightstates) {
			this.lightstates = lightstates;
			return this;
		}
		
		@Override
		public Map<String, LightState> getLightstates() {
			return lightstates;
		}
	}
	
	private final String id;
	private final String name;
	private final Set<String> lights;
	private final String owner;
	private final boolean recycle;
	private final boolean locked;
	private final Optional<AppData> appData;
	private final String picture;
	private final Optional<Instant> lastUpdated;
	private final int version;
	private final Map<String, LightState> lightstates;
	
	public Scene(Source source) {
		id = required(source.getId(), "id");
		name = optional(source.getName(), BuilderUtils.SUPPLY_EMPTY_STRING);
		lights = optionalSet(source.getLights());
		owner = optional(source.getOwner(), BuilderUtils.SUPPLY_EMPTY_STRING);
		recycle = source.isRecycle();
		locked = source.isLocked();
		appData = Optional.ofNullable(source.getAppData()); 
		picture = optional(source.getPicture(), BuilderUtils.SUPPLY_EMPTY_STRING);
		lastUpdated = Optional.ofNullable(source.getLastUpdated());
		version = source.getVersion();
		lightstates = optionalMap(source.getLightstates());
	}
	
	public String getId() {
		return id;
	}
	public Set<String> getLights() {
		return lights;
	}

	public String getName() {
		return name;
	}

	public String getOwner() {
		return owner;
	}

	public boolean isRecycle() {
		return recycle;
	}

	public boolean isLocked() {
		return locked;
	}

	public Optional<AppData> getAppData() {
		return appData;
	}

	public String getPicture() {
		return picture;
	}

	public Optional<Instant> getLastUpdated() {
		return lastUpdated;
	}

	public int getVersion() {
		return version;
	}
	
	public Optional<LightState> lookupLightState(String lightId) {
		return Optional.ofNullable(lightstates.get(lightId));
	}
	
	public Map<String, LightState> getLightStates() {
		return lightstates;
	}
}
