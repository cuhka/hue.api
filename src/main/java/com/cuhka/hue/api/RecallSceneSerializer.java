package com.cuhka.hue.api;

import java.io.IOException;

import com.google.gson.stream.JsonWriter;

class RecallSceneSerializer extends DataSerializer<String> {

	@Override
	public void write(String sceneId, JsonWriter writer) throws IOException {
		writer.beginObject();
		
		writer.name("scene");
		writer.value(sceneId);
		
		writer.endObject();
	}

}
