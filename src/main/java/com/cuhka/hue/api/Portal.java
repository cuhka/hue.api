package com.cuhka.hue.api;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import com.google.gson.stream.JsonReader;

public class Portal implements BridgeDiscovery {
	private List<BridgeRecord> discoverLocalBridges() {
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL("https://www.meethue.com/api/nupnp").openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(false);
			connection.setDoInput(true);
			connection.connect();
			try {
				try (JsonReader reader = new JsonReader(new InputStreamReader(connection.getInputStream(), "utf-8"))) {
					return new RegisteredBridgesParser().parse(reader);
				}
			} finally {
				connection.disconnect();
			}
		} catch (IOException e) {
			throw new HueException(e);
		}
	}

	public List<BridgeLocator> discoverBridges() {
		return Collections.unmodifiableList(discoverLocalBridges());
	}
}
