package com.cuhka.hue.api;

import java.io.Serializable;

class Datastore implements Serializable {
	private static final long serialVersionUID = 1198756282640266267L;
	private Lights lights;
	private Config config;
	private Schedules schedules;

	Datastore() {
	}

	public Lights getLights() {
		return lights;
	}

	public final Config getConfig() {
		return config;
	}

	void setLights(Lights lights) {
		this.lights = lights;
	}

	void setConfig(Config config) {
		this.config = config;
	}

	public Schedules getSchedules() {
		return schedules;
	}

	void setSchedules(Schedules schedules) {
		this.schedules = schedules;
	}
}
