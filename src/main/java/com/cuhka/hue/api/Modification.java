package com.cuhka.hue.api;

import java.io.Serializable;
import java.util.List;

public class Modification implements Serializable {
	private static final long serialVersionUID = -8478301334570917406L;
	private final String path;
	private final Object value;

	public Modification(String path, Object value) {
		this.path = path;
		this.value = value;
	}

	public String getPath() {
		return path;
	}

	public <T> T getValue() {
		@SuppressWarnings("unchecked") T t = (T)value;
		return t;
	}

	@SuppressWarnings("unchecked")
	public List<Number> getNumberListValue() {
		return (List<Number>)value;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getStringListValue() {
		return (List<String>)value;
	}
	
	String getStringValue() {
		return (String) value;
	}

	Number getNumberValue() {
		return (Number) value;
	}

	public Boolean getBooleanValue() {
		return (Boolean) value;
	}

	public String getAttribute() {
		int index = path.lastIndexOf('/');
		return path.substring(index + 1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Modification other = (Modification) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Modification [path=");
		builder.append(path);
		builder.append(", value=");
		
		if (value instanceof Number) {
			Number n = (Number)value;
			if ((double)n.intValue() == n.doubleValue()) {
				builder.append(n.intValue());
			} else {
				builder.append(String.format("%2.02f", n.doubleValue()));
			}
		} else {
			builder.append(value);
		}
		
		builder.append(", attribute=");
		builder.append(getAttribute());
		builder.append("]");
		return builder.toString();
	}
}
