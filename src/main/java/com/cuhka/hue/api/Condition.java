package com.cuhka.hue.api;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

public class Condition implements Serializable {
	private static final long serialVersionUID = -1586745645944671851L;
	private final String address;
	private final Operator operator;
	private final Optional<String> value;

	public static class Builder {
		private String address;
		private Operator operator;
		private String value;
		
		public Builder address(String address) {
			this.address = address;
			return this;
		}
		
		public String getAddress() {
			return address;
		}
		
		public Builder operator(Operator operator) {
			this.operator = operator;
			return this;
		}
		
		public Operator getOperator() {
			return operator;
		}
		
		public Builder value(String value) {
			this.value = value;
			return this;
		}
		
		public String getValue() {
			return value;
		}
		
		public Condition build() {
			return new Condition(this);
		}
	}
	
	private Condition(Builder builder) {
		this.address = Objects.requireNonNull(builder.address, "address");
		this.operator = Objects.requireNonNull(builder.operator, "operator");
		this.value = Optional.ofNullable(builder.value);
	}

	public String getAddress() {
		return address;
	}

	public Operator getOperator() {
		return operator;
	}

	public Optional<String> getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((operator == null) ? 0 : operator.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Condition other = (Condition) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (operator != other.operator)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(30);

		builder.append("Condition [");
		builder.append(address);

		String op;
		switch (operator) {
		case EQ:
			op = " =";
			break;

		case GT:
			op = " >";
			break;

		case LT:
			op = " <";
			break;

		case DX:
			op = " <changed>";
			break;

		default:
			op = " <?>";
			break;
		}

		builder.append(op);
		
		value.ifPresent(v -> {
			builder.append(' ');
			builder.append(value);
		});
		
		builder.append("]");

		return builder.toString();
	}
}
