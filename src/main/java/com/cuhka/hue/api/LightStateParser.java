package com.cuhka.hue.api;

import java.io.IOException;

import com.google.gson.stream.JsonReader;

public class LightStateParser extends DataParser<LightState> {
	@Override
	protected LightState parseObject(JsonReader reader) throws IOException {
		LightState.Builder builder = new LightState.Builder();
		
		reader.beginObject();

		int hue = 0;
		int sat = 0;
		
		while (reader.hasNext()) {
			switch(reader.nextName()) {
			default:
				reader.skipValue();
				break;

			case "on":
				builder.on(reader.nextBoolean());
				break;

			case "bri":
				builder.brightness(reader.nextInt());
				break;

			case "hue":
				hue = reader.nextInt();
				break;

			case "sat":
				sat = reader.nextInt();
				break;

			case "xy":
				reader.beginArray();
				builder.xy(new XYColor(reader.nextDouble(),reader.nextDouble()));
				reader.endArray();
				break;

			case "ct":
				builder.ct(CTColor.fromMired(reader.nextInt()));
				break;
				
			case "alert":
				builder.alert(nextEnum(Alert.class, reader));
				break;

			case "effect":
				builder.effect(nextEnum(Effect.class, reader));
				break;
				
			case "colormode":
				builder.colormode(nextEnum(ColorMode.class, reader));
				break;

			case "reachable":
				builder.reachable(reader.nextBoolean());
				
				break;
			}
		}
		
		reader.endObject();
		
		builder.hs(new HSColor(hue, sat));
		
		return new LightState(builder);
	}	
}
