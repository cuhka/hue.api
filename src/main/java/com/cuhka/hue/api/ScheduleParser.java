package com.cuhka.hue.api;

import java.io.IOException;
import java.time.LocalDateTime;

import com.cuhka.hue.api.Schedule.Status;
import com.google.gson.stream.JsonReader;

class ScheduleParser extends DataParser<Schedule> {
	private final String id;
	
	public ScheduleParser(String id) {
		this.id = id;
	}

	@Override
	protected Schedule parseObject(JsonReader reader) throws IOException {
		Schedule.Builder builder = new Schedule.Builder();
		builder.id = id;
		
		reader.beginObject();
		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "name":
				builder.name = reader.nextString();
				break;

			case "description":
				builder.description = reader.nextString();
				break;

			case "command":
				builder.command = readAction(reader);
				break;

			case "created":
				builder.created = LocalDateTime.parse(reader.nextString());
				break;

			case "localtime":
				builder.recurringTimes = TimeTrigger.fromApi(reader.nextString());
				break;

			case "status":
				builder.status = nextEnum(Status.class, reader);
				break;

			case "autodelete":
				builder.autodelete = reader.nextBoolean();
				break;
				
			default:
				reader.skipValue();
				break;
			}
		}
		reader.endObject();
		
		return new Schedule(builder);
	}

}
