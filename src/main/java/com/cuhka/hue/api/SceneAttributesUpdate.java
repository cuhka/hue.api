package com.cuhka.hue.api;

import java.io.Serializable;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class SceneAttributesUpdate implements Serializable {
	private static final Optional<Object> EMPTY = Optional.empty();
	private static final long serialVersionUID = 3660284657351407596L;
	private final static String NAME = "name";
	private final static String LIGHTS = "lights";
	private final static String TRANSITIONTIME = "transitiontime";
	private final static String RECYCLE = "recycle";
	private final static String APPDATA = "appdata";
	private final static String PICTURE = "picture";

	private final Map<String, Optional<?>> attributes = new HashMap<>();

	public static class Builder {
		private final Map<String, Optional<?>> attributes = new HashMap<>();
		public void clear() {
			attributes.clear();
		}

		protected <T> Optional<T> get(String key) {
			@SuppressWarnings("unchecked")
			final Optional<T> value = (Optional<T>)attributes.getOrDefault(key,EMPTY);
			return value;
		}
		protected void set(String key, Object value) {
			attributes.put(key, Optional.ofNullable(value));
		}
		
		public Builder name(String name) {
			set(NAME, name);
			return this;
		}
		
		public Optional<String> getName() {
			return get(NAME);
		}
		
		public Builder lights(String... lightIds) {
			return lights(Arrays.asList(lightIds));
		}
		
		public Builder lights(Collection<String> lights) {
			set(LIGHTS, lights != null ? new LinkedHashSet<>(lights) : null);
			return this;
		}
		
		public Optional<Set<String>> getLights() {
			return get(LIGHTS);
		}
				
		public Optional<Integer> getTransitionTime() {
			return get(TRANSITIONTIME);
		}

		public Builder transitionTime(Integer value) {
			set(TRANSITIONTIME, value);
			return this;
		}
		
		public Builder transitionTime(Optional<Integer> value) {
			attributes.put(TRANSITIONTIME, value);
			return this;
		}
		
		public Optional<Duration> getTransitionDuration() {
			return this.<Integer>get(TRANSITIONTIME).map(d -> Duration.ofMillis(d * 100));
		}
		
		public Builder transitionDuration(Duration value) {
			 return transitionTime((int)(value.toMillis() / 100L));
			 
		}
		
		public Builder transitionDuration(Optional<Duration> value) {
			return transitionTime(value.map(d -> (int)(d.toMillis() / 100L)));
		}
		
		public Builder recycle(Boolean recycle) {
			set(RECYCLE, recycle);
			return this;
		}
		
		public Optional<Boolean> getRecycle() {
			return get(RECYCLE);
		}
		
		public Builder appData(int version, CharSequence data) {
			return appData(new AppData.Builder().version(version).data(data).build());
		}
		
		public Builder appData(AppData appData) {
			set(APPDATA, appData);
			return this;
		}
		
		public Optional<AppData> getAppData() {
			return get(APPDATA);
		}
		
		public Builder picture(String picture) {
			set(PICTURE, picture);
			return this;
		}
		
		public Optional<String> getPicture() {
			return get(PICTURE);
		}
		
		public SceneAttributesUpdate build() {
			return new SceneAttributesUpdate(this);
		}
	}
	
	private SceneAttributesUpdate(Builder source) {
		attributes.putAll(source.attributes);
	}

	private <T> Optional<T> get(String key) {
		@SuppressWarnings("unchecked")
		final Optional<T> value = (Optional<T>)attributes.getOrDefault(key,EMPTY);
		return value;
	}
	
	public Optional<String> getName() {
		return get(NAME);
	}
	
	public Optional<Set<String>> getLights() {
		return get(LIGHTS);
	}
			
	public Optional<Integer> getTransitionTime() {
		return get(TRANSITIONTIME);
	}
	
	public Optional<Duration> getTransitionDuration() {
		return this.<Integer>get(TRANSITIONTIME).map(d -> Duration.ofMillis(d * 100));
	}
		
	public Optional<Boolean> getRecycle() {
		return get(RECYCLE);
	}
	
	public Optional<AppData> getAppData() {
		return get(APPDATA);
	}
		
	public Optional<String> getPicture() {
		return get(PICTURE);
	}
}
