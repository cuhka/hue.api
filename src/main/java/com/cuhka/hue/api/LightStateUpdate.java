package com.cuhka.hue.api;

import java.io.Serializable;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class LightStateUpdate implements Serializable {
	private static final long serialVersionUID = 7159433170395193696L;
	static final String ON = "on";
	static final String BRI = "bri";
	static final String XY = "xy";
	static final String CT = "ct";
	static final String ALERT = "alert";
	static final String EFFECT = "effect";
	static final String TRANSITIONTIME = "transitiontime";
	static final String HS = "HS";
	static final String BRI_INC = "bri_inc";
	static final String SAT_INC = "sat_inc";
	static final String HUE_INC = "hue_inc";
	static final String CT_INC = "ct_inc";
	static final String XY_INC = "xy_inc";

	private Map<String, Optional<?>> attributes = new HashMap<>();

	public final static LightStateUpdate OFF = new Builder().off().build();

	public static class Builder {
		private final Map<String, Optional<?>> attributes = new HashMap<>();

		private <T> T get(String key) {
			@SuppressWarnings("unchecked")
			T value = (T)attributes.getOrDefault(key, Optional.empty());
			return value;
		}

		protected void set(String key, Object value) {
			attributes.put(key, Optional.ofNullable(value));
		}

		public Builder as(LightState lightstate) {
			on(lightstate.isOn());
			brightness(lightstate.getBrightness());

			lightstate.getColormode().ifPresent(colormode -> {
				switch (colormode) {
				case HS:
					hs(lightstate.getHSColor());
					break;

				case CT:
					ct(lightstate.getCTColor());
					break;

				case XY:
					xy(lightstate.getXYColor());
					break;
				}
			});

			alert(lightstate.getAlert());
			effect(lightstate.getEffect());

			return this;
		}
		
		public Builder on() {
			return on(Boolean.TRUE);
		}

		public Builder off() {
			return on(Boolean.FALSE);
		}

		public Builder on(Boolean on) {
			set(ON, on);
			return this;
		}

		public Optional<Boolean> getOn() {
			return get(ON);
		}

		public Builder brightness(Integer value) {
			set(BRI, value);
			return this;
		}

		public Optional<Integer> getBrightness() {
			return get(BRI);
		}

		public Optional<HSColor> getHS() {
			return get(HS);
		}

		public Builder hs(HSColor value) {
			set(HS, value);
			return this;
		}

		public Builder hs(int hue, int saturation) {
			hs(new HSColor(hue, saturation));
			return this;
		}

		public Optional<XYColor> getXY() {
			return get(XY);
		}

		public Builder xy(XYColor xy) {
			set(XY, xy);
			return this;
		}

		public Optional<CTColor> getColorTemperature() {
			return get(CT);
		}

		public Builder ct(CTColor value) {
			set(CT, value);
			return this;
		}

		public Builder alert(Alert alert) {
			set(ALERT, alert);
			return this;
		}

		public Optional<Alert> getAlert() {
			return get(ALERT);
		}


		public Builder effect(Effect effect) {
			set(EFFECT, effect);
			return this;
		}

		public Optional<Effect> getEffect() {
			return get(EFFECT);
		}


		public Optional<Integer> getTransitionTime() {
			return get(TRANSITIONTIME);
		}

		public Builder transitionTime(Integer value) {
			set(TRANSITIONTIME, value);
			return this;
		}

		public Optional<Duration> getTransitionDuration() {
			return getTransitionTime().map(d -> Duration.ofMillis(d * 100));
		}

		public Builder transitionDuration(Duration value) {
			long time = value.toMillis() / 100L;
			transitionTime((int)time);
			return this;
		}

		/** @since Hue API 1.7 */
		public Optional<Integer> getBrightnessIncrement() {
			return get(BRI_INC);
		}

		public Builder briInc(Integer value) {
			set(BRI_INC, value);
			return this;
		}

		/** @since Hue API 1.7 */
		public Optional<Integer> getSaturationIncrement() {
			return get(SAT_INC);
		}

		public Builder satInc(Integer value) {
			set(SAT_INC, value);
			return this;
		}

		/** @since Hue API 1.7 */
		public Optional<Integer> getHueIncrement() {
			return get(HUE_INC);
		}

		public Builder hueInc(Integer value) {
			set(HUE_INC, value);
			return this;
		}

		public Builder ctInc(Integer value) {
			set(CT_INC, value);
			return this;
		}

		/** @since Hue API 1.7 */
		public Optional<XYInc> getXYIncrement() {
			return get(XY_INC);
		}

		public Builder xyInc(XYInc value) {
			set(XY_INC, value);
			return this;
		}
		
		public Builder xyInc(double dx, double dy) {
			return xyInc(new XYInc(dx, dy));
		}

		public void clear() {
			attributes.clear();
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder(512);

			builder.append("LightStateUpdate.Builder [");

			builder.append(toDataString(attributes));

			builder.append("]");

			return builder.toString();
		}

		public LightStateUpdate build() {
			return new LightStateUpdate(this);
		}
	}

	private LightStateUpdate(Builder source) {
		attributes.putAll(source.attributes);
	}

	private <T> T get(String key) {
		@SuppressWarnings("unchecked")
		T value = (T)attributes.getOrDefault(key, Optional.empty());
		return value;
	}

	public Optional<Boolean> getOn() {
		return get(ON);
	}

	public Optional<Integer> getBrightness() {
		return get(BRI);
	}

	public Optional<HSColor> getHS() {
		return get(HS);
	}

	public Optional<XYColor> getXY() {
		return get(XY);
	}

	public Optional<CTColor> getColorTemperature() {
		return get(CT);
	}

	public Optional<Alert> getAlert() {
		return get(ALERT);
	}

	public Optional<Effect> getEffect() {
		return get(EFFECT);
	}

	public Optional<Integer> getTransitionTime() {
		return get(TRANSITIONTIME);
	}

	public void setTransitionTime(Optional<Integer> value) {
		attributes.put(TRANSITIONTIME, value);
	}

	public Optional<Duration> getTransitionDuration() {
		return getTransitionTime().map(i -> Duration.ofMillis(i * 100));
	}

	/** @since Hue API 1.7 */
	public Optional<Integer> getBrightnessIncrement() {
		return get(BRI_INC);
	}

	/** @since Hue API 1.7 */
	public Optional<Integer> getSaturationIncrement() {
		return get(SAT_INC);
	}

	/** @since Hue API 1.7 */
	public Optional<Integer> getHueIncrement() {
		return get(HUE_INC);
	}

	/** @since Hue API 1.7 */
	public Optional<Integer> getColorTemperatureIncrement() {
		return get(CT_INC);
	}

	/** @since Hue API 1.7 */
	public Optional<XYInc> getXYIncrement() {
		return get(XY_INC);
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(512);

		builder.append("LightStateUpdate [");
		builder.append(toDataString(attributes));
		builder.append("]");

		return builder.toString();
	}

	private static String toDataString(Map<String, Optional<?>> attributes) {
		return attributes.entrySet().stream()
				.filter(e -> e.getValue().isPresent())
				.map(e -> String.format("%s=%s", e.getKey(), e.getValue().get()))
				.collect(Collectors.joining(", "));
	}
}
