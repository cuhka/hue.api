package com.cuhka.hue.api;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

public class NewLights implements Serializable {
	private static final long serialVersionUID = 3932023190285813097L;
	private final Set<LightSummary> lights;
	private final Optional<LocalDateTime> lastscan;
	private final String lastscanText;
	private final boolean scanning;
	
	/**
	 * @param lights
	 * @param lastscan
	 * @param scanning
	 */
	public NewLights(Collection<LightSummary> lights, Optional<LocalDateTime> lastscan, String lastscanText, boolean scanning) {
		this.lights = Collections.unmodifiableSet(new LinkedHashSet<>(lights));
		this.lastscan = lastscan;
		this.lastscanText = lastscanText;
		this.scanning = scanning;
	}

	public Set<LightSummary> getLights() {
		return lights;
	}

	public Optional<LocalDateTime> getLastScan() {
		return lastscan;
	}

	public boolean isScanning() {
		return scanning;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NewLights [scanning=");
		builder.append(scanning);
		builder.append(", lastscan=");
		if (lastscan.isPresent()) {
			builder.append(lastscan.get());
		} else {
			builder.append(lastscanText);
		}
		
		builder.append(", lights=");
		builder.append(lights);
		builder.append("]");
		return builder.toString();
	}
}
