package com.cuhka.hue.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.stream.JsonReader;

class ScenesParser extends DataParser<Scenes> {
	@Override
	protected Scenes parseObject(JsonReader reader) throws IOException {
		Collection<Scene> scenes = new ArrayList<>();
		reader.beginObject();
		
		while (reader.hasNext()) {
			String id = reader.nextName();
			SceneParser parser = new SceneParser(id);
			
			scenes.add(parser.parse(reader));
		}
		
		reader.endObject();
		return new Scenes(scenes);
	}
}
