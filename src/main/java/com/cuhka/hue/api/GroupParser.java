package com.cuhka.hue.api;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import com.google.gson.stream.JsonReader;

public class GroupParser extends DataParser<Group> {
	private final String id;

	public GroupParser(String id) {
		this.id = id;
	}

	protected Group parseObject(JsonReader reader) throws IOException {
		Group.Builder builder = new Group.Builder(id);
		reader.beginObject();
		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "action":
				builder.lightState = new LightStateParser().parse(reader);
				break;
				
			case "name":
				builder.name = reader.nextString();
				break;

			case "type":
				builder.type = GroupType.fromGroupType(reader.nextString());
				break;
				
			case "class":
				builder.locationClass = Location.fromClassString(reader.nextString());
				break;
				
			case "modelid":
				builder.modelId = reader.nextString();
				break;
				
			case "lights":
				reader.beginArray();
				Collection<String> ids = new LinkedList<>();

				while (reader.hasNext()) {
					ids.add(reader.nextString());
				}

				reader.endArray();
				builder.lightIds = ids;
				break;

			default:
				reader.skipValue();
				break;
			}
		}
		reader.endObject();
		return new Group(builder);
	}
}
