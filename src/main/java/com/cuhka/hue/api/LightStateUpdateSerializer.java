package com.cuhka.hue.api;

import java.io.IOException;
import java.util.Optional;

import com.google.gson.stream.JsonWriter;

class LightStateUpdateSerializer extends DataSerializer<LightStateUpdate>  {
	@Override
	public void write(LightStateUpdate update, JsonWriter writer) throws IOException {
		writer.beginObject();
		writeBoolean(writer, "on", update.getOn());
		writeInteger(writer, "bri", update.getBrightness());
		writeHS(writer, update.getHS());
		writeCT(writer, update.getColorTemperature());
		writeXY(writer, update.getXY());
		writeEnum(writer, "alert", update.getAlert());
		writeEnum(writer, "effect", update.getEffect());
		writeInteger(writer, "transitiontime", update.getTransitionTime());
		writeInteger(writer, "bri_inc", update.getBrightnessIncrement());
		writeInteger(writer, "sat_inc", update.getSaturationIncrement());
		writeInteger(writer, "hue_inc", update.getHueIncrement());
		writeInteger(writer, "ct_inc", update.getColorTemperatureIncrement());
		writeXYIncrement(writer, "xy_inc", update.getXYIncrement());
		writer.endObject();
	}

	private void writeXYIncrement(JsonWriter writer, String key, Optional<XYInc> value) throws IOException {
		if (value.isPresent()) {
			XYInc inc = value.get();
			writer.name(key).beginArray();
			writer.value(inc.getDeltaX());
			writer.value(inc.getDeltaY());
			writer.endArray();
		}
	}

	private void writeCT(JsonWriter writer, Optional<CTColor> value) throws IOException {
		if (value.isPresent()) {
			writer.name("ct");
			writer.value(value.get().inMired());
		}
	}

	private void writeHS(JsonWriter writer, Optional<HSColor> value) throws IOException {
		if (value.isPresent()) {
			HSColor color = value.get();
			writer.name("hue");
			writer.value(color.getHue());
			writer.name("sat");
			writer.value(color.getSaturation());
		}
	}

	private void writeXY(JsonWriter writer, Optional<XYColor> value) throws IOException {
		if (value.isPresent()) {
			XYColor color = value.get();
			writer.name("xy");
			writer.beginArray();
			writer.value(color.getX());
			writer.value(color.getY());
			writer.endArray();
		}
	}
}
