package com.cuhka.hue.api;

import static com.cuhka.hue.api.BuilderUtils.optional;

import java.io.Serializable;

public class BridgeInfo implements Serializable {
	private static final long serialVersionUID = -7951469109326338198L;

	public static class Builder implements Source {
		public String name;
		public String apiversion;
		public String swversion;
		public String mac;

		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getApiversion() {
			return apiversion;
		}

		@Override
		public String getSoftwareVersion() {
			return swversion;
		}

		@Override
		public String getMacAddress() {
			return mac;
		}
		
		public BridgeInfo build() {
			return new BridgeInfo(this);
		}
	}

	public interface Source {
		String getName();

		String getApiversion();

		String getSoftwareVersion();

		String getMacAddress();
	}

	private final String name;
	private final String apiversion;
	private final String swversion;
	private final String mac;

	public BridgeInfo(Source source) {
		name = optional(source.getName());
		apiversion = optional(source.getApiversion());
		swversion = optional(source.getSoftwareVersion());
		mac = optional(source.getMacAddress());
	}

	public String getName() {
		return name;
	}

	public String getApiversion() {
		return apiversion;
	}

	public String getSoftwareVersion() {
		return swversion;
	}

	public String getMacAddress() {
		return mac;
	}
}
