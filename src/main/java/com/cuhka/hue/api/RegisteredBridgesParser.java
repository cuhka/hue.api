package com.cuhka.hue.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.stream.JsonReader;

class RegisteredBridgesParser {
	public List<BridgeRecord> parse(JsonReader reader) throws IOException {
		List<BridgeRecord> records = new ArrayList<>(1);

		reader.beginArray();

		while (reader.hasNext()) {
			records.add(parseRecord(reader));
		}

		reader.endArray();
		return records;
	}

	private BridgeRecord parseRecord(JsonReader reader) throws IOException {
		reader.beginObject();
		String id = "";
		String ip = "";

		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "id": 
				id = reader.nextString();
				break;

			case "internalipaddress":
				ip = reader.nextString();
				break;

			default:
				reader.skipValue();
				break;
			}
		}

		reader.endObject();
		
		return new BridgeRecord(id,ip);
	}
}
