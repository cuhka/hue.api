package com.cuhka.hue.api;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import com.google.gson.stream.JsonReader;

class SchedulesParser extends DataParser<Schedules> {	
	@Override
	protected Schedules parseObject(JsonReader reader) throws IOException {
		Set<Schedule> result = new LinkedHashSet<>();

		reader.beginObject();

		while (reader.hasNext()) {
			result.add(new ScheduleParser(reader.nextName()).parse(reader));
		}
		reader.endObject();

		return new Schedules(result);
	}
}
