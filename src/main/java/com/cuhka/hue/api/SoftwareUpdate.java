package com.cuhka.hue.api;

import java.io.Serializable;

public class SoftwareUpdate implements Serializable {
	private static final long serialVersionUID = -8003173779494522705L;
	private final int updateState;
	private final String url;
	private final String text;
	private final boolean notify;

	public SoftwareUpdate(int updateState, String url, String text, boolean notify) {
		this.updateState = updateState;
		this.url = url;
		this.text = text;
		this.notify = notify;
	}

	public final int getUpdateState() {
		return updateState;
	}

	public final String getUrl() {
		return url;
	}

	public final String getText() {
		return text;
	}

	public final boolean isNotify() {
		return notify;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SoftwareUpdate))
			return false;
		SoftwareUpdate other = (SoftwareUpdate) obj;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
}
