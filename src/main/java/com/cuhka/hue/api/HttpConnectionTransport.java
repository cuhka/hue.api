package com.cuhka.hue.api;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

// TODO Check mockito on how to test this class
class HttpConnectionTransport implements Transport {
	private static final Charset UTF_8 = Charset.forName("utf-8");
	private static final String API_ROOT = "/api/";
	private final int port;
	private final String host;
	private final String baseURL;

	HttpConnectionTransport(String host, int port, String clientId) {
		this.host = requireNonNull(host);
		this.port = port;
		this.baseURL = clientId != null ? API_ROOT + clientId : API_ROOT;
	}

	@Override
	public Modifications delete(CharSequence path) {
		try {
			HttpURLConnection connection = (HttpURLConnection) toURL(path).openConnection();
			connection.setRequestMethod("DELETE");
			connection.connect();

			try {
				try (JsonReader reader = new JsonReader(new InputStreamReader(connection.getInputStream(), UTF_8));) {
					return new ModificationsParser().parse(reader);
				}
			} finally {
				connection.disconnect();
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	@Override
	public <T> T get(DataParser<? extends T> parser, CharSequence path) {
		try {
			HttpURLConnection connection = (HttpURLConnection) toURL(path).openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(false);

			connection.connect();
			try {
				try (JsonReader reader = new JsonReader(new InputStreamReader(connection.getInputStream(), UTF_8))) {
					return parser.parse(reader);
				}
			} finally {
				connection.disconnect();
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	@Override
	public <T> Modifications post(T data, DataSerializer<? super T> serializer, CharSequence path) {
		return request(data, serializer, path, "POST");
	}

	private <T> Modifications request(T data, DataSerializer<T> serializer, CharSequence path, String method) {
		try {
			HttpURLConnection connection = (HttpURLConnection) toURL(path).openConnection();

			connection.setRequestMethod(method);
			connection.setDoOutput(data != null);
			connection.connect();
			try {
				if (data != null) {
					try (JsonWriter writer = new JsonWriter(new OutputStreamWriter(connection.getOutputStream(), UTF_8))) {
						serializer.write(data, writer);
					}
				}

				try (JsonReader reader = new JsonReader(new InputStreamReader(connection.getInputStream(), UTF_8));) {
					return new ModificationsParser().parse(reader);
				}
			} finally {
				connection.disconnect();
			}
		} catch (IOException e) {
			throw new HueException(e);
		}
	}

	@Override
	public <T> Modifications put(T data, DataSerializer<? super T> serializer, CharSequence path) {
		return request(data, serializer, path, "PUT");
	}

	private URL toURL(CharSequence path) throws MalformedURLException {
		StringBuilder file = new StringBuilder(baseURL.length() + path.length());
		file.append(baseURL);
		file.append(path);

		return new URL("http", host, port, file.toString()); 
	}
}