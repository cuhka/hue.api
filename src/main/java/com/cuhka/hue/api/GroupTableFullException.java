package com.cuhka.hue.api;


public class GroupTableFullException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public GroupTableFullException(HueError error) {
		super(error);
	}
}
