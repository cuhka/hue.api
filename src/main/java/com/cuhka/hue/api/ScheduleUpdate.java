package com.cuhka.hue.api;

import java.util.Optional;

public class ScheduleUpdate {
	private final Optional<String> name;
	private final Optional<String> description;
	private final Optional<Action> command;
	private final Optional<TimeTrigger> localtime;
	private final Optional<Schedule.Status> status;
	private final Optional<Boolean> autodelete;
	
	public final static class Builder {
		private String name;
		private String description;
		private Action command;
		private TimeTrigger localtime;
		private Schedule.Status status;
		private Boolean autodelete;
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Builder descrption(String description) {
			this.description = description;
			return this;
		}
		
		public Builder command(Action command) {
			this.command = command;
			return this;
		}
		
		
		public Builder localtime(TimeTrigger localtime) {
			this.localtime = localtime;
			return this;
		}
		
		public Builder status(Schedule.Status status) {
			this.status = status;
			return this;
		}
		
		public Builder autodelete(Boolean autodelete) {
			this.autodelete = autodelete;
			return this;
		}

		public String getName() {
			return name;
		}

		public String getDescription() {
			return description;
		}

		public Action getCommand() {
			return command;
		}

		public TimeTrigger getLocaltime() {
			return localtime;
		}

		public Schedule.Status getStatus() {
			return status;
		}

		public Boolean getAutodelete() {
			return autodelete;
		}

		public ScheduleUpdate build() {
			return new ScheduleUpdate(this);
		}
	}
	
	private ScheduleUpdate(Builder builder) {
		this.name = Optional.ofNullable(builder.getName());
		this.description = Optional.ofNullable(builder.description);
		this.command = Optional.ofNullable(builder.command);
		this.localtime = Optional.ofNullable(builder.localtime);
		this.status = Optional.ofNullable(builder.status);
		this.autodelete = Optional.ofNullable(builder.autodelete);
	}

	public Optional<String> getName() {
		return name;
	}

	public Optional<String> getDescription() {
		return description;
	}

	public Optional<Action> getCommand() {
		return command;
	}

	public Optional<TimeTrigger> getLocaltime() {
		return localtime;
	}

	public Optional<Schedule.Status> getStatus() {
		return status;
	}

	public Optional<Boolean> getAutodelete() {
		return autodelete;
	}
}
