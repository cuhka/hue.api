package com.cuhka.hue.api;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.google.gson.stream.JsonWriter;

class RuleUpdateSerializer extends DataSerializer<RuleUpdate> {
	@Override
	public void write(RuleUpdate data, JsonWriter writer) throws IOException {
		writer.beginObject();
		
		writeString(writer, "name", data.getName());
		writeEnum(writer, "status", data.getStatus());
		
		writeConditions(writer, data.getConditions());
		writeActions(writer, data.getActions());
		writer.endObject();
	}


	private void writeConditions(JsonWriter writer, Optional<List<Condition>> conditions) throws IOException {
		if (conditions.isPresent()) {
			writer.name("conditions");
			writer.beginArray();
			List<Condition> list = conditions.get();
			
			for (Condition condition : list) {
				writer.beginObject();
				writer.name("address").value(condition.getAddress());
				writer.name("operator").value(condition.getOperator().name().toLowerCase());
				
				String value = condition.getValue().orElse(null);
				
				if (value != null) {
					writer.name("value").value(value);
				}
				
				writer.endObject();
			}
			
			writer.endArray();
		}
	}
	
	private void writeActions(JsonWriter writer, Optional<List<Action>> actions) throws IOException {
		if (actions.isPresent()) {
			writer.name("actions");
			writer.beginArray();
			List<Action> list = actions.get();
			
			for (Action action : list) {
				writeAction(writer, action);
			}
			
			writer.endArray();
		}
	}

}
