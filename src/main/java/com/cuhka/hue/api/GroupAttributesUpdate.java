package com.cuhka.hue.api;

import static java.util.Arrays.asList;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.gson.stream.JsonWriter;

public class GroupAttributesUpdate implements Serializable {
	private static final long serialVersionUID = 8307629448461445778L;

	static class Serializer extends DataSerializer<GroupAttributesUpdate> {
		@Override
		public void write(GroupAttributesUpdate data, JsonWriter writer) throws IOException {
			writer.beginObject();
			writeString(writer, "name", data.getName());
			writeString(writer, "type", data.getType().map(t -> t.toString()));
			writeString(writer, "class", data.getLocationClass().map(c -> c.toString()));
			writeArray(writer, "lights", data.getLightIds());
			writer.endObject();
		}
	}

	public static class Builder {
		private String name;
		private Collection<String> ids = null;
		private Optional<GroupType> type = Optional.empty();
		private Optional<Location> locationClass = Optional.empty();


		public Optional<String> getName() {
			return Optional.ofNullable(name);
		}

		public Optional<Location> getLocationClass() {
			return locationClass;
		}

		public Optional<Collection<String>> getLightIds() {
			return Optional.ofNullable(ids);
		}

		public Builder name(String value) {
			name = value;
			return this;
		}

		public Builder lights(Light... lights) {
			lights(asList(lights));
			return this;
		}

		public Builder lights(Collection<? extends Light> lights) {
			lightsIds(lights.stream().map((l) -> l.getId()).collect(Collectors.toList()));	
			return this;
		}

		public Builder lightIds(String... ids) {
			lightsIds(asList(ids));
			return this;
		}

		public Builder lightsIds(Collection<String> ids) {
			this.ids = ids;
			return this;
		}

		public Builder type(GroupType groupType) {
			type = Optional.ofNullable(groupType);
			return this;
		}

		public Builder type(Optional<GroupType> groupType) {
			type = groupType;
			return this;
		}

		public Optional<GroupType> getType() {
			return type;
		}

		public Builder location(Location locationClass) {
			location(Optional.ofNullable(locationClass));
			return this;
		}

		public Builder location(Optional<Location> locationClass) {
			this.locationClass = locationClass;
			return this;
		}

		public GroupAttributesUpdate build() {
			return new GroupAttributesUpdate(this);
		}
	}

	private final Optional<String> name;
	private final Optional<Collection<String>> lightIds;
	private final Optional<GroupType> type;
	private final Optional<Location> locationClass;

	private GroupAttributesUpdate(Builder builder) {
		name = builder.getName();
		lightIds = builder.getLightIds();
		type = builder.getType();
		locationClass = builder.getLocationClass();
	}

	public Optional<String> getName() {
		return name;
	}

	public Optional<Collection<String>> getLightIds() {
		return lightIds;
	}

	public Optional<GroupType> getType() {
		return type;
	}

	public Optional<Location> getLocationClass() {
		return locationClass;
	}

	@Override
	public String toString() {
		return "GroupAttributesUpdate [name=" + name + ", lightIds=" + lightIds + ", type=" + type + ", locationClass="
				+ locationClass + "]";
	}
}

