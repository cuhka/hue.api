package com.cuhka.hue.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

class ModificationsParser extends DataParser<Modifications> {
	@Override
	public Modifications parse(JsonReader reader) throws IOException {
		return parseObject(reader);
	}
	
	@Override
	protected Modifications parseObject(JsonReader reader) throws IOException {
		List<Modification> successes = new ArrayList<>();
		List<HueError> errors = new ArrayList<>();

		reader.beginArray();

		while (reader.hasNext()) {
			parse(reader, successes, errors);
		}
		reader.endArray();

		Modifications.Builder builder = new Modifications.Builder();

		builder.successes = successes;
		builder.errors = errors;

		return new Modifications(builder);
	}

	private void parse(JsonReader reader, List<Modification> successes, List<HueError> errors) throws IOException {
		reader.beginObject();

		switch (reader.nextName()) {
		case "success":
			successes.add(readSuccess(reader));
			break;

		case "error":
			errors.add(readError(reader));
			break;

		default:
			reader.skipValue();
			break;
		}

		reader.endObject();
	}

	private HueError readError(JsonReader reader) throws IOException {
		reader.beginObject();
		HueError.Builder builder = new HueError.Builder();

		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "type":
				builder.type = reader.nextInt();
				break;

			case "address":
				builder.address = reader.nextString();
				break;

			case "description":
				builder.description = reader.nextString();
				break;
			default:
				reader.skipValue();
				break;
			}
		}

		HueError error = new HueError(builder);
		reader.endObject();
		return error;
	}

	private Modification readSuccess(JsonReader reader) throws IOException {
		JsonToken token = reader.peek();
		switch (token) {
		case BEGIN_OBJECT:
			return readSuccessObject(reader);

		case STRING:
			return readSuccessLine(reader);

		default:
			throw new IOException("Unexpected structure of success record, token=" + token);
		}
	}

	private Modification readSuccessLine(JsonReader reader) throws IOException {
		String line = reader.nextString();
		String path,value;
		int index = line.lastIndexOf(' ');
		
		if (index < 0) {
			path = line;
			value = "";
		} else {
			path = line.substring(0, index);
			value = line.substring(index + 1);
		}

		return new Modification(path, value);
	}

	private Modification readSuccessObject(JsonReader reader) throws IOException {
		Modification result;
		reader.beginObject();
		result = new Modification(reader.nextName(), readValue(reader));
		reader.endObject();
		return result;
	}
}
