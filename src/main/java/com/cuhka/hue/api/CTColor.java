package com.cuhka.hue.api;

import java.io.Serializable;

public class CTColor implements Comparable<CTColor>, Serializable {
	private static final long serialVersionUID = 1556693409576829381L;

	private final int mired;

	public final static CTColor CONCENTRATE = CTColor.fromMired(234);
	public final static CTColor RELAX = CTColor.fromMired(467);
	public final static CTColor READING = CTColor.fromMired(346);
	public final static CTColor STIMULATE = CTColor.fromMired(156);	
	public final static CTColor MATCHFLAME = fromKelvin(1750);
	public final static CTColor CANDLELIGHT = fromKelvin(1500);
	public final static CTColor INCANDESCENT_LAMP_40W = fromKelvin(2680);
	public final static CTColor INCANDESCENT_LAMP_200W = fromKelvin(3000);
	public final static CTColor TUNGSTEN_LAMP = fromKelvin(3400);
	public final static CTColor HR_DUSK_DAWN = fromKelvin(3400);
	public final static CTColor SUNRISE_SUNSET = fromKelvin(3200);
	public final static CTColor SUNNY_NOON = fromKelvin(5500);
	public final static CTColor OVERCAST_SKY = fromKelvin(7000);
	public final static CTColor BLUE_SKY = fromKelvin(10_500);

	public static CTColor fromMired(int mired) {
		return new CTColor(mired);
	}

	public static CTColor fromKelvin(int kelvin) {
		if (kelvin <= 0) {
			throw new IllegalArgumentException();
		}
		
		return fromMired(1_000_000 / kelvin);
	}

	private CTColor(int mired) {
		this.mired = mired;
	}

	public int inKelvin() {
		return (int) (1_000_000d * (1d / (double) mired));
	}

	public int inMired() {
		return mired;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}

		if (object == this) {
			return true;
		}

		if (object instanceof CTColor) {
			CTColor other = (CTColor) object;
			return mired == other.mired;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return mired;
	}

	@Override
	public String toString() {
		return String.format("CT [k=%d m=%d]", inKelvin(), inMired());
	}

	@Override
	public int compareTo(CTColor o) {
		return o.mired - mired;
	}
}
