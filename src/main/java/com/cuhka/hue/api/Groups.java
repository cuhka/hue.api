package com.cuhka.hue.api;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Groups extends ImmutableSet<Group> {
	public Groups(Collection<? extends Group> groups) {
		super(groups);
	}

	public Optional<Group> find(Predicate<? super Group> p) {
		return stream().filter(p).findAny();
	}

	public Groups findContainingLights(String... lightIds) {
		return findContainingLights(Arrays.asList(lightIds));
	}

	public Groups findContainingLights(Collection<String> lightIds) {
		return filter((g) -> lightIds.contains(g.getId()));
	}

	public Optional<Group> findById(String id) {
		return find((l) -> l.getId().equals(id));
	}

	public Optional<Group> findByName(String name) {
		return find((l) -> l.getName().equals(name));
	}

	public Groups filter(Predicate<? super Group> p) {
		return new Groups(stream().filter(p).collect(Collectors.toSet()));
	}
}
