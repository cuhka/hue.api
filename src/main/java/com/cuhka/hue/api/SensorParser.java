package com.cuhka.hue.api;

import java.io.IOException;

import com.google.gson.stream.JsonReader;

public class SensorParser extends DataParser<Sensor> {
	private final String id;
	
	public SensorParser(String id) {
		this.id = id;
	}

	@Override
	protected Sensor parseObject(JsonReader reader) throws IOException {
		Sensor.Builder builder = new Sensor.Builder();
		builder.id(id);

		reader.beginObject();
		
		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "name":
				builder.name(reader.nextString());
				break;
				
			case "type":
				builder.type(reader.nextString());
				break;
				
			case "modelid":
				builder.modelId(reader.nextString());
				break;
				
			case "manufacturername":
				builder.manufacturer(reader.nextString());
				break;
				
			case "swversion":
				builder.swVersion(reader.nextString());
				break;
				
			case "uniqueid":
				builder.uuid(reader.nextString());
				break;
				
			case "config":
				builder.config(readMap(reader));
				break;

			case "state":
				builder.state(readMap(reader));
				break;
				
			default:
				reader.skipValue();
				break;

			}
		}
		
		reader.endObject();
		return builder.build();
	}

	
}
