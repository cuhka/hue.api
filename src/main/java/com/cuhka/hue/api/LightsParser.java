package com.cuhka.hue.api;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import com.google.gson.stream.JsonReader;

public class LightsParser extends DataParser<Lights> {
	@Override
	protected Lights parseObject(JsonReader reader) throws IOException {
		Set<Light> lights = new LinkedHashSet<Light>();

		reader.beginObject();

		while (reader.hasNext()) {
			String id = reader.nextName();
			Light light = new LightParser(id).parse(reader);
			lights.add(light);
		}

		reader.endObject();

		return new Lights(lights);
	}
}
