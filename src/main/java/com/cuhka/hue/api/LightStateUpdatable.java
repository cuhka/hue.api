package com.cuhka.hue.api;


public interface LightStateUpdatable<T> {
	public abstract LightState getLightState();
	public abstract Update<T> updateLightState(LightStateUpdate update);
}