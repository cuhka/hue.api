package com.cuhka.hue.api;

import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;
import static java.util.Collections.unmodifiableSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Collection;
import java.util.function.Supplier;

final class BuilderUtils {
	/** Supplies an empty string (""). */
	final static Supplier<String> SUPPLY_EMPTY_STRING = () -> "";
	
	/** Supplies an integer with value 0. */
	final static Supplier<Integer> SUPPLY_ZERO = () -> Integer.valueOf(0);

	/** 
	 * Returns the value if it is not null, raises an IllegalArgumentException otherwise.
	 * 
	 * @param value value to check and return
	 * @param name name of parameter to use in exception
	 * @throws IllegalArgumentException if value is null
	 * @return value passed
	 */
	final static <T> T required(T value, String name) {
		return required(value, () -> new IllegalArgumentException("Value " + name + " may not be null "));
	}

	/**
	 * Returns the value if it not null, raises the supplied exception if otherwise
	 * @param value value to check and return
	 * @param orElseThrow supplier for exception to be thrown
	 * @return value passed
	 * @throws RuntimeException exception supplied by supplier
	 */
	final static <T> T required(T value, Supplier<? extends RuntimeException> orElseThrow) {
		if (value != null) {
			return value;
		}

		throw orElseThrow.get();
	}

	/**
	 * Returns the value if the value is not null, otherwise an empty string
	 * @param value value to return
	 * @return value, or ""
	 */
	final static <T> String optional(String value) {
		return optional(value, SUPPLY_EMPTY_STRING);
	}

	final static <K, V> Map<K, V> optional(Map<K, V> values) {
		return unmodifiableMap(new LinkedHashMap<K, V>(optional(values, Collections::emptyMap)));
	}

	final static <T> Set<T> optionalSet(Collection<? extends T> values) {
		return unmodifiableSet(new LinkedHashSet<T>(optional(values, Collections::emptySet)));
	}

	final static <T> List<T> optionalList(Collection<? extends T> values) {
		return unmodifiableList(new ArrayList<T>(optional(values, Collections::emptyList)));
	}

	final static <T> T optional(T value, Supplier<? extends T> orElseReturn) {
		return value != null ? value : orElseReturn.get();
	}

	final static String getStringAttribute(Modifications modification, String attribute, Supplier<String> supplier) {
		Optional<Modification> value = modification.findAttribute(attribute);
		return value.isPresent() ? value.get().getStringValue() : supplier.get();
	}
	
	final static Boolean getBooleanAttribute(Modifications modification, String attribute, Supplier<Boolean> supplier) {
		Optional<Modification> value = modification.findAttribute(attribute);
		return value.isPresent() ? value.get().getBooleanValue() : supplier.get();
	}
	
	final static Integer getIntegerAttribute(Modifications modification, String attribute, Supplier<Integer> supplier) {
		Optional<Modification> value = modification.findAttribute(attribute);
		return value.isPresent() ? Integer.valueOf(value.get().getNumberValue().intValue()) : supplier.get();
		
	}

	final static <E extends Enum<E>> E getEnumAttribute(Class<E> enumClass, Modifications modifications, String attribute, Supplier<E> supplier) {
		Optional<Modification> mod = modifications.findAttribute(attribute);
		
		E value;
		if (mod.isPresent()) {
			value = Enum.valueOf(enumClass, mod.get().getStringValue().toUpperCase());
		} else {
			value = supplier.get();
		}
		
		return value;
	}

	public static <K,V> Map<K,V> optionalMap(Map<K, V> value) {
		if (value == null || value.isEmpty()) {
			return Collections.emptyMap();
		} 

		return unmodifiableMap(new LinkedHashMap<>(value));
	}
}
