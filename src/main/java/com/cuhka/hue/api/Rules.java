package com.cuhka.hue.api;

import java.util.Collection;
import java.util.Optional;

public class Rules extends ImmutableSet<Rule> {
	protected Rules(Collection<? extends Rule> collection) {
		super(collection);
	}
	
	public Optional<Rule> findById(String id) {
		return find(r -> r.getId().equals(id));
	}
	
	public Optional<Rule> findByName(String name) {
		return find(r -> r.getName().equals(name));
	}
}
