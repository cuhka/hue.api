package com.cuhka.hue.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.stream.JsonReader;

class RuleParser extends DataParser<Rule> {
	private final String id;
	
	public RuleParser(String id) {
		this.id = id;
	}


	@Override
	protected Rule parseObject(JsonReader reader) throws IOException {
		Rule.Builder builder = new Rule.Builder();
		builder.id(id);
		reader.beginObject();

		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "name":
				builder.name(reader.nextString());
				break;
				
			case "owner":
				builder.owner(reader.nextString());
				break;
				
			case "created":
				builder.created(readInstant(reader));
				break;
				
			case "lasttriggered":
				builder.lastTriggered(readInstant(reader));
				break;
				
			case "timestriggered":
				builder.timesTriggered(reader.nextInt());
				break;
				
			case "status":
				builder.status(nextEnum(Rule.Status.class, reader));
				break;
				
			case "conditions":
				builder.conditions(readConditions(reader));
				break;
				
				
			case "actions":
				builder.actions(readActions(reader));
				break;
			
			}
		}
		
		reader.endObject();
		
		return builder.build();
	}


	private Collection<Action> readActions(JsonReader reader) throws IOException {
		Collection<Action> actions = new ArrayList<>();
		
		reader.beginArray();
		
		while (reader.hasNext()) {
			actions.add(readAction(reader));
		}
		
		reader.endArray();
		
		return actions;
	}


	private Collection<Condition> readConditions(JsonReader reader) throws IOException {
		Collection<Condition> conditions = new ArrayList<>();
		reader.beginArray();
		
		while (reader.hasNext()) {
			conditions.add(readCondition(reader));
		}
		
		reader.endArray();
		return conditions;
	}


	private Condition readCondition(JsonReader reader) throws IOException {
		Condition.Builder builder = new Condition.Builder();
		reader.beginObject();
		
		while (reader.hasNext()) {
			switch (reader.nextName()) {
			case "address":
				builder.address(reader.nextString());
				break;
				
			case "operator":
				builder.operator(nextEnum(Operator.class, reader));
				break;
			
			case "value":
				builder.value(reader.nextString());
				break;
				
			default:
				reader.skipValue();
				break;				
			}
		}
		reader.endObject();
		
		return builder.build();
	}

}
