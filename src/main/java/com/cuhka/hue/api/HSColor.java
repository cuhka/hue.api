package com.cuhka.hue.api;

import java.io.Serializable;

public class HSColor implements Serializable {
	private static final long serialVersionUID = -6096908627300081720L;

	public final static int MAX_HUE = 65535;
	public final static int MAX_SAT = 254;
	
	public final static int HUE_RED = 0;
	public final static int HUE_ORANGE = 6375;
	public final static int HUE_YELLOW = 12_750;
	public final static int HUE_GREEN = 25_500;
	public final static int HUE_BLUE = 46_920;
	public final static int HUE_PURPLE = 50_000;
	public final static int HUE_VIOLET = 53_295;
	
	private final int hue;
	private final int saturation;

	/**
	 * 
	 * @param hue value between 0 and 65535, 0 and 65535 are red, 25500 is green and 46920 is blue.
	 * @param saturation value between 0 ('white') and 255 ('most colored') 
	 */
	public HSColor(int hue, int saturation) {
		this.hue = hue;
		this.saturation = saturation;
	}
	
	public int getHue() {
		return hue;
	}

	public int getSaturation() {
		return saturation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hue;
		result = prime * result + saturation;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HSColor other = (HSColor) obj;
		if (hue != other.hue)
			return false;
		if (saturation != other.saturation)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HSColor [hue=");
		builder.append(hue);
		builder.append(", saturation=");
		builder.append(saturation);
		builder.append("]");
		return builder.toString();
	}

	public HSColor withHue(int hue) {
		return new HSColor(hue, this.getSaturation());
	}

	public HSColor withSaturation(int sat) {
		return new HSColor(this.getHue(), sat);
	}
}
