package com.cuhka.hue.api;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

abstract class ImmutableSet<E> implements Set<E> {
	protected final Set<E> items;
	
	protected ImmutableSet(Collection<? extends E> collection) {
		items = Collections.unmodifiableSet(new LinkedHashSet<>(collection));
	}
	
	@Override
	public int size() {
		return items.size();
	}

	@Override
	public boolean isEmpty() {
		return items.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return items.contains(o);
	}

	@Override
	public Iterator<E> iterator() {
		return items.iterator();
	}

	public Optional<E> find(Predicate<? super E> p) {
		return stream().filter(p).findAny();
	}	
	
	@Override
	public Object[] toArray() {
		return items.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return items.toArray(a);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return items.containsAll(c);
	}

	@Override
	public String toString() {
		return items.toString();
	}
	
	@Override
	public final boolean add(E e) {
		throw new UnsupportedOperationException("Immutable collection");
	}

	@Override
	public final boolean addAll(Collection<? extends E> c) {
		throw new UnsupportedOperationException("Immutable collection");
	}

	@Override
	public final void clear() {
		throw new UnsupportedOperationException("Immutable collection");
	}

	@Override
	public final boolean remove(Object o) {
		throw new UnsupportedOperationException("Immutable collection");
	}

	@Override
	public final boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("Immutable collection");
	}

	@Override
	public final boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("Immutable collection");
	}
}
