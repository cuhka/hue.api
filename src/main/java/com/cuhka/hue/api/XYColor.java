package com.cuhka.hue.api;

import java.io.Serializable;

public class XYColor implements Serializable {
	private static final long serialVersionUID = -1793602770460556362L;
	private final double x, y;

	/**
	 * @param x
	 * @param y
	 */
	public XYColor(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}

		if (object == this) {
			return true;
		}

		if (object instanceof XYColor) {
			XYColor other = (XYColor) object;
			return x == other.x && y == other.y;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return (int) (x + 7d * y);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("XYColor [x=");
		builder.append(x);
		builder.append(", y=");
		builder.append(y);
		builder.append("]");
		return builder.toString();
	}

}
