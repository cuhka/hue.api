package com.cuhka.hue.api;

import java.util.Collection;
import java.util.Optional;

public class Sensors extends ImmutableSet<Sensor> {
	protected Sensors(Collection<? extends Sensor> collection) {
		super(collection);
	}
	
	public Optional<Sensor> findById(String id) {
		return find(s -> s.getId().equals(id));
	}
	
	public Optional<Sensor> findByUuid(String uuid) {
		return find(s -> s.getUuid().equals(uuid));
	}
}
