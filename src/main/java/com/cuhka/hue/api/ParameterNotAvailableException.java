package com.cuhka.hue.api;


public class ParameterNotAvailableException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public ParameterNotAvailableException(HueError error) {
		super(error);
	}
}
