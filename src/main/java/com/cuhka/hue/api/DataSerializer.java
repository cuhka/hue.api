package com.cuhka.hue.api;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import com.google.gson.stream.JsonWriter;

public abstract class DataSerializer<T> {
	public abstract void write(T data, JsonWriter writer) throws IOException;
	
	protected void writeString(JsonWriter writer, String key, Optional<String> value) throws IOException {
		if (value.isPresent()) {
			writer.name(key).value(value.get());
		}
	}

	protected void writeDouble(JsonWriter writer, String key, Optional<Double> value) throws IOException {
		if (value.isPresent()) {
			writer.name(key).value(value.get().doubleValue());
		}
	}

	protected <E extends Enum<E>> void writeEnum(JsonWriter writer, String string, Optional<E> value) throws IOException {
		if (value.isPresent()) {
			writer.name(string).value(value.get().name().toLowerCase());
		}
	}

	protected void writeInteger(JsonWriter writer, String name, Optional<Integer> value) throws IOException {
		if (value.isPresent()) {
			writer.name(name).value(value.get().intValue());
		}
	}

	protected void writeBoolean(JsonWriter writer, String name, Optional<Boolean> value) throws IOException {
		if (value.isPresent()) {
			writer.name(name).value(value.get());
		}
	}

	protected void writeStringArray(JsonWriter writer, String name, Optional<? extends Iterable<String>> array) throws IOException {
		if (array.isPresent()) {
			writer.name(name).beginArray();
			
			for (String value : array.get()) {
				writer.value(value);
			}
			
			writer.endArray();
		}
	}
	
	protected void writeArray(JsonWriter writer, String name, Optional<? extends Iterable<? extends Object>> array) 
			throws IOException {
		if (array.isPresent()) {
			writer.name(name).beginArray();
			for (Object value : array.get()) {
				writeValue(writer, value);
			}
			writer.endArray();
		}
	}
	
	protected void writeMap(JsonWriter writer, String name, Optional<? extends Map<? extends Object, ? extends Object>> map) throws IOException {
		if (map.isPresent()) {
			writer.name(name);
			writeMap(writer, map.get());
		}
	}
	
	protected void writeMap(JsonWriter writer, Map<?, ?> map) throws IOException {
		writer.beginObject();

		for (Map.Entry<?, ?> entry : map.entrySet()) {
			writer.name(entry.getKey().toString());
			writeValue(writer, entry.getValue());
		}
		writer.endObject();
	}

	protected void writeValue(JsonWriter writer, Object value) throws IOException {
		if (value == null) {
			writer.nullValue();
		} else if (value instanceof String) {
			writer.value((String) value);
		} else if (value instanceof Boolean) {
			writer.value(((Boolean) value).booleanValue());
		} else if (value instanceof Long) {
			writer.value(((Long) value).longValue());
		} else if (value instanceof Number) {
			writer.value((Number) value);
		} else if (value instanceof Double) {
			writer.value(((Double) value).doubleValue());
		} else if (value instanceof Map) {
			Map<?, ?> map = (Map<?, ?>) value;
			writeMap(writer, map);
		} else {
			writer.value(value.toString());
		}
	}

	protected void writeAction(JsonWriter writer, Action action) throws IOException {
		writer.beginObject();
		writer.name("address").value(action.getAddress());
		writer.name("method").value(action.getMethod().name());
		writer.name("body");
		writeMap(writer, action.getBody());
		writer.endObject();
	}
}
