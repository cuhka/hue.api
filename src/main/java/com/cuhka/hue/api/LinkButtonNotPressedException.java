package com.cuhka.hue.api;


public class LinkButtonNotPressedException extends HueException {
	private static final long serialVersionUID = 1373568516341864352L;

	public LinkButtonNotPressedException(HueError error) {
		super(error);
	}
}
