package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.argThat;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.lang.reflect.Type;
import java.util.Map;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

public class TestUtils {
	static <T> T read(String fixture, DataParser<T> parser) throws IOException {
		return read(fixture, parser, null);
	}
	
	static JsonReader open(String fixture) throws IOException {
		InputStream in = TestUtils.class.getResourceAsStream("/" + fixture + ".json");
		InputStreamReader stream = new InputStreamReader(in, "utf-8"); 
		return new JsonReader(stream);
	}
	
	static <T> T read(String fixture, DataParser<T> parser, Transport transport) throws IOException {
		try (
			JsonReader reader = open(fixture)) {
			 T parse = parser.parse(reader);
			 assertEquals("End document excpected", JsonToken.END_DOCUMENT, reader.peek());
			 return parse;
		}
	}
	
	static CharSequence eq(CharSequence value) {
		return argThat(new BaseMatcher<CharSequence>() {
			@Override
			public boolean matches(Object item) {
				return item.toString().equals(value);
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("\"");
				description.appendText(value.toString());
				description.appendText("\"");
			}
		});
	}
	
	public static <T> Map<String, Object> serialize(T data, DataSerializer<T> serializer) throws UncheckedIOException {
		try {
			StringWriter out = new StringWriter();
			try (JsonWriter writer = new JsonWriter(out)) {
				serializer.write(data, writer);
			}
			Type type = new TypeToken<Map<String, ?>>(){}.getType();

			return new Gson().fromJson(out.toString(), type);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

}
