package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ColorTemperatureTest {
	@Test
	public void fromKelvin() {
		CTColor ct1 = CTColor.fromKelvin(25_000);
		assertEquals(40, ct1.inMired());
		assertEquals(25_000, ct1.inKelvin());
		CTColor ct2 = CTColor.fromKelvin(5_000);
		assertEquals(200, ct2.inMired());
		assertEquals(5_000, ct2.inKelvin());
	}
	
	
	@Test
	public void fromMired() {
		CTColor ct1 = CTColor.fromMired(40);
		assertEquals(40, ct1.inMired());
		assertEquals(25_000, ct1.inKelvin());
		CTColor ct2 = CTColor.fromMired(200);
		assertEquals(200, ct2.inMired());
		assertEquals(5_000, ct2.inKelvin());				
	}
	
	@Test
	public void equals() {
		assertEquals(CTColor.fromKelvin(25_000), CTColor.fromKelvin(25_000));
		assertNotEquals(CTColor.fromKelvin(25_000), CTColor.fromKelvin(25_001));
	}
	
	@Test
	public void compare() {
		assertTrue(CTColor.fromKelvin(25_000).compareTo(CTColor.fromKelvin(25_000)) == 0);
		assertTrue(CTColor.fromKelvin(25_000).compareTo(CTColor.fromKelvin(5_000)) > 0);
		assertTrue(CTColor.fromKelvin(5_000).compareTo(CTColor.fromKelvin(25_000)) < 0);
	}
}
