package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

public class RegisteredBridgesParserTest {
	private List<BridgeRecord> read(String fixture) throws IOException {
		try (JsonReader reader = TestUtils.open(fixture)) {
			 List<BridgeRecord> parse = new RegisteredBridgesParser().parse(reader);
			 assertEquals("End document excpected", JsonToken.END_DOCUMENT, reader.peek());
			 return parse;
		}

	}
	
	@Test
	public void noBridgesRegistered() throws IOException {
		assertTrue(read("empty_array").isEmpty());
	}
	
	@Test
	public void multipleBridgesRegistered() throws IOException {
		List<BridgeRecord> records = read("bridges");
		
		assertEquals(2,records.size());
	}
}
