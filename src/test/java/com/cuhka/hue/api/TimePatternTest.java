package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.EnumSet;
import java.util.Set;

import org.junit.Test;

public class TimePatternTest {
	@SuppressWarnings("unchecked")
	private <T extends TimeTrigger> T fromApi(String value) {
		return (T) TimeTrigger.fromApi(value);
	}
	
	@Test
	public void recurringTime() {
		RecurringTime rt = fromApi("W124/T06:44:00");
		Set<DayOfWeek> workdays = EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY);
		assertEquals(workdays, rt.getDays());
		assertEquals(LocalTime.of(6, 44, 0), rt.getTime());
		assertTrue(rt.getRandomize().isZero());
	}
	
	@Test
	public void randomizedRecurring() throws IOException {
		RecurringTime rt = fromApi("W006/T23:33:00A02:38:14");
		assertEquals(9494, rt.getRandomize().getSeconds());
	}
	
	@Test 
	public void scheduledDateTime() throws IOException {
		ScheduledDateTime dt = fromApi("2014-12-14T20:33:39");
		assertEquals(LocalDateTime.of(2014, 12, 14, 20, 33, 39), dt.getDateTime());
		assertTrue(dt.getRandomize().isZero());
	}
	
	@Test 
	public void scheduledDateRandomizedTime() throws IOException {
		ScheduledDateTime dt = fromApi("2014-12-14T20:33:39A13:14:19");
		assertEquals(LocalDateTime.of(2014, 12, 14, 20, 33, 39), dt.getDateTime());
		assertEquals(47659, dt.getRandomize().getSeconds());
	}
	
	@Test
	public void timer() throws IOException {
		Timer timer = fromApi("PT17:30:01");
		assertEquals(63001, timer.getDuration().getSeconds());
		assertTrue(timer.getRandomize().isZero());
	}
	
	@Test
	public void randomizedTimer() throws IOException {
		Timer timer = fromApi("PT17:30:01A00:30:00");
		assertEquals(63001, timer.getDuration().getSeconds());
		assertEquals(60 * 30, timer.getRandomize().getSeconds());
	}
	
	@Test
	public void recurringTimer() throws IOException {
		RecurringTimer timer = fromApi("R/PT08:40:00");
		assertEquals(31200, timer.getDuration().getSeconds());
		assertFalse(timer.getOccurences().isPresent());
	}
	

	@Test
	public void limitedOccurrencesTimer() throws IOException {
		RecurringTimer timer = fromApi("R23/PT08:15:00A00:05:00");
		assertEquals(29700, timer.getDuration().getSeconds());
		assertEquals(23, timer.getOccurences().get().intValue());
		assertEquals(5 * 60, timer.getRandomize().getSeconds());
	}	
}
