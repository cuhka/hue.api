package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RGBTest {
	@Test
	public void constructor() {
		RGB color = new RGB(0xABCDEF);
//		assertEquals(0xAB, color.getRed());
		assertEquals(0xCD, color.getGreen());
		assertEquals(0xEF, color.getBlue());
	}
}
