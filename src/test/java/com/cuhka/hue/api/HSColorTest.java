package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.Test;

public class HSColorTest {
	private HSColor color = new HSColor(7,9);
	
	@Test
	public void withHue() {
		HSColor other = color.withHue(12);
		
		assertNotSame(color,other);
		assertEquals(7, color.getHue());
		assertEquals(12, other.getHue());
	}
	
	@Test
	public void withSaturation() {
		HSColor other = color.withSaturation(3);
		assertNotSame(color,other);
		assertEquals(9, color.getSaturation());
		assertEquals(3, other.getSaturation());
	}
}
