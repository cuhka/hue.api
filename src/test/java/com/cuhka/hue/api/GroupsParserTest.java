package com.cuhka.hue.api;

import static org.junit.Assert.assertArrayEquals;

import java.io.IOException;

import org.junit.Test;

public class GroupsParserTest {
	@Test
	public void readIds() throws IOException {
		Groups groups = TestUtils.read("groups", new GroupsParser());
		
		
		assertArrayEquals(new String[] {"1","2"}, groups.stream().map(Group::getId).toArray());
	}
}
