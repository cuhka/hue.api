package com.cuhka.hue.api;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Set;

import org.junit.Test;

import com.cuhka.hue.api.Action.Method;

public class SchedulesParserTest {
	
	private Schedules read(String fixture) throws IOException {
		return TestUtils.read(fixture, new SchedulesParser());
	}
	
	private Schedules read() throws IOException {
		return read("schedules");
	}
	
	@Test
	public void readEmpty() throws IOException {
		Set<Schedule> schedules = read("empty-object");
		assertTrue("Should have no schedules", schedules.isEmpty());
	}
	
	@Test
	public void checkIds() throws IOException {
		Set<Schedule> schedules = read();
		String[] expected = {
				"6517784399245592", "7276728195346371", "8686736440532961", "2314815448061259", "3733158812549334",
				"1940332887618502", "6131490350020568", "1223260115508727", "2734097037522154", "4589734062017087",
				"9804717584448563", "7072324349231938"
		};
		
		Object[] actuals = schedules.stream().map(Schedule::getId).toArray();
		assertArrayEquals(expected, actuals);
	}
	
	@Test
	public void findById() throws IOException {
		Schedules schedules = read();
		assertTrue(schedules.findById("1223260115508727").isPresent());
		assertTrue(schedules.findById("3733158812549334").isPresent());
		assertFalse(schedules.findById("0000000000000000").isPresent());
	}
	
	@Test
	public void properties() throws IOException {
		Schedules schedules = read();
		Schedule schedule = schedules.findById("4589734062017087").orElseThrow(AssertionError::new);
		
		assertEquals("name","Zonop", schedule.getName());
		assertEquals("description", "\"Zon op\" voor", schedule.getDescription());
		assertEquals("created", LocalDateTime.of(2015, 1, 4,12,43,52), schedule.getCreated()); 
		assertEquals("status", Schedule.Status.DISABLED, schedule.getStatus());
	}
	
	
	
	
	@Test
	public void command() throws IOException {
		Schedules schedules = read();
		Schedule schedule = schedules.findById("7072324349231938").orElseThrow(AssertionError::new);
		Action command = schedule.getCommand(); 
		
		assertEquals("address", "/api/00000000657c78d8ffffffffdad18e66/groups/0/action", command.getAddress());
		assertEquals("method", Method.PUT, command.getMethod());
		assertEquals("body.scene", "3b766e267-off-0", command.getBody().get("scene"));
	}
	
	@Test
	public void autodelete() throws IOException {
		Schedules schedules = read();
		
		Schedule schedule = schedules.findById("2734097037522154").orElseThrow(AssertionError::new);
		assertFalse(schedule.isAutodelete());
		
		schedule = schedules.findById("1223260115508727").orElseThrow(AssertionError::new);
		assertTrue(schedule.isAutodelete());
	}
}
