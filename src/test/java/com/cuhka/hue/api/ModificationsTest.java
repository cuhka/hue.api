package com.cuhka.hue.api;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;

public class ModificationsTest {
	@Test
	public void findAttribute() {
		Modifications.Builder builder = new Modifications.Builder();
		builder.successes = asList(new Modification[] {new Modification("/path/to/attribute", "value")});
		
		Modifications result = new Modifications(builder);
		Optional<Modification> modification = result.findAttribute("attribute");
		assertTrue(modification.isPresent());
		assertEquals("value", modification.get().getStringValue());
		assertFalse(result.findAttribute("notthere").isPresent());
	}
	
}
