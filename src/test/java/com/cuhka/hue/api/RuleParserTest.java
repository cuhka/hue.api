package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class RuleParserTest {
	@Test
	public void parse() throws IOException {
		Rule rule = TestUtils.read("rule", new RuleParser("id"));
		
		assertEquals("id", rule.getId());
		assertEquals("Tap 2.4 Default", rule.getName());
		assertEquals("fffffffff0849153ffffffffb82c643d", rule.getOwner());
		assertEquals(ZonedDateTime.of(2016, 1, 4, 9, 19, 30, 0, ZoneOffset.UTC).toInstant(), rule.getCreated());
		assertEquals(ZonedDateTime.of(2016, 1,10, 8, 24, 57 , 0, ZoneOffset.UTC).toInstant(), rule.getLastTriggered().orElseThrow(AssertionError::new));

		List<Condition> conditions = rule.getConditions();
		assertEquals(2, conditions.size());
		Condition c1 = conditions.get(0);
		assertEquals("/sensors/2/state/buttonevent", c1.getAddress());
		assertEquals(Operator.EQ, c1.getOperator());
		assertEquals("18", c1.getValue().orElseThrow(AssertionError::new));
		
		Condition c2 = conditions.get(1);
		assertEquals("/sensors/2/state/lastupdated", c2.getAddress());
		assertEquals(Operator.DX, c2.getOperator());
		assertFalse(c2.getValue().isPresent());

		List<Action> actions = rule.getActions();
		
		assertEquals(1, actions.size());
		Action a = actions.get(0);
		assertEquals("/groups/0/action", a.getAddress());
		assertEquals(Action.Method.PUT, a.getMethod());
		Map<String, ?> body = a.getBody();
		
		assertEquals(1, body.size());
		assertEquals("86ac159ae-on-0", body.get("scene"));
	}
}
