package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

public class ModificationsParserTest {
	@Test
	public void emptyResponse() throws IOException {
		Modifications response = TestUtils.read("empty_array", new ModificationsParser());
		assertTrue(response.getSuccesses().isEmpty());
		assertTrue(response.getErrors().isEmpty());
	}

	@Test
	public void successes() throws IOException {
		Modifications response = TestUtils.read("put_response", new ModificationsParser());
		assertEquals("success count",4,response.getSuccesses().size());

		Modification success = response.getSuccesses().get(1);
		assertEquals("path", "/lights/1/state/hue", success.getPath());
		assertEquals("attribute","hue", success.getAttribute());
		assertEquals("value", 1000, ((Number)success.getValue()).intValue());
	}

	@Test
	public void errors() throws IOException {
		Modifications response = TestUtils.read("put_response", new ModificationsParser());
		assertEquals("error count",3,response.getErrors().size());
		HueError error = response.getErrors().get(2);
		assertEquals("type", 7, error.getType());
		assertEquals("address", "/lights/1/state/xy", error.getAddress());
		assertEquals("description","invalid value, , for parameter, xy", error.getDescription());
	}

	@Test
	public void errorresponse() throws IOException {
		TestUtils.read("error", new ModificationsParser());
	}

	@Test
	public void deleted() throws IOException {
		Modifications modifications = TestUtils.read("deleted_result", new ModificationsParser());
		Modification attributeModification = modifications.getSuccesses().get(0);
		assertEquals("path", "/groups/4", attributeModification.getPath());
		assertEquals("value", "deleted", attributeModification.getValue());
	}

	@Test
	public void xyarray() throws IOException {
		Modifications modifications = TestUtils.read("put_response", new ModificationsParser());
		Modification mod = modifications.findAttribute("xy").orElseThrow(AssertionError::new);
		List<Number> list = mod.getNumberListValue();
		
		assertEquals(2, list.size());
		assertEquals(0.1, list.get(0).doubleValue(),0.0);
		assertEquals(0.2, list.get(1).doubleValue(),0.0);
	}
}

