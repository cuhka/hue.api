package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;

public class ConfigParserTest {

	private Config read(String fixture) throws IOException {
		return TestUtils.read(fixture, new ConfigParser());
	}
	
	@Test
	public void parseSampleConfig() throws IOException {
		Config config = read("config"); 

		assertEquals("name", "Philips hue", config.getBridgeName());
		assertEquals("zigbeechannel",20, config.getZigbeeChannel());
		assertEquals("mac", "ab:cd:ef:00:01:02", config.getMacAddress());
		assertEquals("ipaddress", "192.168.1.4", config.getIPAddress());
		assertEquals("netmask", "255.255.255.0", config.getNetmask());
		
		Config.Proxy proxy = config.getProxy().orElseThrow(() -> new AssertionError("Expected proxy"));
		
		assertEquals("proxyaddress","192.168.1.3", proxy.getIpaddress());
		assertEquals("proxyport", 8080, proxy.getPort());

		assertEquals("UTC", LocalDateTime.of(2014, 6, 5, 20, 20, 04, 0).toInstant(ZoneOffset.UTC), config.getUTC());
		assertEquals("localtime", ZonedDateTime.of(2014, 6, 5, 22, 20, 04, 0,ZoneId.of("Europe/Amsterdam")), config.getLocalDateTime());
		assertEquals("timezone", ZoneId.of("Europe/Amsterdam"), config.getTimeZone());
		assertEquals("swversion", "01009914", config.getSoftwareVersion());
		assertEquals("apiversion", "1.2.1", config.getAPIVersion());
		assertTrue("linkbutton", config.isLinkButtonPressed());
		assertTrue("portalservices", config.isSynchronizingToPortal());
		assertTrue("portalconnection", config.isConnectedToPortal());
	}
	
	@Test
	public void noProxy() throws IOException {
		Config config =  read("config-noproxy");
		assertFalse("proxy", config.getProxy().isPresent());
	}
	
	@Test
	public void whitelist() throws IOException {
		Config config = read("config");

		Set<ApiRegistration> whitelist = config.getWhitelist();
		assertEquals("whitelist", 4, whitelist.size());
		Optional<ApiRegistration> foosRegistration = whitelist.stream().filter(r -> r.getId().equals("foodeveloper")).findFirst();
		assertTrue("whitelist/foodeveloper", foosRegistration.isPresent());

		ApiRegistration registration = foosRegistration.get();
		assertEquals("whitelist/foodeveloper/last use date", LocalDateTime.of(2014, 6, 5, 20, 20, 4, 0), registration.getLastUsed());
		assertEquals("whitelist/foodeveloper/create date", LocalDateTime.of(2014, 6, 4, 15, 36, 28, 0), registration.getCreated());
		assertEquals("whitelist/foodeveloper/name", "test user", registration.getName());
	}
	
	@Test
	public void swupdate() throws IOException {
		Config config = read("config");
		
		SoftwareUpdate swupdate = config.getSoftwareUpdate().orElseThrow(() -> new AssertionError("Excpected swupdate"));
		
		assertEquals("swupdate/updatestate",1,swupdate.getUpdateState());
		assertEquals("swupdate/url","www.meethue.com/patchnotes/1453", swupdate.getUrl());
		assertEquals("swupdate/text","This is a software update", swupdate.getText());
		assertEquals("swupdate/notify", true, swupdate.isNotify());
	}
	
	@Test
	public void noswupdate() throws IOException {
		Config config = read("config-noproxy");
		
		assertFalse("swupdate should not be present", config.getSoftwareUpdate().isPresent());
	}
	
	@Test
	public void portalstate() throws IOException {
		Config config = read("config");
		Config.PortalState portalstate = config.getPortalState();
		
		assertTrue("signedon", portalstate.isSignedOn());
		assertFalse("incoming", portalstate.isIncoming());
		assertTrue("outgoing", portalstate.isOutgoing());
		assertEquals("communication", Config.PortalState.Communication.DISCONNECTED, portalstate.getCommunication());
	}
}
