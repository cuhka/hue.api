package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.time.LocalDateTime;

import org.junit.Test;

public class NewLightsParserTest {
	@Test
	public void testScanned() throws IOException {
		NewLights newLights = TestUtils.read("newlights_scanned", new NewLightsParser());

		
		LightSummary[] lights = new LightSummary[2];
		lights = newLights.getLights().toArray(lights); 
		assertEquals("id","7", lights[0].getId());
		assertEquals("name", "Hue Lamp 7", lights[0].getName());
		
		assertEquals("id", "8", lights[1].getId());
		assertEquals("name", "Hue Lamp 8", lights[1].getName());
		
		LocalDateTime expected = LocalDateTime.of(2014, 07, 17, 14,10,15);
		assertTrue("lastscan", newLights.getLastScan().isPresent());
		assertEquals("lastscan", expected, newLights.getLastScan().get());
		assertFalse(newLights.isScanning());
	}
	
	@Test
	public void testScanning() throws IOException {
		NewLights newLights = TestUtils.read("newlights_scanning", new NewLightsParser());
		assertFalse(newLights.getLastScan().isPresent());
		assertTrue(newLights.isScanning());
	}
	
	@Test
	public void testNeverScanned() throws IOException {
		NewLights newLights = TestUtils.read("newlights_none", new NewLightsParser());
		assertFalse(newLights.getLastScan().isPresent());
		assertFalse(newLights.isScanning());
	}
}
