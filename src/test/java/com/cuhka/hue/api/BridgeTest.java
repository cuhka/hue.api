package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;

public class BridgeTest {
	private Transport transport = mock(Transport.class);
	private Bridge bridge = new Bridge(transport);

	@Test
	public void searchNewLights() {
		when(transport.post(anyObject(), anyObject(), TestUtils.eq("/lights"))).thenReturn(new Modifications(new Modifications.Builder()));		
		bridge.searchNewLights("ABC","DEF","XYZ");
		
		verify(transport).post(Matchers.anyCollectionOf(String.class), any(NewLightsSerializer.class), TestUtils.eq("/lights"));
	}
	
	
	@Test
	public void fetchSchedules() {
		bridge.fetchSchedules();
		verify(transport, times(1)).get(any(SchedulesParser.class), TestUtils.eq("/schedules/"));
	}
	
	@Test
	public void fetchGroups() {
		bridge.fetchGroups();
		verify(transport, times(1)).get(any(GroupsParser.class), TestUtils.eq("/groups"));
	}
	
	@Test
	public void fetchScenes() {
		bridge.fetchScenes();
		verify(transport, times(1)).get(any(ScenesParser.class), TestUtils.eq("/scenes/"));
	}
	
	@Test
	public void fetchScene() {
		bridge.fetchScene("foo");
		verify(transport, times(1)).get(any(SceneParser.class), TestUtils.eq("/scenes/foo"));
	}
	
	@Test
	public void recallScene() {
		bridge.recallScene("foo");
		verify(transport, times(1)).put(eq("foo"), any(RecallSceneSerializer.class), TestUtils.eq("/groups/0/action"));
	}
	
	@Test
	public void deleteScene() {
		bridge.deleteScene("foo");
		ArgumentCaptor<CharSequence> path = ArgumentCaptor.forClass(CharSequence.class);
		verify(transport, times(1)).delete(path.capture());
		assertEquals("/scenes/foo", path.getValue().toString());
	}
	
	@Test
	public void deleteGroup() {
		Group group = mock(Group.class);
		when(group.getId()).thenReturn("9");
		bridge.deleteGroup(group);
		verify(transport).delete(TestUtils.eq("/groups/9"));
	}
	
	@Test 
	public void updateGroupLightState() {
		LightStateUpdate.Builder builder = new LightStateUpdate.Builder();
		Group group = mock(Group.class);
		when(group.getId()).thenReturn("9");
		LightStateUpdate update = builder.build();
		bridge.updateGroupLightState(group, update);
		verify(transport).put(same(update), any(LightStateUpdateSerializer.class), TestUtils.eq("/groups/9/action"));
	}
	
	@Test
	public void updateLightState() {
		Light light = mock(Light.class);
		when(light.getId()).thenReturn("34");
		LightStateUpdate.Builder builder = new LightStateUpdate.Builder();
		LightStateUpdate update = builder.build();
		bridge.updateLightState(light, update);
		
		verify(transport).put(same(update), any(LightStateUpdateSerializer.class), TestUtils.eq("/lights/34/state"));
	}
	
	@Test
	public void updateName() {
		Light light = mock(Light.class);
		when(light.getId()).thenReturn("34");
		LightAttributesUpdate.Builder update = new LightAttributesUpdate.Builder();
		update.name("foo");
		bridge.updateLightAttributes(light, update.build());
		ArgumentCaptor<LightAttributesUpdate> argument = ArgumentCaptor.forClass(LightAttributesUpdate.class);
		verify(transport).put(argument.capture(), any(LightAttributesUpdateSerializer.class), TestUtils.eq("/lights/34"));
		assertEquals("foo", argument.getValue().getName().orElseThrow(AssertionError::new));
	}
	
	@Test
	public void deleteRegistration() {
		ApiRegistration registration = mock(ApiRegistration.class);
		when(registration.getId()).thenReturn("id");
		bridge.deleteRegistration(registration);
		verify(transport).delete(TestUtils.eq("/config/whitelist/id"));
	}
	
	@Test
	public void fetchSensors() {
		bridge.fetchSensors();
		verify(transport).get(any(SensorsParser.class), TestUtils.eq("/sensors/"));
	}
	
	@Test
	public void fetchSensor() {
		bridge.fetchSensor("9");
		verify(transport).get(any(SensorParser.class), TestUtils.eq("/sensors/9"));
	}
}
