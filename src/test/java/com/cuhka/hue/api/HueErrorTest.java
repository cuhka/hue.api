package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

import org.junit.Test;

public class HueErrorTest {
	private HueError.Builder builder = new HueError.Builder();
	@Test
	public void toException() {
		builder.type = 1;
		builder.address = "/a/b";
		builder.description = "reason why";
		
		HueError error = new HueError(builder);
		HueException exception = error.toException();
		assertSame(error, exception.getError().get());
		assertEquals("(1) /a/b: reason why", exception.getMessage());
	}
	
	public final static int INVALID_JSON = 2;
	public final static int RESOURCE_NOT_AVAILABLE = 3;
	public final static int METHOD_NOT_AVAILABLE_FOR_RESOURCE = 4;
	public final static int MISSING_REQUIRED_PARAMETERS = 5;
	public final static int PARAMETER_NOT_AVAILABLE = 6;
	public final static int INVALID_VALUE = 7;
	public final static int PARAMETER_NOT_MODIFIABLE = 8;
	public final static int INTERNAL_ERROR = 901;
	public final static int LINK_BUTTON_NOT_PRESSED = 101;
	public final static int PARAMETER_NOT_MODIFIABLE_DEVICE_OFF = 201;
	public final static int GROUP_TABLE_FULL = 301;
	public final static int DEVICE_GROUP_TABLE_FULL = 302;
	
	@Test
	public void noError() {
		assertFalse(new HueException().getError().isPresent());
	}
	
	@Test
	public void defaultsToHueException() {
		builder.type = 1024;
		assertEquals(HueException.class, new HueError(builder).toException().getClass());
	}

	@Test
	public void testExceptions() {
		int[] errors = {
				HueError.UNAUTHORIZED_USER,
				HueError.INVALID_JSON,
				HueError.RESOURCE_NOT_AVAILABLE,
				HueError.MISSING_REQUIRED_PARAMETERS,
				HueError.PARAMETER_NOT_AVAILABLE,
				HueError.INVALID_VALUE,
				HueError.PARAMETER_NOT_MODIFIABLE,
				HueError.INTERNAL_ERROR,
				HueError.LINK_BUTTON_NOT_PRESSED,
				HueError.PARAMETER_NOT_MODIFIABLE_DEVICE_OFF,
				HueError.GROUP_TABLE_FULL,
				HueError.DEVICE_GROUP_TABLE_FULL
		};
		
		Class<?>[] excpected = {
				UnauthorizedUserException.class,
				InvalidJsonException.class,
				ResourceNotAvailableException.class,
				MissingRequiredParametersException.class,
				ParameterNotAvailableException.class,
				InvalidValueException.class,
				ParameterNotModifiableException.class,
				InternalErrorInBridgeException.class,
				LinkButtonNotPressedException.class,
				DeviceSwitchedOffException.class,
				GroupTableFullException.class,
				DeviceGroupTableFullException.class
		};
		
		for (int i = 0; i < errors.length; i++) {
			builder.type = errors[i];
			assertEquals(excpected[i], new HueError(builder).toException().getClass());
		}
	}
}
