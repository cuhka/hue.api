package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LightStateUpdateTest {
	private LightStateUpdate.Builder state = new LightStateUpdate.Builder();

	@Test
	public void on() {
		assertFalse(state.getOn().isPresent());
		state.on(true);
		assertTrue(state.getOn().orElseThrow(AssertionError::new));
		
		state.on(false);
		assertFalse(state.getOn().orElseThrow(AssertionError::new));
		state.on(null);
		assertFalse(state.getOn().isPresent());
	}
	
	@Test
	public void bri() {
		assertFalse(state.getBrightness().isPresent());
		state.brightness(50);
		assertEquals(50, state.getBrightness().orElseThrow(AssertionError::new).intValue());
		state.brightness(null);
		assertFalse(state.getBrightness().isPresent());
	}
	
	@Test
	public void huesat() {
		assertFalse(state.getHS().isPresent());
		state.hs(new HSColor(25500,255));
		HSColor color = state.getHS().orElseThrow(AssertionError::new);
		assertEquals(25500, color.getHue());
		assertEquals(255, color.getSaturation());
		state.hs(null);
		assertFalse(state.getHS().isPresent());
	}
	
	@Test
	public void xy() {
		assertFalse(state.getXY().isPresent());
		state.xy(new XYColor(40,50));
		
		XYColor xy =  state.getXY().orElseThrow(AssertionError::new);
		assertEquals(40D, xy.getX(),0.0001);
		assertEquals(50D, xy.getY(),0.0001);
		state.xy(null);
		assertFalse(state.getXY().isPresent());

	}
	
	@Test
	public void ct() {
		assertFalse(state.getColorTemperature().isPresent());
		state.ct(CTColor.fromKelvin(50));
		
		assertEquals(50, state.getColorTemperature().orElseThrow(AssertionError::new).inKelvin());
		state.ct(null);
		assertFalse(state.getColorTemperature().isPresent());
	}
	
	@Test
	public void alert() {
		assertFalse(state.getAlert().isPresent());
		state.alert(Alert.LSELECT);
		
		assertEquals(Alert.LSELECT, state.getAlert().orElseThrow(AssertionError::new));
		state.alert(null);
		assertFalse(state.getAlert().isPresent());		
	}
	
	@Test
	public void effect() {
		state.effect(Effect.COLORLOOP);
		assertEquals(Effect.COLORLOOP, state.getEffect().orElseThrow(AssertionError::new));
		state.effect(null);
		assertFalse(state.getEffect().isPresent());			
	}
	
	@Test
	public void transitiontime() {
		assertFalse(state.getTransitionTime().isPresent());
		state.transitionTime(50);
		
		assertEquals(50, state.getTransitionTime().orElseThrow(AssertionError::new).intValue());
		state.transitionTime(null);
		assertFalse(state.getTransitionTime().isPresent());		
	}
}
