package com.cuhka.hue.api;

import static org.junit.Assert.assertEquals;

import java.io.UncheckedIOException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class ScheduleUpdateSerializerTest {
	private final ScheduleUpdate.Builder builder = new ScheduleUpdate.Builder();
	private final LocalTime time = LocalTime.of(7,9,2);

	private Map<String, Object> serialize() throws UncheckedIOException {
		return TestUtils.serialize(builder.build(), new ScheduleUpdateSerializer());
	}

	@Test
	public void noDays() {
		RecurringTime times = new RecurringTime(Collections.emptySet(), time);

		builder.localtime(times).build();
		Map<String, Object> result = serialize();

		assertEquals("W000/T07:09:02", result.get("localtime"));
	}

	@Test
	public void allDays() {
		RecurringTime times = new RecurringTime(EnumSet.allOf(DayOfWeek.class), time);
		builder.localtime(times).build();
		Map<String, Object> result = serialize();

		assertEquals("W127/T07:09:02", result.get("localtime"));
	}

	@Test 
	public void tuesday() {
		RecurringTime times = new RecurringTime(Collections.singleton(DayOfWeek.TUESDAY), time);
		builder.localtime(times).build();
		Map<String, Object> result = serialize();

		assertEquals("W032/T07:09:02", result.get("localtime"));
	}

	@Test
	public void weekdays() {
		//124
		Set<DayOfWeek> days = EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY);
		RecurringTime times = new RecurringTime(days, time);
		builder.localtime(times).build();
		Map<String, Object> result = serialize();

		assertEquals("W124/T07:09:02", result.get("localtime"));
	}

	@Test
	public void weekends() {
		Set<DayOfWeek> days = EnumSet.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);
		RecurringTime times = new RecurringTime(days, time);
		builder.localtime(times).build();
		Map<String, Object> result = serialize();

		assertEquals("W003/T07:09:02", result.get("localtime"));
	}

	@Test
	public void randomizedSchedule() {
		Duration duration = Duration.ofSeconds((64 * 60) + 14);
		RecurringTime times = new RecurringTime(Collections.singleton(DayOfWeek.TUESDAY), time, duration);
		builder.localtime(times);
		Map<String, Object> result = serialize();

		assertEquals("W032/T07:09:02A01:04:14", result.get("localtime"));
	}
	
	@Test
	public void specificMoment() {
		ScheduledDateTime sdt = new ScheduledDateTime(LocalDateTime.of(2016, 1, 24, 20, 48, 23));
		builder.localtime(sdt);
		Map<String, Object> result = serialize();

		assertEquals("2016-01-24T20:48:23", result.get("localtime"));
	}
	
	@Test
	public void randomizedSpecificMoment() {
		Duration duration = Duration.ofSeconds((1203 * 60) + 32);
		ScheduledDateTime sdt = new ScheduledDateTime(LocalDateTime.of(2016, 1, 24, 20, 48, 23), duration);
		builder.localtime(sdt);
		Map<String, Object> result = serialize();

		assertEquals("2016-01-24T20:48:23A20:03:32", result.get("localtime"));
	}
	
	@Test 
	public void timer() {
		Timer timer = new Timer(Duration.ofSeconds(13688));
		builder.localtime(timer);
		Map<String, Object> result = serialize();
		assertEquals("PT03:48:08", result.get("localtime"));
	}
	
	@Test
	public void randomizedTimer() {
		Timer timer = new Timer(Duration.ofSeconds(13688), Duration.ofSeconds(1255));
		builder.localtime(timer);
		Map<String, Object> result = serialize();
		assertEquals("PT03:48:08A00:20:55", result.get("localtime"));
	}
	
	@Test
	public void recurringTimer() {
		RecurringTimer timer = new RecurringTimer(Duration.ofMinutes(15), 7);
		builder.localtime(timer);
		Map<String, Object> result = serialize();
		assertEquals("R07/PT00:15:00", result.get("localtime"));
	}
	
	@Test
	public void recurringRandomizedTimer() {
		RecurringTimer timer = new RecurringTimer(Duration.ofMinutes(15), Duration.ofSeconds(5), 7);
		builder.localtime(timer);
		Map<String, Object> result = serialize();
		assertEquals("R07/PT00:15:00A00:00:05", result.get("localtime"));
	}
	
	@Test
	public void infiniteRecurringTimer() {
		RecurringTimer timer = new RecurringTimer(Duration.ofMinutes(15));
		builder.localtime(timer);
		Map<String, Object> result = serialize();
		assertEquals("R/PT00:15:00", result.get("localtime"));
	}
	
}
