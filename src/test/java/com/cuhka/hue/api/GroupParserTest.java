package com.cuhka.hue.api;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.junit.Test;

public class GroupParserTest {
	private Transport transport = mock(Transport.class);
	@Test
	public void parse() throws IOException {
		Group group = TestUtils.read("group", new GroupParser("90210"), transport);
		
		assertEquals("name","Test group", group.getName());
		assertArrayEquals("lights", new String[] {"1","2"}, group.getLightIds().toArray());
	}
	
	@Test(expected=HueException.class)
	public void error() throws IOException {
		TestUtils.read("error", new GroupParser("90210"), transport);
	}
}
