package com.cuhka.hue.api;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ScheduleTest {
	private Schedule.Builder builder  = new Schedule.Builder("34");
	
	@Test 
	public void updateBuilder() {
		builder.name = "old";
		Schedule schedule = new Schedule(builder);
		Modifications.Builder builder = new Modifications.Builder();
		builder.successes = singletonList(new Modification("/schedule/34/name","newname"));
		Modifications modification = new Modifications(builder);
		
		Schedule.MergeAttributes update = new Schedule.MergeAttributes(schedule, modification);
		
		Schedule updated = new Schedule(update);
		assertEquals("newname", updated.getName());
	}
}
