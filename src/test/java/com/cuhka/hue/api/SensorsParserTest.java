package com.cuhka.hue.api;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class SensorsParserTest {
	private Sensors sensors;
	
	@Before public void readFixture() throws IOException {
		sensors = TestUtils.read("sensors", new SensorsParser());
	}
	
	@Test
	public void standardData() {
			Sensor daylight = sensors.findById("1").get();
			
			assertEquals("Daylight", daylight.getName());
			assertEquals("Daylight", daylight.getType());
			assertEquals("PHDL00", daylight.getModelId());
			assertEquals("Philips", daylight.getManufacturer());
			assertEquals("1.0", daylight.getSwVersion());
			
			Sensor tap = sensors.findById("2").get();
			
			assertEquals("Tap Keuken", tap.getName());
			assertEquals("ZGPSwitch", tap.getType());
			assertEquals("ZGPSWITCH", tap.getModelId());
			assertEquals("Philips", tap.getManufacturer());
			assertEquals("00:00:00:00:00:43:46:b5-f2", tap.getUuid());
	}
	
	@Test
	public void config() {
		Sensor daylight = sensors.findById("1").get();
		Map<String, Object> config = daylight.getConfig();
		
		assertEquals(Boolean.TRUE, config.get("on"));
		assertEquals("none", config.get("long"));
		assertEquals("none", config.get("lat"));
		assertEquals(30, ((Number)config.get("sunriseoffset")).intValue());
		assertEquals(-30, ((Number)config.get("sunsetoffset")).intValue());
	}
	
	@Test
	public void state() {
		Sensor daylight = sensors.findById("1").get();
		Map<String, Object> state = daylight.getState();
		
		assertTrue(state.containsKey("daylight"));
		assertNull(state.get("daylight"));
		assertEquals("none", state.get("lastupdated"));
	}
}
