package com.cuhka.hue.api;

import java.io.IOException;

class ReadFixture {
	@Deprecated
	static <T> T read(String fixture, DataParser<T> parser) throws IOException {
		return TestUtils.read(fixture, parser);
	}

	@Deprecated
	static <T> T read(String fixture, DataParser<T> parser, Transport transport) throws IOException {
		return TestUtils.read(fixture, parser);
	}

	@Deprecated
	static CharSequence eq(CharSequence value) {
		return TestUtils.eq(value);
	}
}
