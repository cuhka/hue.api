package com.cuhka.hue.api;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

public class ScenesParserTest {	
	private Scenes scenes;

	@Before
	public void readScenes() throws IOException {
		scenes = TestUtils.read("scenes", new ScenesParser());
	}
	
	@Test
	public void checkIds() {
		String[] expecteds = {
				"3b766e267-off-0", "42f2ce120-on-0", "ff5712732-on-0", "c14a18b9c-on-0", "cf942ab8c-off-0",
				"a4e0ff130-on-4", "a4e0ff130-on-0", "c8416ea1d-on-2", "3137addbe-on-1", "72df79a22-off-0",
				"9df49c948-on-0", "d15806a10-on-0", "3f253da49-on-3", 
		};
		Object[] actuals = scenes.stream().limit(13).map(Scene::getId).toArray();
		
		assertArrayEquals(expecteds, actuals);
	}
	
	@Test
	public void content() {
		Scene scene = scenes.findById("d15806a10-on-0").orElseThrow(AssertionError::new);
		assertEquals("Blue rain on 140", scene.getName());
		
		String[] expecteds = {"1", "2"};
		Object[] actuals = scene.getLights().toArray();
		
		assertArrayEquals(expecteds, actuals);
		assertEquals("somebody", scene.getOwner());
		assertTrue(scene.isRecycle());
		assertFalse(scene.isLocked());
		AppData appData = scene.getAppData().orElseThrow(AssertionError::new);
		assertEquals(6, appData.getVersion());
		assertEquals("my data", appData.getData());
		assertEquals("", scene.getPicture());
		assertFalse(scene.getLastUpdated().isPresent());
		assertEquals(2, scene.getVersion());
	}
	
	@Test
	public void v2scene() throws IOException {
		Scene scene = TestUtils.read("v2-scene", new SceneParser(""));
		
		assertEquals(2, scene.getLightStates().size());
		Optional<LightState> state = scene.lookupLightState("4");
		
		assertTrue(state.isPresent());
		LightState lightState = state.get();
		assertEquals(ColorMode.CT, lightState.getColormode().get());
		assertEquals(254, lightState.getBrightness());
		assertTrue(lightState.isOn());
		assertEquals(155, lightState.getCTColor().inMired());
	}
}
